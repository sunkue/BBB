#version 450


layout(std140) uniform VP_MAT
{
	mat4 u_vp_mat;
};

uniform mat4 u_m_mat;
uniform mat4 u_sub_model_mat = mat4(1);

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_texcoord;
layout(location = 3) in ivec4 a_boneIDs;
layout(location = 4) in vec4 a_weights;

const int MAX_BONES = 100;
uniform mat4 u_FinalBoneMatrices[MAX_BONES];

out VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} vs_out;

uniform vec2 u_texcoord_offset = vec2(0);
uniform bool u_animate = false;
void main()
{
	vec4 totalPosition = vec4(a_position, 1) * (1 - float(u_animate));
    vec3 localNormal = a_normal;
    
    for(int i = 0 ; i < 4 && u_animate ; i++)
    {
        if(a_boneIDs[i] == -1) 
            continue;
        if(a_boneIDs[i] >= MAX_BONES) 
        {
            totalPosition = vec4(a_position, 1.0f);
            break;
        }
        vec4 localPosition = u_FinalBoneMatrices[a_boneIDs[i]] * vec4(a_position,1.0f);
        totalPosition += localPosition * a_weights[i];
        vec3 localNormal = mat3(u_FinalBoneMatrices[a_boneIDs[i]]) * a_normal;
    } 

	vs_out.world_pos = vec3(u_m_mat * u_sub_model_mat * vec4(totalPosition.xyz, 1));
	vs_out.normal = normalize(mat3(transpose(inverse(u_m_mat * u_sub_model_mat))) * localNormal);
	vs_out.texcoord = a_texcoord + u_texcoord_offset;

	gl_Position = u_vp_mat * vec4(vs_out.world_pos, 1);
}

