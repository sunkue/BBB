#version 450
#pragma optionNV(unroll none)
layout (lines_adjacency) in;
layout (line_strip, max_vertices = 128) out;
//layout (triangle_strip, max_vertices = 128) out;

uniform int tess = 30;

vec4 hermit(float t, vec4 p0, vec4 v0, vec4 p1, vec4 v1)
{
/*
	const mat4 m =
	mat4
	(
		 2, -3,  0, 1,
		 1, -2,  1, 1,
		-2,  3,  0, 0,
		 1, -1,  0, 0
	);
*/
/*
	const mat4 m =
	mat4
	(
		 2,  1, -2,  1,
		-3, -2,  3, -1,
		 0,  1,  0,  0,
		 1,  1,  0,  0
	);
*/
	vec4 Tmat = vec4(pow(t,3), pow(t,2), t, 1);
	mat4 params =  mat4(p0, v0, p1, v1);

	float s = 1 - t;
	float s2 = pow(s, 2);
	float t2 = pow(t, 2);

	vec4 ret = s2*(1+2*t)*p0 + t2*(1+2*s)*p1 + s2*t*v0 - s*t2*v1;

//	vec4 ret = params *  m * Tmat;
	return ret;
}

void main(void) 
{
	float dt = 1 / (float(tess));
	float tension = 0;
	
	vec4 v[2];
	v[0] = (1 - tension) * (gl_in[2].gl_Position - gl_in[0].gl_Position) / 2;
	v[1] = (1 - tension) * (gl_in[3].gl_Position - gl_in[1].gl_Position) / 2;

	for (int i = 0; i <= tess; i++)
	{
		float t = dt * i;
		gl_Position = hermit(t, gl_in[1].gl_Position, v[0], gl_in[2].gl_Position, v[1]);
		EmitVertex();
	}

	EndPrimitive();
} 

/*
void main(void) 
{
	float du = 1.0/float(tess);
	float u = 0;
	for (int i = 0; i<=tess; i++)
	{
		float term1 = 1.0 - u;
		float term2 = term1*term1;
		float term3 = term1*term2;
		float u2 = u*u;
		float u3 = u*u2;
		vec4 p = term3 * gl_in[0].gl_Position
		+ 3.0 * u * term2 * gl_in[1].gl_Position
		+ 3.0 * u2 * term1 * gl_in[2].gl_Position
		+ u3 * gl_in[3].gl_Position;
		gl_Position = p;
		EmitVertex();
		u += du;
	}
	EndPrimitive();
} 
*/