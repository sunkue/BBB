#version 450

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_right;

layout(std140) uniform VP_MAT
{
	mat4 u_vp_mat;
};

float w = 20; // track scale

out vec4 outliner;
out vec4 inliner;

out GS_IN
{
	vec4 world_pos_in;
	vec4 world_pos_out;
	vec3 normal;
	vec2 texcoord;
} vs_out;

void main()
{
    gl_Position = u_vp_mat * vec4(a_position, 1.0); 
    outliner = u_vp_mat * vec4(a_position + w * a_right, 1.0); 
    inliner = u_vp_mat * vec4(a_position - w * a_right, 1.0); 

	vs_out.world_pos_out = vec4(a_position + w * a_right, 1);
	vs_out.world_pos_in = vec4(a_position - w * a_right, 1);
	vs_out.normal = vec3(0, 1, 0);
}
