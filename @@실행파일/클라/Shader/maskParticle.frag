#version 450

struct Material
{
	sampler2D albedo1;
	sampler2D specular1;
	float shininess;
};

in VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} fs_in;

uniform Material u_material;
uniform vec3 u_color = vec3(1);

uniform sampler2D u_depth;
uniform float u_transitionSize;
const vec2 invViewportSize = vec2(1.f/1240.f, 1.f/720.f);
const vec2 cameraRange = vec2(0.1f, 20000.f);

out vec4 o_color;

float calc_depth(in float z)
{
    return (2.0 * cameraRange.x) / (cameraRange.y + cameraRange.x - z*(cameraRange.y - cameraRange.x));
}

void main()
{
	vec2 texcoord = fs_in.texcoord;

	o_color =  vec4(u_color, texture(u_material.albedo1, texcoord).r);
    vec2 coords = gl_FragCoord.xy * invViewportSize;
    float geometryZ = calc_depth(texture(u_depth, coords).r);
    float sceneZ = calc_depth(gl_FragCoord.z);
    float a = clamp(geometryZ - sceneZ, 0.0, 1.0);
    float b = smoothstep(0.0, u_transitionSize, a);
    o_color *= b;
   // o_color = vec4(a, 0, 0, 1.0); // uncomment to visualize raw Z difference
    //o_color = vec4(b, b, b, 1.0); // uncomment to visualize blending coefficient
}
