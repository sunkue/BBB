
struct Material
{
	sampler2D albedo1;
	sampler2D specular1;
	float shininess;
};

struct LightBasic
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight
{
	LightBasic power;
	vec3 position;
	vec3 direction;
	vec3 attenuation;
	float in_cutoff;
	float out_cutoff;
};

uniform Material u_material;
uniform SpotLight u_spotlight0;
uniform SpotLight u_spotlight1;
uniform SpotLight u_spotlight2;
uniform SpotLight u_spotlight3;
uniform vec3 u_camera_pos;

out vec4 o_color;

in VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} fs_in;

void main()
{
	vec3 world_pos = fs_in.world_pos;
	vec3 normal = fs_in.normal;
	vec2 texcoord = fs_in.texcoord;
	vec3 view_dir = normalize(u_camera_pos - world_pos);

	o_color = texture(u_material.albedo1, texcoord);
	
	vec3 albedo = o_color.rgb;
	float specular_color = 0;
	float shininess = 1524;

	vec3 result = vec3(0);
	result = caculate_light(u_spotlight0, view_dir, normal, world_pos, albedo, specular_color, shininess, 0);
	result = result + caculate_light(u_spotlight1, view_dir, normal, world_pos, albedo, specular_color, shininess, 0);
	result = result + caculate_light(u_spotlight2, view_dir, normal, world_pos, albedo, specular_color, shininess, 0);
	result = result + caculate_light(u_spotlight3, view_dir, normal, world_pos, albedo, specular_color, shininess, 0);
	o_color = vec4(result, 1);
}

