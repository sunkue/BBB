#version 450

uniform sampler2D u_texture; 
uniform sampler2D u_mask; 
uniform bool u_blackMask = true;

in vec2 texcoord; 
out vec4 o_flagcolor;

void main() 
{ 
    o_flagcolor = texture(u_texture, texcoord);
    //o_flagcolor.a = 1 - texture(u_mask, texcoord).r;
    o_flagcolor.a = float(u_blackMask) + (1 - 2 * float(u_blackMask)) * texture(u_mask, texcoord).r;
} 

