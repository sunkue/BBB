#pragma once

#pragma comment(lib,"./Dependencies/assimp/assimp-vc140-mt.lib")

#include "Dependencies/assimp/assimp/Importer.hpp";
#include "Dependencies/assimp/assimp/scene.h";
#include "Dependencies/assimp/assimp/postprocess.h";

#pragma comment(lib,"./Dependencies/OPGL/x64GL/glew32.lib")
#pragma comment(lib,"./Dependencies/OPGL/x64GL/glew32s.lib")
#pragma comment(lib,"./Dependencies/OPGL/x64GL/freeglut.lib")
#pragma comment(lib,"./Dependencies/GLFW/glfw3.lib")
#pragma comment(lib,"./Dependencies/GLFW/glfw3_mt.lib")
#pragma comment(lib,"./Dependencies/GLFW/glfw3dll.lib")
#pragma comment(lib,"./Dependencies/FTGL/freetype.lib")

#pragma comment(lib,"./Dependencies/irrklang/lib/irrKlang.lib")

#define WIN32_LEAN_AND_MEAN


#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <cstdlib>
#include <cassert>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <chrono>
#include <array>
#include <queue>
#include <functional>
#include <memory>
#include <list>
#include <set>
#include <unordered_set>

using namespace std;
using namespace chrono;
using namespace std::literals::string_view_literals;
using clk = std::chrono::high_resolution_clock;

#pragma warning(push)
#pragma warning(disable: 26495)
#pragma warning(disable: 26812)


#include "Dependencies/glad/include/glad/glad.h"
//#include "Dependencies/glad/include/KHR/khrplatform.h"

//#include "Dependencies/OPGL/glew.h"
//#include "Dependencies/OPGL/wglew.h"
//#/include "Dependencies/OPGL/freeglut.h"
#include "Dependencies/GLFW/GLFW/glfw3.h"
#include "Dependencies/GLFW/GLFW/glfw3native.h"

#include "../../NetworkShare/Share.h"
#include "sunkue_glm.hpp"

#include "Dependencies/imgui/imgui.h"
#include "Dependencies/imgui/imconfig.h"
#include "Dependencies/imgui/imgui_impl_glfw.h"
#include "Dependencies/imgui/imgui_impl_opengl3.h"

#include "Dependencies/FTGL/ft2build.h"

#include "Dependencies/irrklang/include/irrKlang.h"

#include FT_FREETYPE_H

#define gui ImGui

#define initpath(x) "Resource/init/"s.append(x)
#define soundPath(x) "Resource/Audio/"s.append(x)

using namespace irrklang;

class AssimpGLMHelpers
{
public:

	static inline glm::mat4 ConvertMatrixToGLMFormat(const aiMatrix4x4& from)
	{
		glm::mat4 to;
		//the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
		to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
		to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
		to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
		to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;
		return to;
	}

	static inline glm::vec3 GetGLMVec(const aiVector3D& vec)
	{
		return glm::vec3(vec.x, vec.y, vec.z);
	}

	static inline glm::quat GetGLMQuat(const aiQuaternion& pOrientation)
	{
		return glm::quat(pOrientation.w, pOrientation.x, pOrientation.y, pOrientation.z);
	}
};


#pragma warning(pop)