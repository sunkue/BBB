#pragma once

#include "Vehicle.h"

class Player
{
private:
	enum class State
	{
		FREE
		, INCHARGE // nonready
		, MATCHREADY
		, RUNNING
	} state{ State::FREE };
public:
	void ChangeReady(bool ready) { state = ready ? State::MATCHREADY : State::INCHARGE; }
	bool IsFree()const { return  State::FREE == state; }
	bool IsReady()const { return  State::MATCHREADY == state; }

public:
	GET_REF(vehicle);

	void Connect(const VehiclePtr& v)
	{
		state = State::INCHARGE;
		InitVehicle(v);
	}

	void Disconnect()
	{
		state = State::FREE;
		DisableVehicle();
	}

	void InitVehicle(const VehiclePtr& v)
	{
		DisableVehicle();
		vehicle_ = v;
		vehicle_->set_enable(true);
	}

	

	void DisableVehicle()
	{
		if (vehicle_)vehicle_->set_enable(false);
		vehicle_ = {};
	}

private:
	VehiclePtr vehicle_;
};

