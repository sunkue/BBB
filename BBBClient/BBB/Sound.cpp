#include "stdafx.h"
#include "Sound.h"
#include "AudioSystem.h"

Sound::Sound(ISound* irrklangSoundInterface)
	:Sound_{ irrklangSoundInterface }
{
}

Sound::~Sound()
{
	Sound_->drop();
}

void Sound::Restart()
{
	Sound_->setIsPaused(false);
}

void Sound::Play()
{
	AudioSystem::get().PlaySound(this);
}

void Sound::Stop()
{
	Sound_->stop();
}

void Sound::Pause()
{
	Sound_->setIsPaused(true);
}

void Sound::SetVolume(float value)
{
	Sound_->setVolume(value);
}

void Sound::SetLoop(bool loop)
{
	Sound_->setIsLooped(loop);
}

bool Sound::GetPaused() const
{
	return Sound_->getIsPaused();
}

float Sound::GetVolume() const
{
	return Sound_->getVolume();
}

void Sound::Set3DAttributes(const glm::vec3& pos)
{
	Sound_->setPosition({ pos.x, pos.y, pos.z });
}
