#include "stdafx.h"
#include "Vehicle.h"
#include "TrackNode.h"
#include "Item.h"
#include "Game.h"
#include "Event.h"
#include "ItemEvent.h"

void VehicleObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
}

void VehicleObj::save_file_impl(ofstream& file)
{
	Obj::save_file_impl(file);

}

bool VehicleObj::process_input(const KeyBoardEventManager::key_event& key)
{
	bool pressed = (GLFW_RELEASE != key.action);
	bool keydown = (GLFW_PRESS == key.action);
	bool keyrepeat = (GLFW_REPEAT == key.action);
	bool keyup = (GLFW_RELEASE == key.action);


	if (Game::get().isEditMode())
	{
		switch (key.key)
		{
		case GLFW_KEY_UP:
		{
			front_on_ = pressed;
		}
		CASE GLFW_KEY_DOWN :
		{
			back_on_ = pressed;
		}
		CASE GLFW_KEY_LEFT :
		{
			left_on_ = pressed;
		}
		CASE GLFW_KEY_RIGHT :
		{
			right_on_ = pressed;
		}
		CASE GLFW_KEY_LEFT_CONTROL :
		{
			brake_on_ = pressed;
		}
		CASE GLFW_KEY_SPACE :
		{
			item_use_on_ = keyup;
		}
		CASE GLFW_KEY_LEFT_SHIFT :
		{
			draft_on_ = pressed;
		}
		CASE GLFW_KEY_U :
		{
			if (keydown)
				get_Audio()->PlaySound("idle3.wav", true);
		}
		CASE GLFW_KEY_I :
		{
			if (keydown)
				get_Audio()->PlaySound("idle1.mp3", false, 1, true);
		}
		CASE GLFW_KEY_O :
		{
			if (keydown)
				get_Audio()->PlaySound("idle2.wav");
		}
		CASE GLFW_KEY_P :
		{
			if (keydown)
				EventManager::get().Add((make_shared<GameClearEvent>(10000ms)));
		}

		CASE GLFW_KEY_APOSTROPHE : // '
		{
			if (pressed)
			{
				clear_lab();
				clear_lab();
				clear_lab();
				clear_lab();
				Renderer::get().changeSceneWithLoading(&GameEndScene::get());
			}
		}
		break; default: return false;
		}
	}

	if (Game::get().isPlayMode())
	{
		if (keyrepeat)
			return true;

		switch (key.key)
		{
		case GLFW_KEY_UP:
		{
			front_on_ = pressed;
		}
		CASE GLFW_KEY_DOWN :
		{
			back_on_ = pressed;
		}
		CASE GLFW_KEY_LEFT :
		{
			left_on_ = pressed;
		}
		CASE GLFW_KEY_RIGHT :
		{
			right_on_ = pressed;
		}
		CASE GLFW_KEY_LEFT_CONTROL :
		{
			brake_on_ = pressed;
		}
		CASE GLFW_KEY_SPACE :
		{
			// 조준
			if (keydown)
			{

			}

			if (keyup)
			{
				item_use_on_ = true;
			}
		}
		CASE GLFW_KEY_LEFT_SHIFT :
		{
			draft_on_ = pressed;
		}
		CASE GLFW_KEY_TAB :
		{
			if (keydown)
			{
				for (auto& c : GameScene::get().get_cars())
					c->get_boundings().ToggleShow();
			}
		}
		CASE GLFW_KEY_Q :
		{
			if (keydown)
			{
				Track::get().set_draw(!Track::get().get_draw());
			}
		}
		break; default: return false;
		}
	}

	return true;
}

#define CHECK_NAN(v)								\
{													\
	auto res = glm::isnan(v);						\
	if (res.x || res.y || res.z)					\
	{												\
		cout << #v << __LINE__ << __func__ << endl;		\
	}												\
}

void VehicleObj::update(milliseconds time_elapsed)
{
	if (!get_enable())
		return;

	if (2 < glm::length(get_linear_speed()))
	{
		if (0 < angular_speed_.y)
			get_animator()->PlayAnimation(get_animations()["D_L"]);
		else if (angular_speed_.y < 0)
			get_animator()->PlayAnimation(get_animations()["D_R"]);
		else
			get_animator()->PlayAnimation(get_animations()["D"]);
	}
	else
	{
		if (0 < angular_speed_.y)
			get_animator()->PlayAnimation(get_animations()["IDLE_L"]);
		else if (angular_speed_.y < 0)
			get_animator()->PlayAnimation(get_animations()["IDLE_R"]);
		else
			get_animator()->PlayAnimation(get_animations()["IDLE"]);
	}



	update_state();

	DynamicObj::update(time_elapsed);

	if (item_use_on_)
	{
		item_use_on_ = false;

		if (Game::get().isEditMode())
		{
			OnUseItem();
		}
		else
		{
			if (!item_list_.itemQ.empty())
			{
				//for valid check..
				auto itemtype = item_list_.itemQ.front()->get_type();
				cs_use_item use_item;
				use_item.itemtype = static_cast<decltype(use_item.itemtype)>(itemtype);
				Networker::get().do_send(&use_item);
			}
		}
	}

	if (outed_)
	{
		rimcolor = lerp(rimcolor, glm::vec3{ 1,0,0 }, time_elapsed.count() / 1000.f);
		if (0 <= Track::get().check_include(*this))
		{
			outed_ = false;
		}
	}
	else
	{
		rimcolor = lerp(rimcolor, glm::vec3{ 0 }, time_elapsed.count() / 1000.f * 2);
	}


	// add particles
	if (draft_on_ || boost_)
	{
		static glm::vec3 prevPos = get_position();
		glm::vec3 newPos = get_position() - get_head_dir() * 3;
		if (glm::length(prevPos - newPos) < 3)
			return;
		prevPos = newPos;

		static RandomEngine RandomEngine;
		auto r = GetRandomValueZeroToOne(RandomEngine.random);
		r = r * 2.f - 1.f;
		newPos += r * 0.4f * get_right_dir();

		auto& particles = GameScene::get().get_particles();
		static auto t = Texture::create(load_texture_file("smoke"s + ".png"s, "./Resource/Texture/particle"s));
		particles.emplace_back
		(
			newPos + glm::vec3{ 0,0.5,0 },
			get_scale() * 5,
			glm::vec3{ 1 },
			t,
			1s
		);

		if (50 < GameScene::get().get_particles().size())
		{
			particles.erase(particles.begin());
		}
	}
}

void VehicleObj::update_speed(float time_elapsed)
{
	auto head_dir = get_head_dir();
	CHECK_NAN(get_rotation());
	CHECK_NAN(head_dir);
	CHECK_NAN(linear_speed_);

	/* fric */
	auto prev_dir = glm::normalize(linear_speed_);
	auto prev_speed = glm::length(linear_speed_);
	if (prev_speed < glm::epsilon<float>())
	{
		prev_dir = V_ZERO;
	}
	auto fric = -1 * prev_dir * friction_ * time_elapsed;
	CHECK_NAN(fric);
	/* accel */
	if (draft_on_)
	{
		head_dir = glm::normalize(linear_speed_);
		draft_time_ = std::min(draft_time_ + time_elapsed, 3.f);
		CHECK_NAN(head_dir); // nan.
	}
	else
	{
		head_dir *= draft_time_;
		draft_time_ = std::max(draft_time_ - time_elapsed, 1.f);
		CHECK_NAN(head_dir);
	}
	if (boost_)
	{
		acceleration_ = 1;
	}
	auto accel = head_dir * acceleration_ * time_elapsed * draft_time_;
	CHECK_NAN(accel);

	///* accumulate */
	auto new_linear_speed = linear_speed_ + accel + fric;
	CHECK_NAN(new_linear_speed);

	/* no backward movement */
	auto new_dir = glm::normalize(new_linear_speed);
	auto new_speed = glm::length(new_linear_speed);
	if (new_speed < ellipsis)
	{
		new_dir = V_ZERO;
	}
	CHECK_NAN(new_dir);

	auto maxspeed = max_speed_;
	if (boost_)
	{
		maxspeed *= 1.2;
		new_linear_speed += accel * 20;
	}

	/* overspeed */
	bool over_speed = maxspeed < new_speed;

	// [B] if (over_speed) { new_linear_speed = new_dir * _max_speed; }
	new_linear_speed -= new_linear_speed * over_speed;
	new_linear_speed += new_dir * maxspeed * over_speed;
	CHECK_NAN(new_linear_speed);

	linear_speed_ = new_linear_speed;
	linear_speed_.y = 0;
	CHECK_NAN(linear_speed_);
}



void VehicleObj::update_uniform_vars(const ShaderPtr& shader) const
{
	if (!get_enable())return;
	Obj::update_uniform_vars(shader);
	shader->use();
	shader->set("u_rimcolor", rimcolor);
	shader->set("u_rimpower", rimpower);
	shader->set("u_rimboundary", rimboundary);
	glUseProgram(0);
}

void VehicleObj::on_collide(Obj& other, OUT void* context)
{
	if (&other == this)
		return;

	if (shield_) return;
RE:
	if (collision_detect(other))
	{
		auto d = glm::normalize(other.get_position() - get_position());
		d.y = 0;
		move(-d * 0.001);
		if (glm::epsilon<float>() < glm::length(d))
			goto RE;
	}

	if (auto car = dynamic_cast<VehicleObj*>(&other))
	{
		if (shield_) return;
		set_force(get_force() + car->get_position());
		set_force2(get_force2() + car->get_linear_speed());
		set_collided(true);
		auto v = glm::length(get_linear_speed()) / max_speed_;
		get_Audio()->PlaySound("collision.wav", true, v * v * 0.6);
		return;
	}

	if (auto item = dynamic_cast<ItemObj*>(&other))
	{
		return;
	}

	{
		if (shield_) return;
		auto force = other.get_position();
		force.y = get_position().y;
		set_force(get_force() + force);

		if (dynamic_cast<AutoVehicleObj*>(this))
		{
			auto& node = Track::get().get_tracks()[included_node_];
			auto next = node->get_next_nodes()[0];
			for (int i = 1; i < 4; i++)
				next = next->get_next_nodes()[0];
			auto t = 10 * glm::normalize(next->get_position() - get_position());
			set_force2(get_force2() + t);
		}
		else 
			set_force2(get_force2() - get_linear_speed() * 0.5);
		
		set_collided(true);
		auto v = glm::length(get_linear_speed()) / max_speed_;
		get_Audio()->PlaySound("collision.wav", true, v * v * 0.6);
		return;
	}
}

void VehicleObj::draw_gui()
{
	gui::Begin("Vehicle");
	gui::DragFloat3("rimcolor", glm::value_ptr(rimcolor), 0.01, 0, 1);
	gui::DragFloat("rimpower", &rimpower, 0.5, 0, 50);
	gui::DragFloat("rimboundary", &rimboundary, 0.0125, 0, 1);

	gui::Text("This is Vehicle in real game.");
	gui::DragInt("ID", (int*)&id_);
	gui::DragInt("rank", &rank_);
	gui::DragInt("cp", (int*)&check_point_);
	gui::DragInt("lab", &lab_);
	gui::DragInt("node", &included_node_);
	gui::DragFloat3("linear_speed", glm::value_ptr(linear_speed_));
	auto speed = glm::length(linear_speed_);
	gui::DragFloat("speed", &speed);
	GUISAVE(); GUILOAD();
	gui::SliderFloat("max_speed", &max_speed_, 1.0f, 100.0f);
	gui::DragFloat("angular_power", &angular_power_, 0.125, 0.5, 5);
	gui::DragFloat("acceleration_power", &acceleration_power_, 1, 10, 50);
	gui::DragFloat("friction_power", &friction_power_, 0.125, 0.125, 15);
	gui::Text("state");
	gui::SliderFloat("acceleration", &acceleration_, -max_speed_, max_speed_);
	gui::SliderFloat("friction", &friction_, 0, max_speed_);
	gui::Checkbox("front_on", &front_on_);
	gui::Checkbox("back_on", &back_on_);
	gui::Checkbox("right_on", &right_on_);
	gui::Checkbox("left_on", &left_on_);
	gui::Checkbox("brake_on", &brake_on_);
	gui::Checkbox("draft_on", &draft_on_);
	gui::End();

	Obj::draw_gui();
}

void VehicleObj::draw(const ShaderPtr& shader) const
{
	//Obj::draw(shader);
	if (!get_enable())return;

	const unordered_set<string_view> wheels
	{
		"Tire_FL2SG.002"sv,
		"Breake_Pad_FRSG.002"sv,
		"Breake_Pad_FRSG1.002"sv,
		"F6EngineSG1.002"sv,
		"polySurface19SG1.002"sv,
		"polySurface47SG.002"sv,
	};

	// 회전 시키기 () uniform model_mat 다시 셋..
//	get_model()->draw_without(wheels, shader);
	glm::vec2 offset = glm::vec2{ 1, 0 } *glm::length(this->get_linear_speed()) * (1 - brake_on_) * (1 - draft_on_) * 0.133f * glm::sin(TimerSystem::get().game_time().count());
	glm::mat4 tireRotateMat = glm::rotate(offset.x * 30, Z_DEFAULT);

	get_model()->draw_with_f("Tire_FL2SG.002"sv, shader, [tireRotateMat, offset, shader]()
		{
			shader->use();
			shader->set("u_texcoord_offset", offset);
			// shader->set("u_sub_model_mat", tireRotateMat);
		},
		[shader]()
		{
			shader->use();
			shader->set("u_texcoord_offset", glm::vec2{ 0 });
			shader->set("u_sub_model_mat", glm::mat4());
		});

	boundings_.draw(shader);

	//	get_model()->draw_only("Lights1SG.002"sv, shader);
	item_list_.draw(*this, shader);
}


void VehicleObj::update_state()
{
	const glm::vec3 _angular_power = Y_DEFAULT * angular_power_;
	const float _acceleration_power = acceleration_power_;
	const float _friction_power = friction_power_;

	angular_speed_ = _angular_power * static_cast<int>(angular_control_);
	acceleration_ = _acceleration_power * static_cast<int>(accel_control_);

	// [B] if(_brake_on)_acceleration = 0;
	//_acceleration -= _acceleration * _brake_on;

	friction_ = _friction_power;
	// [B] if(_brake_on)_friction += _friction_power;
	friction_ += _friction_power * draft_on_ * 7;
	//glm::length(get_linear_speed());

	friction_ += _friction_power * brake_on_ * 2;

	int accel_control{ static_cast<int>(CONTROLL::none) };
	int angular_control{ static_cast<int>(CONTROLL::none) };
	accel_control += front_on_;
	accel_control -= back_on_;
	angular_control -= right_on_;
	angular_control += left_on_;
	accel_control_ = static_cast<CONTROLL>(accel_control);
	angular_control_ = static_cast<CONTROLL>(angular_control);
}
#include "UI.h"
void VehicleObj::reset1()
{
	front_on_ = false;
	back_on_ = false;
	right_on_ = false;
	left_on_ = false;
	brake_on_ = false;
	draft_on_ = false;
	item_use_on_ = false;
	draft_time_ = 1;
	rimcolor = glm::vec3{};
	item_list_.Pop();
	item_list_.Pop();
	item_list_.Pop();
	boost_ = false;
	duple_ = false;
	shield_ = false;
}
void VehicleObj::reset2()
{
	included_node_ = 0;
	rank_ = 1;
	lab_ = -1;
	clear_time_ = {};
	linear_speed_ = { glm::epsilon<float>() / 1.25,0,0 };
	angular_speed_ = {};
	GameUI::get().get_textUi().countdown_ = -1;


	auto& b = const_cast<Boundings&>(get_boundings());
	b = Boundings();
	load("car" + to_string(id_));
	b.load("carbounding");

	Track::get().init_include_obj(*this, true);
	set_enable(false);
}

void VehicleObj::update_camera(Camera* camera, float time_elpased) const
{
	if (get_collided())
	{
		// camera->camera_shake(glm::length(linear_speed_) * 0.01f);
	}

	if (boost_)
	{
		const float targetfovy = 80.f;
		auto fovy = camera->get_fovy();
		fovy = std::lerp(fovy, targetfovy, (time_elpased));
		camera->set_fovy(fovy);
	}
	else
	{
		const float targetfovy = 45.f;
		auto fovy = camera->get_fovy();
		fovy = std::lerp(fovy, targetfovy, (time_elpased * 2));
		camera->set_fovy(fovy);
	}

	glm::vec3 target_dir; // using for target & position
	/* target */
	{
		const auto head_dir = get_head_dir();
		const auto moving_speed = get_linear_speed();
		const auto moving_len = glm::length(moving_speed);
		auto moving_dir = glm::normalize(moving_speed);
		if (moving_len < 0.125f)
		{
			moving_dir = head_dir;
		}
		target_dir = glm::lerp(head_dir, moving_dir, clamp(0.2f * moving_len, 0.00f, 0.4f));
		static float YY = 2.f;
		YY = boost_ ? lerp(YY, 4.f, time_elpased * 2) : lerp(YY, 2.f, time_elpased);
		camera->set_target(get_position() + target_dir * 8.0f + Y_DEFAULT * YY);
	}

	/* position */
	{
		auto diff = camera->get_diff();
		auto rotate = quat_from2vectors({ diff.x, 0,diff.z }, { -target_dir.x, 0, -target_dir.z });
		const auto trans_diff = glm::translate(diff);
		const auto trans = glm::translate(get_position());
		const auto scale = glm::scale(get_scale());
		auto m = trans * glm::toMat4(rotate) * scale * trans_diff;
		camera->set_position(m * V4_DEFAULT);
	}


}

void VehicleObj::regenerate()
{
	Track::get().include_obj(*this, true);
	const float speed{ 1 }; //  no 0.
	auto& node = Track::get().get_tracks()[included_node_];
	linear_speed_ = speed * node->get_front();
	auto q = glm::rotation(get_head_dir(), node->get_front());
	rotate(q);
}

void VehicleObj::regenerate(int nodeid)
{
	Track::get().include_obj(nodeid, *this, true);
	const float speed{ 1 }; //  no 0.
	auto& node = Track::get().get_tracks()[included_node_];
	linear_speed_ = speed * node->get_front();
	auto q = glm::rotation(get_head_dir(), node->get_front());
	outed_ = false;
	rotate(q);
}

bool VehicleObj::rank_worse_than(VehicleObj& other)
{
	if (!get_enable())
		return true;

	//lab
	if (lab_ < other.lab_)
	{
		return true;
	}
	else if (lab_ > other.lab_)
	{
		return false;
	}

	// same lab
	// node
	auto& nodes = Track::get().get_tracks();
	auto& this_node = nodes.at(included_node_);
	auto& other_node = nodes.at(other.included_node_);
	if (this_node->get_from_start() < other_node->get_from_start())
	{
		return true;
	}
	else if (this_node->get_from_start() > other_node->get_from_start())
	{
		return false;
	}
	// same lab
	// same node
	// pos
	auto next_pos = this_node->get_next_center();
	for (auto next : this_node->get_next_nodes())
	{
		next_pos += next->get_next_center();
	}
	next_pos /= this_node->get_next_nodes().size() + 1;

	auto this_diff = next_pos - get_position();
	auto other_diff = next_pos - other.get_position();
	return glm::length(other_diff) < glm::length(this_diff);
}

void VehicleObj::clear_lab()
{
	lab_ += 1;
	get_Audio()->PlaySound("bell3.wav", true, 0.7);
	if (3 <= lab_)
		clear_time_ = TimerSystem::get().game_time();
};

bool VehicleObj::TryAcquireItem(ItemObj& itemObj)
{
	duple_ = false;
	auto item = itemObj.to_instance();
	auto ret = item_list_.TryPush(item);
	if (ret)get_Audio()->PlaySound("item.wav", true, 0.7);
	return ret;
}

void VehicleObj::OnUseItem()
{
	optional<ItemInstancePtr> item = item_list_.Pop();
	if (!item.has_value())
		return;

	auto usingItem = item.value();
	// usingItem
	EventManager::get().Add(ItemEventFactory::createEvent(usingItem, nullptr, this));
}

bool VehicleObj::NetTryAcquireItem(ItemType itemtype, ObjID itemid)
{
	duple_ = false;
	auto item = ItemInstance::create(itemtype);
	for (auto& item : Track::get().get_items())
	{
		if (itemid == item->get_objid())
		{
			get_Audio()->PlaySound("item.wav", true, 0.7);
			AudioSystem::get().ForceUpdate();
			item->set_enable(false);
			break;
		}
	}
	return item_list_.TryPush(item);
}

ItemType VehicleObj::NetUseItem()
{
	optional<ItemInstancePtr> item = item_list_.Pop();

	if (!item.has_value())
	{
		cerr << "[ERR]VehicleObj::NetUseItem" << endl;
		terminate();
		// return;
	}

	auto usingItem = item.value();
	auto ret = usingItem->get_type();
	EventManager::get().Add(ItemEventFactory::createEvent(usingItem, nullptr, this));
	return ret;
}

//////////////////////////////////////////////
//				
//				BACKMIRROR
//				
//////////////////////////////////////////////

void VehicleObj::BackMirror::update_camera(Camera* camera, float time_elpased) const
{
	glm::vec3 diff = camera->get_diff();
	//glm::vec3 backdir = -1 * vehicle_->get_head_dir();
	glm::vec3 vehicle_backdir = -1 * vehicle_->get_head_dir();
	glm::vec3 vehicle_postiion = vehicle_->get_position();
	auto vehicle_scale = vehicle_->get_scale();

	// target
	camera->set_target(vehicle_postiion + vehicle_backdir * 100.f + 0.2 * Y_DEFAULT);

	// position // 수정해야함
	{
		auto rotate = quat_from2vectors({ diff.x, 0,diff.z }, { vehicle_backdir.x, 0, vehicle_backdir.z });
		const auto trans_diff = glm::translate(diff);
		const auto trans = glm::translate(vehicle_postiion);
		const auto scale = glm::scale(vehicle_scale);
		auto m = trans * glm::toMat4(rotate) * scale * trans_diff;
		camera->set_position(m * V4_DEFAULT);
	}

	//camera->set_position(vehicle_postiion + diff);
}

void VehicleObj::use_duple()
{
	duple_ = true;
	if (Game::get().isEditMode())
		item_list_.UseDuple();
}


/*=================================================
*
*				AutoVehicleObj
*
==================================================*/

void AutoVehicleObj::save_file_impl(ofstream& file)
{
	Obj::save_file_impl(file);
	SAVE_FILE(file, PredictForwardNodeCount_);
	SAVE_FILE(file, MoveHeadDirBlendAlpha_);
	SAVE_FILE(file, StreightMoveBelowValue_);
}

void AutoVehicleObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
	LOAD_FILE(file, PredictForwardNodeCount_);
	LOAD_FILE(file, MoveHeadDirBlendAlpha_);
	LOAD_FILE(file, StreightMoveBelowValue_);
}

void AutoVehicleObj::update(milliseconds time_elapsed)
{
	DoAi(time_elapsed);
	VehicleObj::update(time_elapsed);
}

void AutoVehicleObj::draw_gui()
{
	gui::Begin("AI");
	gui::Checkbox("Ai", &AiOn_); gui::SameLine();
	if (gui::SmallButton("AllAiToggle"))
	{
		for (auto& c : GameScene::get().get_cars())
		{
			if (auto ac = dynamic_cast<AutoVehicleObj*>(c.get()))
			{
				ac->AiOn_ = !ac->AiOn_;
			}
		}
	}
	gui::DragInt("PredictNodeAfter", &PredictForwardNodeCount_, 1, 0.6, 15);
	gui::DragFloat("MoveHeadBlendAlpha", &MoveHeadDirBlendAlpha_, 0.01, 0, 1, "%.2f");
	gui::DragFloat("StreightUnderValue", &StreightMoveBelowValue_, 0.1, 0, 1, "%.1f");
	gui::DragFloat("Value", &JugmentValue);
	gui::End;
	VehicleObj::draw_gui();
}

void AutoVehicleObj::DoAi(milliseconds time_elapsed)
{
	if (!AiOn_)return;
	// 상태 변경
	included_node_ = Track::get().find_closest_track(*this);
	auto& node = Track::get().get_tracks()[included_node_];
	auto next = node->get_next_nodes()[0];
	for (int i = 1; i < PredictForwardNodeCount_; i++)
		next = next->get_next_nodes()[0];

	auto target = next->get_next_center();

	auto targetDir = glm::normalize(target - get_position());
	auto movingDir = get_moving_dir();
	auto headDir = get_head_dir();
	//auto movingDir = get_head_dir();
	auto predictDir = glm::lerp(headDir, movingDir, MoveHeadDirBlendAlpha_);
	/* 좌우 판별 */
	JugmentValue = glm::dot(Y_DEFAULT, glm::cross(targetDir, predictDir));
	if (0 < JugmentValue)
	{
		//cout << "RR" << endl;
		right_on_ = true;
		left_on_ = false;
		back_on_ = true;
		front_on_ = false;
	}
	else
	{
		//	cout << "LL" << endl;
		right_on_ = false;
		left_on_ = true;
		back_on_ = true;
		front_on_ = false;
	}

	if (abs(JugmentValue) < StreightMoveBelowValue_)
	{
		//	cout << ".." << endl;
		front_on_ = true;
		back_on_ = false;
		right_on_ = false;
		left_on_ = false;
		item_use_on_ = true;
	}
	else
	{
		item_use_on_ = false;
	}
}
