#pragma once
#include <unordered_map>
#include <string>
#include "Sound.h"

class AudioSystem
{
	SINGLE_TON(AudioSystem);
	~AudioSystem();
public:
	Sound* LoadSound(string_view fileName, bool Is3D = true);
	Sound* CreateSound(ISoundSource* source, bool Is3D = true);

	void PlaySound(Sound* sound);
	void ForceUpdate() { Engine->update(); } // thread
	void SetListener(const glm::vec3& pos, const glm::vec3& lookDir);
	void SetDefault3DSoundMinDistance(float x);
private:
	ISoundEngine* Engine;
	unordered_map<string, ISoundSource*> SoundSourceTable;
	vector<Sound*> Sounds;
};



