#pragma once


class KeyBoardEventManager
{
	SINGLE_TON(KeyBoardEventManager) = default;
public:
	struct key_event
	{
		int key, code, action, modifiers;
		clk::time_point time_of_event;
	};
	using key = int;
	using action = int;
	using main_key_func = std::function<bool(const key_event&)>;
	using key_func = std::function<void(const key_event&)>;
private:
	std::map<key, action> keys_;
	std::queue<key_event> key_events_;
	std::map<key, key_func> key_functions_;
private:
	main_key_func main_func_ = [](auto) { return false; };
public:
	void KeyBoard(GLFWwindow* window, int key, int code, int action, int modifiers);
	void BindKeyFunc(key key, key_func func);
	void BindMainKeyFunc(main_key_func func);
	void UnBindKeyFunc() { key_functions_.clear(); }
	void ProcessInput();
};


