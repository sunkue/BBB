#pragma once

#include "FileHelper.h"


/// /////////////////////////////////////////////////////////////////////////

#pragma warning(push)
#pragma warning(disable: 26812)
#pragma warning(disable: 26495)
BETTER_ENUM
(
	SHADER_MODE, int
	, NONE
	, SHAPEN
	, SHAPEN2
	, BLUR
	, BLUR2
	, EDGE
);
#pragma warning(pop)

class Obj;

/* 소유된 (붙은) 카메라 */
using CameraPtr = shared_ptr<class Camera>;
class Camera : public IDataOnFile
{
protected:
	virtual void save_file_impl(ofstream& file) final;
	virtual void load_file_impl(ifstream& file) final;

public:
	SHADER_MODE mode{ SHADER_MODE::NONE };

public:
	void update(float time_elapsed);

	glm::mat4 view_mat()const { return glm::lookAt(position_, target_, up_); };

	glm::vec3 get_look_dir()const { return glm::normalize(target_ - position_); };
	glm::vec3 get_right()const { return glm::normalize(glm::cross(up_, -get_look_dir())); };

	void draw_gui(string_view name)
	{
		if (!enable_) return;
		gui::Begin(name.data());

		GUISAVE();
		GUILOAD();

		if (gui::Button(+mode._to_string()))
		{
			if (mode._size() == mode._value + 1)
			{
				mode = SHADER_MODE::NONE;
			}
			else
			{
				mode._value++;
			}
		}
		gui::DragFloat3("diff", glm::value_ptr(diff_));
		gui::DragFloat("fovy", &fovy_);

		gui::DragFloat3("target", glm::value_ptr(target_), 0.001);
		gui::DragFloat3("pos", glm::value_ptr(position_));
		gui::End();
	}

	SET(ownner);
	GET(ownner);

	SET(diff);
	GET(diff);

	SET(target);
	GET(target);

	SET(up);
	GET(up);

	GET(position);
	SET(position);

public:
	GET(shaking);
	GET(shaking_time);
	void camera_shake(float duration) { shaking_ = true; shaking_time_ = duration; };
private:
	bool shaking_ = false;
	float shaking_time_ = 0.f;

public:
	GET(enable);
	SET(enable);
	GET(fovy);
	SET(fovy);
private:
	bool enable_ = true;

	Obj* ownner_ = nullptr;

	glm::vec3 diff_{};
	GLfloat fovy_{ 45.0f };
	glm::vec3 position_{ V_ZERO };
	glm::vec3 target_{ V_ZERO };
	glm::vec3 up_{ Y_DEFAULT };
};


/// /////////////////////////////////////////////////////////////////////////
