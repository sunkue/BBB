#include "stdafx.h"
#include "Terrain.h"

/////////////////////////////////////////////////////////

HeightMapImage::HeightMapImage(
	string_view fileName
	, int width
	, int length
	, glm::vec3 scale
)
	: Width_{ width }
	, Length_{ length }
	, Scale_{ scale }
{
	const int imageSize{ width * length };
	unique_ptr<HeightRawMap_t[]> HMPixels = make_unique<HeightRawMap_t[]>(imageSize);
	ifstream image{ fileName.data() , ios::binary };
	image.read(reinterpret_cast<char*>(HMPixels.get()), imageSize * sizeof(HeightRawMap_t));

	HeightMapPixels_ = make_unique<HeightRawMap_t[]>(imageSize);
	for (size_t y = 0; y < length; y++) {
		for (size_t x = 0; x < width; x++) {
			HeightMapPixels_[x + (((size_t)length - 1 - y) * width)]
				= HMPixels[x + ((size_t)y * width)];
		}
	}
}

#define WITH_APPROXIMATE_OPPOSITE_CORNER
float HeightMapImage::GetHeight(const float fx, const float fz)const
{
	if (OutOfRange(fx, fz))return -50.0f;

	size_t const x = static_cast<size_t>(fx);
	size_t const z = static_cast<size_t>(fz);
	float const xFract = fx - x;
	float const zFract = fz - z;

	float BL{ static_cast<float>(HeightMapPixels_[(x + 0) + ((z + 0) * Width_)]) };
	float BR{ static_cast<float>(HeightMapPixels_[(x + 1) + ((z + 0) * Width_)]) };
	float TL{ static_cast<float>(HeightMapPixels_[(x + 0) + ((z + 1) * Width_)]) };
	float TR{ static_cast<float>(HeightMapPixels_[(x + 1) + ((z + 1) * Width_)]) };
#ifdef WITH_APPROXIMATE_OPPOSITE_CORNER
	const bool R2L{ (z % 2) != 0 };
	if (R2L) {
		if (xFract <= zFract)BR = BL + (TR - TL);
		else TL = TR + (BL - BR);
	}
	else {
		if (zFract < (1.0f - xFract))TR = TL + (BR - BL);
		else BL = TL + (BR - TR);
	}
#endif // WITH_APPROXIMATE_OPPOSITE_CORNER
	float const THeight{ lerp(TL, TR, xFract) };
	float const BHeight{ lerp(BL, BR, xFract) };
	float const Height{ lerp(BHeight, THeight, zFract) };
	return Height;
}

glm::vec3 HeightMapImage::GetNormal(const int x, const int z)const
{
	if (OutOfRange(x, z))return { 0.0f,1.0f,0.0f };

	int HMIndex{ x + (z * Width_) };
	int xHMAdd{ (x < Width_ - 1) ? (1) : (-1) };
	int zHMAdd{ (z < Length_ - 1) ? (Width_) : (-Width_) };
	float y1{ static_cast<float>(HeightMapPixels_[static_cast<size_t>(HMIndex)]) * Scale_.y };
	float y2{ static_cast<float>(HeightMapPixels_[static_cast<size_t>(HMIndex) + xHMAdd] * Scale_.y) };
	float y3{ static_cast<float>(HeightMapPixels_[static_cast<size_t>(HMIndex) + zHMAdd] * Scale_.y) };
	glm::vec3 edge1{ 0.0f,y3 - y1,Scale_.z };
	glm::vec3 edge2{ Scale_.x,y2 - y1,0.0f };
	glm::vec3 Normal{ glm::normalize(glm::cross(edge1, edge2)) };
	return Normal;
}


/////////////////////////////////////////////


HeighMapGridMesh::HeighMapGridMesh(string_view fileName, int width, int length, glm::vec3 scale) : Mesh{}
{

	HeightMapImage_ = make_unique<HeightMapImage>(fileName, width, length, scale);
	// vertex 
	auto verticesCount = width * length;
	vertices_.resize(verticesCount);
	float height{ 0.0f };
	for (int i = 0, z = 0; z < length; z++) {
		for (int x = 0; x < width; x++, i++) {
			glm::vec3 pos{ scale.x * x, GetHeight(x,z),scale.z * z };
			glm::vec3 nor{ HeightMapImage_->GetNormal(x,z) };
			glm::vec2 tex{ x % 2 , z % 2 };
			vertices_[i] = Vertex{ pos, nor, tex };
		}
	}

	/// index 
	auto indicesCount = (width * 2) * (length - 1) + (length - 1 - 1);
	indices_.resize(indicesCount);
	for (int j = 0, z = 0; z < length - 1; z++) {
		if ((z % 2) == 0) {
			for (int x = 0; x < width; x++) {
				UINT const index{ static_cast<UINT>(x) + (z * width) };
				if ((x == 0) && (0 < z))indices_[j++] = index;
				indices_[j++] = index;
				indices_[j++] = index + width;
			}
		}
		else {
			for (int x = width - 1; 0 <= x; x--) {
				UINT const index{ static_cast<UINT>(x) + (z * width) };
				if (x == (width - 1))indices_[j++] = index;
				indices_[j++] = index;
				indices_[j++] = index + width;
			}
		}
	}

	setup_mesh();
}


float HeighMapGridMesh::GetHeight(int x, int z)const
{
	HeightRawMap_t* HMPixels{ HeightMapImage_->GetPixels() };
	glm::vec3 scale{ HeightMapImage_->get_Scale() };
	int width{ HeightMapImage_->get_Width() };
	float height{ HMPixels[x + (z * width)] * scale.y };
	return height;
}

/////////////////////////////////////////////////////////

Terrain::Terrain(string_view fileName, int width, int length, glm::vec3 scale)
	: Width_{ width }
	, Length_{ length }
	, Scale_{ scale }
	, HeightMapGridMesh_{ fileName,width,length,scale }
{
	AlbedoTex_ = Texture::create(load_texture_file("Terrain/Grass001_1K-JPG/Grass001_1K_Color.jpg"s, "./Resource/Texture"s, true));
	NormalTex_ = Texture::create(load_texture_file("Terrain/Grass001_1K-JPG/Grass001_1K_NormalGL.jpg"s, "./Resource/Texture"s, true));
	AlbedoTex_ = Texture::create(load_texture_file("Terrain/Grass002_1K-JPG/Grass002_1K_Color.jpg"s, "./Resource/Texture"s, true));
	NormalTex_ = Texture::create(load_texture_file("Terrain/Grass002_1K-JPG/Grass002_1K_NormalGL.jpg"s, "./Resource/Texture"s, true));
}

float Terrain::GetWorldHeight(float x, float z) const
{
	return 0;
}

void Terrain::update_uniform_vars(const ShaderPtr& shader) const
{
	shader->use();
	glm::mat4 m = model_mat();
	shader->set("u_m_mat", m);
	glActiveTexture(GL_TEXTURE0);
	shader->set("u_material.albedo1", AlbedoTex_);
	glActiveTexture(GL_TEXTURE1);
	shader->set("u_material.normalmap1", NormalTex_);
	glUseProgram(0);
}

void Terrain::draw(const ShaderPtr& shader) const
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL - 1);
	HeightMapGridMesh_.draw(shader, GL_TRIANGLE_STRIP);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Terrain::draw_gui()
{
	gui::Begin("Terrain");
	gui::DragFloat3("translate", glm::value_ptr(translate_), 0.0625);
	gui::End();
}

