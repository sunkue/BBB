#include "stdafx.h"
#include "DynamicObj.h"
#include "KeyboardEvent.h"
#include "Camera.h"
#include "Game.h"

void DynamicObj::update_movement(float time_elapsed)
{
	if (get_collided())
	{
		auto others_pos = get_force();
		auto diff = others_pos - get_position();
		diff.y = 0;
		int backed = 0;
		auto speed = glm::length(linear_speed_);
		/*
	RE:
		for (auto& other : GameScene::get().get_cars())
		{
			if (collision_detect(*other))
			{
				auto d = glm::normalize(other->get_position() - get_position());
				move(-d * time_elapsed);
				if (glm::epsilon<float>() < glm::length(d))
					goto RE;
			}
		}
	   */
	   // v1 = vsinTH1 + vcosTH1
	   // v2 = vsinTH2 + vcosTH2
	   // (other_getpos - pos) & linear_speed => ����. =>TH2
		constexpr float e = 0.5f;

		//if (get_objid() == Game::get().playerid)
		{
			//	linear_speed_ -= ((1 + e) / 2) * (linear_speed_ + glm::normalize(diff) * speed * 0.5f);
		}
		//else
		{
			linear_speed_ -= ((1 + e) / 2) * (linear_speed_ - get_force2());
			move(linear_speed_ * time_elapsed);
		//	move(linear_speed_ * time_elapsed);
		//	move(linear_speed_ * time_elapsed);
		}
		set_force(glm::vec3{});
		set_force2(glm::vec3{});
		set_collided(false);
	}
	else
		move(linear_speed_ * time_elapsed);
}
