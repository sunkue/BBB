#pragma once
#include "Shader.h"
#include "texture.h"
#include "Light.h" 
#include "Obj.h"

class SceneHelper;

enum class SceneType
{
	None
	, Game
	, Title
	, Matching
};

////////////////////////////////////////////////////////////////

class Scene
{
public:
	virtual void enterScene() {};
	virtual void draw() {};
	virtual void update(milliseconds) {};
	virtual void leaveScene();
};

////////////////////////////////////////////////////////////////

class TitleScene : public Scene
{
	SINGLE_TON(TitleScene) = default;
public:
	virtual void enterScene() final;
	virtual void draw() final;
	virtual void update(milliseconds elapsed) final;
	virtual void leaveScene() final;
public:
	enum class SelectedButton
	{
		none = 0
		, play = 1
		, edit = 2
	} selected_button_{ SelectedButton::none };
};

////////////////////////////////////////////////////////////////

class MatchingScene : public Scene
{
	SINGLE_TON(MatchingScene) = default;
public:
	virtual void enterScene() final;
	virtual void draw() final;
	virtual void update(milliseconds elapsed) final;
	virtual void leaveScene() final;
	void ready_draw();
	void draw_gui();
	struct MatchingChair
	{
		ObjPtr vehicle;
		SpotLightPtr spot;
	};

private:
	array<MatchingChair, MAX_PLAYER> chairs_;
	ShaderPtr shader_;
	CameraPtr camera_;
	ObjPtr floor_;
};


////////////////////////////////////////////////////////////////

class GameEndScene : public Scene
{
	SINGLE_TON(GameEndScene) = default;
public:
	virtual void enterScene() final;
	virtual void draw() final;
	virtual void update(milliseconds elapsed) final;
	virtual void leaveScene() final;
private:
};

////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////

class LoadingScene : public Scene
{
	SINGLE_TON(LoadingScene);
public:
	virtual void enterScene() final;
	virtual void draw() final;
	virtual void update(milliseconds elapsed) final;
	virtual void leaveScene() final;
public:
	Scene* prevScene{};
	Scene* nextScene{};
	const milliseconds loadingTime{ 1s };

private:
public:
	bool closed = false;
	milliseconds loadingTimeInterVal{};
	ShaderPtr shader_;

	// 2~-1
	float size_{};
	glm::vec3 color_{};
	GLuint quad_vao_;
};

////////////////////////////////////////////////////////////////
