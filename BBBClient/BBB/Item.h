#pragma once
#include "Obj.h"

struct TimerEvent
{
	milliseconds begin_time;
	void* context;
};

#pragma warning(push)
#pragma warning(disable: 26812)
#pragma warning(disable: 26495)
BETTER_ENUM
(
	ItemType, int
	, rocket = 0
	, rocket3
	, swap
	, steal

	, shield
	, repair
	, boost
	, duple
);
#pragma warning(pop)

struct ItemTypeHelper
{
	inline static ModelPtr toItemObj(ItemType type)
	{
		switch (type)
		{
		case ItemType::rocket:
		{
			return Model::item_rocket();
		}
		CASE ItemType::rocket3 :
		{
			return Model::item_rocket3();
		}
		CASE ItemType::swap :
		{
			return Model::item_swap();
		}
		CASE ItemType::steal :
		{
			return Model::item_steal();
		}
		CASE ItemType::shield :
		{
			return Model::item_shield();
		}
		CASE ItemType::repair :
		{
			return Model::item_repair();
		}
		CASE ItemType::boost :
		{
			return Model::item_boost();
		}
		CASE ItemType::duple :
		{
			return Model::item_duple();
		}
		break; default: cerr << "err! ItemType" << endl; break;
		}
	}

	inline static TexturePtr toTexture(ItemType type)
	{
	
		switch (type)
		{
		case ItemType::rocket:
		{
			return Texture::item_rocket();
		}
		CASE ItemType::rocket3 :
		{
			return Texture::item_rocket3();
		}
		CASE ItemType::swap :
		{
			return Texture::item_swap();
		}
		CASE ItemType::steal :
		{
			return Texture::item_steal();
		}
		CASE ItemType::shield :
		{
			return Texture::item_shield();
		}
		CASE ItemType::repair :
		{
			return Texture::item_repair();
		}
		CASE ItemType::boost :
		{
			return Texture::item_boost();
		}
		CASE ItemType::duple :
		{
			return Texture::item_duple();
		}
		break; default: cerr << "err! ItemType" << endl; break;
		}
	}
};

using ItemInstancePtr = shared_ptr<class ItemInstance>;
using ItemObjPtr = shared_ptr<class ItemObj>;

// 맵에 떠 있는 아이템.
class ItemObj : public Obj, public RandomEngine
{
	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);
public:
	CREATE_SHARED(ItemObj);
	explicit ItemObj(const ModelPtr& model = Model::no_model())
		: Obj{ model }
	{
		const_cast<Boundings&>(get_boundings()).load("boxbounding");
		randomize_type();
	}
	virtual ~ItemObj() {}
public:
	ItemInstancePtr to_instance();
	virtual void update(milliseconds time_elapsed) override;
	virtual void on_collide(Obj& other, OUT void* context = nullptr);
	virtual void draw(const ShaderPtr& shader)const override;
	virtual void draw_gui() override;
	void init();
	void initseed(random_device::result_type seed)
	{
		RandomEngine_initseed(seed);
		randomize_type();
	}
private:
	int included_node_{ -1 };
private:
	void normalize_probability_table();
	void randomize_type();
private:
	ItemType type_{ ItemType::rocket };
	array<float, ItemType::_size()> probability_table_{ 1 };

public:
	GET(type);
	GET(included_node);
	SET(included_node);
private:
	milliseconds disabled_time_{};
	static constexpr milliseconds regen_period_{ 5000ms };

private:
	static constexpr float rotate_max_{ 1.5f };
	static constexpr milliseconds rotate_period_{ 1500ms };
	milliseconds internal_time_var_{};
	float dir{ 1.f };
};

// 획득된 아이템 인스턴스.
class ItemInstance
{
public:
	CREATE_SHARED(ItemInstance);
	ItemInstance(ItemType type) : type_{ type } { }
	ItemObjPtr to_obj(glm::vec3 pos);
	GET(type);
private:
	ItemType type_;
};

// 아이템리스트 
// 렌더러에서는 그냥 array + size
class ItemInstanceList
{
public:
	static constexpr size_t maxSize = 3;
public:
	bool TryPush(const ItemInstancePtr& item);
	optional<ItemInstancePtr> Pop();
	size_t size() const { return itemQ.size(); }
	FixedQueue<ItemInstancePtr, maxSize> itemQ;
	void UseDuple() { doubled = true; }
protected:
	bool doubled = false;
};

// vehicle가 소유한 아이템리스트, drawable.
class DrawableItemInstanceList : public ItemInstanceList
{
public:
	void draw(const Obj& ownner, const ShaderPtr& shader) const;
	void draw_gui();
private:
	const ModelPtr model{ Model::quad() };
//	glm::vec2 diff{ 2.5, 0 };
//	float idxtransferpos{ 1.25 };
//	float originscale{ 0.25 };
	glm::vec2 diff{ 0, 1.5 };
	float idxtransferpos{ 1.25 };
	float originscale{ 0.25 };
};
