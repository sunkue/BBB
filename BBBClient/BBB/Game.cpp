#include "stdafx.h"
#include "Game.h"
#include "DynamicObj.h"

void Game::save_file_impl(ofstream& file)
{
	SAVE_FILE(file, version);
	SAVE_FILE(file, serveraddr_);
}

void Game::load_file_impl(ifstream& file)
{
	LOAD_FILE(file, version);
	LOAD_FILE(file, serveraddr_);
}


