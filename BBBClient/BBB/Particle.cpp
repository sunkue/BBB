#include "stdafx.h"
#include "Particle.h"
#include "GameScene.h"

////////////////////////////////////////////////////
// 
//			ParticleRenderer
// 
////////////////////////////////////////////////////

void ParticleRenderer::draw(const TexturePtr& particle_mask, glm::vec3 pos, glm::vec3 scale, glm::vec3 color) const
{
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	shader->use();
	auto cameraPos = GameScene::get().get_main_camera()->get_position();
	auto diff = glm::normalize(pos - cameraPos);
	glm::quat q = glm::rotation(glm::vec3{ 1,0,0 }, diff);
	glm::mat4 m = glm::translate(pos) * glm::toMat4(q) * glm::scale(scale);
	shader->set("u_m_mat", m);
	glActiveTexture(GL_TEXTURE0);
	shader->set("u_material.albedo1", particle_mask);
	glActiveTexture(GL_TEXTURE1);
	shader->set("u_depth", GameScene::get().get_gbuffer_renderer()->depth_tbo);
	shader->set("u_color", color);
	shader->set("u_transitionSize", transitionSize);
	Model::rect()->draw(shader);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

////////////////////////////////////////////////////
// 
//				Particle
// 
////////////////////////////////////////////////////

void Particle::draw(const ParticleRenderer& particleRenderer) const
{
	particleRenderer.draw(mask, pos, scale, color);
}
