#include "stdafx.h"
#include "Renderer.h"
#include "Mesh.h"
#include "texture.h"
#include "Obj.h"

Mesh::Mesh
(
	const string& name,
	const vector<Vertex>& vertices,
	const vector<GLuint>& indices,
	const vector<TexturePtr>& textures
)
	: vertices_{ move(vertices) }
	, indices_{ move(indices) }
	, textures_{ move(textures) }
	, name_{ move(name) }
{
	setup_mesh();
	//vertices_.clear(); // ..
}

void Mesh::draw(const ShaderPtr& shader, GLenum Mode)const
{
	shader->use();
	GLuint albedo_n = 1;
	GLuint specular_n = 1;
	for (const auto& t : textures_)
	{
		if ("albedo" == t->type)
		{
			shader->set("u_material." + t->type + to_string(albedo_n++), t);
		}
		else if ("specular" == t->type)
		{
			shader->set("u_material." + t->type + to_string(specular_n++), t);
		}
	}
	glBindVertexArray(vao);
	glDrawElements(Mode, indices_.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

void Mesh::setup_mesh()
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), vertices_.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(GLuint), indices_.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, pos));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, nor));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, tex));

	// ids
	glEnableVertexAttribArray(3);
	glVertexAttribIPointer(3, 4, GL_INT, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, boneIds));

	// weights
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, boneWeights));

	glBindVertexArray(0);
}


/// ////////////////////////////////////////////

CubeMap::CubeMap(const ShaderPtr& shader, const vector<string_view>& textures, const string_view dir)
	: cube_shader_{ shader }
	, cube_texture_{ load_cube_texture_file(textures, dir) }
	, vao_{ setup_mesh() }
{
}

void CubeMap::draw() const
{

}


GLuint CubeMap::setup_mesh()
{
	static GLuint cached_vao = GLuint(-1);
	if (cached_vao != GLuint(-1))
	{
		return cached_vao;
	}

	GLuint vbo;
	glGenVertexArrays(1, &cached_vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(cached_vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_verticle), cube_verticle, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);

	return cached_vao;
}



