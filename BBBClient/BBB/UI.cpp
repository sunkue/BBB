#include "stdafx.h"
#include "UI.h"
#include "GameScene.h"
#include "timer.h"
#include "Game.h"


void SetQuadvao(GLuint& vao, float w, float h)
{
	glGenVertexArrays(1, &vao);
	GLuint vbo;
	glGenBuffers(1, &vbo);
	struct QUAD
	{
		glm::vec2 pos;
		glm::vec2 tex;  // gl_Position.y / 2 + 0.5f
	};

	// 프레임 위치는 윈도우 좌표계로 그리자. 스타트 포인트 + w/h
	QUAD quadv[] =
	{
		{ { 0,-h}, {0,0} }
		,{{ w, 0}, {1,1} }
		,{{ 0, 0}, {0,1} }
		,{{ 0,-h}, {0,0} }
		,{{ w,-h}, {1,0} }
		,{{ w, 0}, {1,1} }
	};

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(QUAD), (const GLvoid*)offsetof(QUAD, pos));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(QUAD), (const GLvoid*)offsetof(QUAD, tex));

	glBindVertexArray(0);
}

/// //////////////////////////////////////

void GameUI::draw()
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	if (!Game::get().gameEnd)
	{
		ranking_.draw();
		backmirror_.draw();
		minimap_.draw();
		textUi_.draw();
	}
	else
	{
		textUi_.drawEndSceneUi();
	}

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

void GameUI::update()
{
	if (!Game::get().gameEnd)
	{
		ranking_.update();
		backmirror_.update();
		minimap_.update();
		textUi_.update();
	}
}

/// //////////////////////////////////////

GameUI::RankingUI::RankingUI()
{
	//// init players & ui pos
	{
		glm::vec2 sp = start_pos;

		auto playernum = GameScene::get().get_cars().size();
		dynamic_sps_.reserve(playernum);
		static_sps_.reserve(playernum);

		for (int i = 0; i < playernum; i++)
		{
			//textures_.emplace_back(Texture::create(CreatePngTexture("./Resource/Texture/UI/car1.png")));
			static_sps_.emplace_back(sp);
			dynamic_sps_.emplace_back(sp);

			sp -= diff;
		}
	}

	profile_gray_ = Texture::create();
	profile_gray_->id = CreatePngTexture("./Resource/Texture/UI/ranking.png");
	//	ready_frame_gray_ = Texture::create();
		//profile_gray_->id = CreatePngTexture("./Resource/Texture/matching/ready_frame.png");

	for (auto& c : car_textures_)
		c = Texture::color_white();
	car_textures_[0] = Texture::color_cobalt();
	car_textures_[1] = Texture::color_red();
	car_textures_[2] = Texture::color_green();
	car_textures_[3] = Texture::color_redpurple();

		//// init frame_vao
	{
		SetQuadvao(quad_vao_);
	}
}

void GameUI::RankingUI::draw()
{
	//// init players & ui pos
	/*
	{
		glm::vec2 sp = start_pos;

		auto playernum = GameScene::get().get_cars().size();
		dynamic_sps_.clear();
		static_sps_.clear();

		dynamic_sps_.reserve(playernum);
		static_sps_.reserve(playernum);

		for (int i = 0; i < playernum; i++)
		{
			static_sps_.emplace_back(sp);
			dynamic_sps_.emplace_back(sp);

			sp -= diff;
		}
	}
	*/

	shader_->use();
	glActiveTexture(GL_TEXTURE0);

	for (InGameID i = 0; i < Game::get().players.size(); i++)
	{
		auto& player = Game::get().players[i];

		if (player.IsFree())
			continue;

		if (Game::get().playerid == i)
			continue;

		// update uniforms.
		shader_->set("u_texture", profile_gray_);
		shader_->set("u_mixtexture", car_textures_[i]);
		shader_->set("u_startpos", dynamic_sps_[i]);
		shader_->set("u_size", size_);

		// draw
		glBindVertexArray(quad_vao_);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
	{
		auto playerId = Game::get().playerid;
		auto& player = Game::get().players[playerId];

	//	auto& car_texture = car->get_model()->get_textures()[2];
		// update uniforms.
		shader_->set("u_texture", profile_gray_);
		shader_->set("u_mixtexture", car_textures_[playerId]);
		shader_->set("u_startpos", dynamic_sps_[playerId]);
		shader_->set("u_size", size_ * 1.25);

		// draw
		glBindVertexArray(quad_vao_);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
}

void GameUI::RankingUI::update()
{
	auto elapsedt = TimerSystem::get().tick_timef();

	const auto& cars = GameScene::get().get_cars(); // 등수 뽑아내서 rank 에 저장하기.
	for (int i = 0; i < cars.size(); i++)
	{
		auto origin = dynamic_sps_[i];
		auto target = static_sps_[reinterpret_cast<VehicleObj*>(cars[i].get())->get_rank() - 1]; // rank[i] 번쨰 위치로
		auto diff = target - origin;
		dynamic_sps_[i] += diff * elapsedt;
	}
}

/// //////////////////////////////////////

GameUI::TextUI::TextUI()
//	: TextRenderer_{ "Resource/Font/나눔손글씨 달의궤도.ttf" }
	: TextRenderer_{ "Resource/Font/TTTogether.ttf" }
{
}

void GameUI::TextUI::draw()
{
	// bind main render target
	if (auto car = dynamic_cast<VehicleObj*>(GameScene::get().get_player().get()))
	{
		// lab
		auto str = to_string(std::max(0, car->get_lab())).append("/").append(to_string(MAX_LAB));
		auto x = TextRenderer_.RenderText(str, lab_startpos_.x, lab_startpos_.y, lab_size_, color_);
		TextRenderer_.RenderText(" Lab"s, x, lab_startpos_.y, lab_size_ / 2, color_);

		// time
		auto gametime = TimerSystem::get().game_time().count();
		auto cleartime = Game::get().players[Game::get().playerid].get_vehicle()->get_clear_time().count();
		if (MAX_LAB <= Game::get().players[Game::get().playerid].get_vehicle()->get_lab()
			&& cleartime  < gametime)
		{
			gametime = cleartime;
		}
		short min = gametime / (60 * 1000);
		short sec = (gametime / 1000) % 60;
		short below_sec = gametime % 1000; below_sec /= 10;
		str = "0" + to_string(min) + ".";
		str += sec < 10 ? "0" : "";
		str += to_string(sec) + "." + to_string(below_sec);
		TextRenderer_.RenderText(str, time_startpos_.x, time_startpos_.y, time_size_, color_);

		if (0 <= countdown_)
		{
			TextRenderer_.RenderText(" " + to_string(countdown_), countdown_startpos_.x, countdown_startpos_.y, countdown_size_
				, glm::vec3{ 0.975,.11125,0.3375 });
		}
		//rank
		//if (auto v = dynamic_cast<VehicleObj*>(GameScene::get().get_player().get()))
		//{
		//	str = " " + to_string(v->get_rank()) + "/" + "2";
		////	TextRenderer_.RenderText(str, rank_startpos_.x, rank_startpos_.y, rank_size_, color_);
		//}
	}
}

void GameUI::TextUI::drawEndSceneUi()
{
	for (auto& car : GameScene::get().get_cars())
	{
		if (!car->get_enable()) continue;

		auto rank = car->get_rank();
		if (10 <= rank)continue;
		auto sp = end_rank_sp_ + end_diff_ * rank;
		auto color = color_;
		auto size = end_rank_size_;
		string str = " " + to_string(rank);

		if (Game::get().playerid == car->get_objid())
		{
			size *= 1.4;
		}

		switch (rank)
		{
		case 1: str += "st"; break;
		case 2: str += "nd"; break;
		case 3: str += "rd"; break;
		default: str += "th"; break;
		}
		sp.x = TextRenderer_.RenderText(str, sp.x, sp.y, size, car->color_);

		str.clear();
		sp.x += 0.1f;

		if (car->get_lab() < MAX_LAB)
		{
			str += " Retired";
			//		color = { 0.75,0,0.15 };
			TextRenderer_.RenderText(str, sp.x, sp.y, size, car->color_);
			continue;
		}

		auto cleartime = car->get_clear_time().count();
		short min = cleartime / (60 * 1000);
		short sec = (cleartime / 1000) % 60;
		short below_sec = cleartime % 1000; below_sec /= 10;
		
		str += "0" + to_string(min) + ".";
		str += sec < 10 ? "0" : "";
		str += to_string(sec) + "." + to_string(below_sec);
		TextRenderer_.RenderText(str, sp.x, sp.y, size, car->color_);
	}
}

void GameUI::TextUI::drawMatchingSceneUi()
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	auto sp = readybutton_sp_;
	auto size = readybutton_size_;
	TextRenderer_.RenderText("READY", sp.x, sp.y, size, glm::vec3{ 0 });

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

}

void GameUI::TextUI::update()
{

}


/// //////////////////////////////////////

GameUI::BackMirrorUI::BackMirrorUI()
{
	// init main texture
	auto& mainScreen = Screen::mainScreen();
	glGenFramebuffers(1, &fbo_);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_);

	main_ = Texture::create();
	glGenTextures(1, &main_->id);
	glBindTexture(GL_TEXTURE_2D, main_->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, main_->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	{
		mask_ = Texture::create(load_texture_file("backmirrorMask"s + ".png"s, "./Resource/Texture/UI"s));
		frame_ = Texture::create(load_texture_file("backmirrorFrame"s + ".png"s, "./Resource/Texture/UI"s));

		SetQuadvao(quad_vao_);
	}
}

void GameUI::BackMirrorUI::draw()
{
	if (!GameScene::get().get_backmirror_camera()->get_enable())
		return;

	shaderFrame_->use();
	shaderFrame_->set("u_texture", frame_);
	shaderFrame_->set("u_startpos", startposFrame_);
	shaderFrame_->set("u_size", sizeFrame_);
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	shader_->use();
	shader_->set("u_texture", main_);
	shader_->set("u_mask", mask_);
	shader_->set("u_startpos", startpos_);
	shader_->set("u_size", size_);
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);

}

void GameUI::BackMirrorUI::update()
{
}

/// //////////////////////////////////////

GameUI::MinimapUI::MinimapUI()
{
	// init main texture
	auto& mainScreen = Screen::mainScreen();
	glGenFramebuffers(1, &fbo_);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_);

	main_ = Texture::create();
	glGenTextures(1, &main_->id);
	glBindTexture(GL_TEXTURE_2D, main_->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, main_->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//// init frame_vao
	{
		frame_ = Texture::create(load_texture_file("minimapMask2"s + ".png"s, "./Resource/Texture/UI"s));
		SetQuadvao(quad_vao_);
	}
}

void GameUI::MinimapUI::draw()
{
	if (!GameScene::get().get_minimap_camera()->get_enable())
		return;

	shader_->use();

	// update uniforms.

	shader_->set("u_texture", main_);
	shader_->set("u_mask", frame_);
	shader_->set("u_startpos", startpos_);
	shader_->set("u_size", size_);

	// draw
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

/// //////////////////////////////////////

void TitleUI::draw()
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	glDisable(GL_DEPTH_TEST);

	gamemodebutton_.draw();

	glEnable(GL_DEPTH_TEST);
}

TitleUI::GameModeButtonUI::GameModeButtonUI()
{
	//vec2 pos = a_position * u_size + u_startpos;

	playButton_ = Texture::create();
	playButton_->id = load_texture_file("PlayButton.jpg", "./Resource/Texture/title", true);
	editButton_ = Texture::create();
	editButton_->id = load_texture_file("EditButton.jpg", "./Resource/Texture/title", true);
	SetQuadvao(quad_vao_);
}
void TitleUI::GameModeButtonUI::draw()
{
	glm::vec2 startpos1 = getStartPos1();
	glm::vec2 startpos2 = getStartPos2();
	constexpr float selectscale = 0.1f;
	bool selected = false;
	//버튼 두개 그리기
	shader_->use();

	// update uniforms.
	shader_->set("u_texture", playButton_);
	shader_->set("u_startpos", startpos1);
	selected = TitleScene::SelectedButton::play == TitleScene::get().selected_button_;
	shader_->set("u_size", size_ + selectscale * selected);
	// draw
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// update uniforms.
	shader_->set("u_texture", editButton_);
	shader_->set("u_startpos", startpos2);
	selected = TitleScene::SelectedButton::edit == TitleScene::get().selected_button_;
	shader_->set("u_size", size_ + selectscale * selected);
	// draw
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

/// //////////////////////////////////////

void MatchingUI::draw()
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	glDisable(GL_DEPTH_TEST);

	readybutton_.draw();

	glEnable(GL_DEPTH_TEST);
}

MatchingUI::ProfileUI::ProfileUI()
{
	profile_gray_ = Texture::create();
	profile_gray_->id = CreatePngTexture("./Resource/Texture/matching/match_profile.png");
	ready_frame_gray_ = Texture::create();
	ready_frame_gray_->id = CreatePngTexture("./Resource/Texture/matching/ready_frame.png");
	//	CreatePngTexture
	SetQuadvao(quad_vao_);
}

void MatchingUI::ProfileUI::draw()
{
	shader_->use();

	auto halfmaxplayer = Game::get().players.size() / 2;
	auto halfhalfmaxplayer = halfmaxplayer / 2;
	for (InGameID i = 0; i < Game::get().players.size(); i++)
	{
		auto& player = Game::get().players[i];
		if (!player.IsFree())
		{
			auto car = player.get_vehicle();
			auto car_texture = car->get_model()->get_textures()[2];
			glm::vec2 startpos{ center_ };

			auto xpower{ i };
			if (i < halfmaxplayer)
			{
				startpos.y += diff_.y;
			}
			else
			{
				startpos.y -= diff_.y;
				xpower -= halfmaxplayer;
			}

			auto xpos = (i % halfmaxplayer); // 0,1,2,3
			xpos -= (halfmaxplayer - 1 - xpos); // -3.-1, 1, 3

			startpos.x += diff_.x * xpower;
			// update uniforms.
			shader_->set("u_texture", profile_gray_);
			shader_->set("u_mixtexture", car_texture);
			shader_->set("u_startpos", startpos);
			shader_->set("u_size", size_);
			// draw
			glBindVertexArray(quad_vao_);
			glDrawArrays(GL_TRIANGLES, 0, 6);

			if (player.IsReady())
			{
				shader_->set("u_texture", ready_frame_gray_);
				shader_->set("u_mixtexture", Model::box_yellow()->get_textures()[0]);
				shader_->set("u_startpos", startpos);
				shader_->set("u_size", size_);
				glBindVertexArray(quad_vao_);
				glDrawArrays(GL_TRIANGLES, 0, 6);
			}
		}
	}
}

void MatchingUI::ProfileUI::update()
{

}

/// //////////////////////////////////////

MatchingUI::ReadyButtonUI::ReadyButtonUI()
{
	readybutton_gray_ = Texture::create();
	readybutton_gray_->id = CreatePngTexture("./Resource/Texture/matching/ReadyButton.png");
	SetQuadvao(quad_vao_);
}

void MatchingUI::ReadyButtonUI::draw()
{
	shader_->use();

	auto id = Game::get().playerid;
	auto& player = Game::get().players[id];

	shader_->set("u_texture", readybutton_gray_);
	if (player.IsReady())
		shader_->set("u_mixtexture", Model::box_gray()->get_textures()[0]);
	else
		shader_->set("u_mixtexture", Model::box_yellow()->get_textures()[0]);
	shader_->set("u_startpos", sp_);
	shader_->set("u_size", size_);
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void MatchingUI::ReadyButtonUI::update()
{

}


/// //////////////////////////////////////

