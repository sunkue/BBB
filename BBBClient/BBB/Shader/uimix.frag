#version 450

uniform sampler2D u_texture; 
uniform sampler2D u_mixtexture; 
uniform vec2 u_mixsp; 

in vec2 texcoord; 

out vec4 o_flagcolor;

void main() 
{ 
    o_flagcolor = texture(u_texture, texcoord);
    o_flagcolor *= texture(u_mixtexture, texcoord);
    if(  o_flagcolor.a < 0.1)
        discard;
} 

