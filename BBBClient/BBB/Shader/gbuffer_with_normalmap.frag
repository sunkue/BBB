#version 450

struct Material
{
	sampler2D normalmap1;
	sampler2D albedo1;
};

layout(location = 0) out vec4 g_world_pos;
layout(location = 1) out vec4 g_normal;
layout(location = 2) out vec4 g_albedospec;
layout(location = 3) out float g_depth;

in VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} fs_in;

uniform Material u_material;

vec4 quatTangent(vec3 u)
{
	vec3 v = vec3( 0, 0, 1 );
	float norm_u_norm_v = sqrt(dot(u, u) * dot(v, v));
	float real_part = norm_u_norm_v + dot(u, v);
	vec3 w;
	
	if (real_part < 0.00001 * norm_u_norm_v)
	{
		real_part = 0.0f;
		w = abs(u.x) > abs(u.z) ? vec3(-u.y, u.x, 0.f) : vec3(0.f, -u.z, u.y);
	}
	else
	{
		w = cross(u, v);
	}

	return normalize(vec4(w.x, w.y, w.z, real_part));
}

mat4 quatToMatrix(vec4 q)
{
	mat4x4 m;
    float sqw = q.w*q.w;
    float sqx = q.x*q.x;
    float sqy = q.y*q.y;
    float sqz = q.z*q.z;

    // invs (inverse square length) is only required if quaternion is not already normalised
    float invs = 1 / (sqx + sqy + sqz + sqw);
    m[0][0] = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
    m[1][1] = (-sqx + sqy - sqz + sqw)*invs ;
    m[2][2] = (-sqx - sqy + sqz + sqw)*invs ;
    
    float tmp1 = q.x*q.y;
    float tmp2 = q.z*q.w;
    m[1][0] = 2.0 * (tmp1 + tmp2)*invs ;
    m[0][1] = 2.0 * (tmp1 - tmp2)*invs ;
    
    tmp1 = q.x*q.z;
    tmp2 = q.y*q.w;
    m[2][0] = 2.0 * (tmp1 - tmp2)*invs ;
    m[0][2] = 2.0 * (tmp1 + tmp2)*invs ;
    tmp1 = q.y*q.z;
    tmp2 = q.x*q.w;
    m[2][1] = 2.0 * (tmp1 + tmp2)*invs ;
    m[1][2] = 2.0 * (tmp1 - tmp2)*invs ;      
	return m;
}

void main()
{	
	vec3 world_pos = fs_in.world_pos;
	vec2 texcoord = fs_in.texcoord;
	vec3 normal = fs_in.normal;

	vec4 q = quatTangent(normal);
	mat4 tangentMat = quatToMatrix(q);

	normal = normalize(texture(u_material.normalmap1, texcoord).rgb * 2 - 1);
	g_normal = tangentMat * vec4(normal, 0);
	g_world_pos = vec4(world_pos, 0);
	g_albedospec.rgb = texture(u_material.albedo1, texcoord).rgb;
	g_albedospec.a = 0.2;

	g_depth = gl_FragCoord.z;
}

