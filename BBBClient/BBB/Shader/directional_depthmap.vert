#version 450

uniform mat4 u_lightpos_mat;
uniform mat4 u_m_mat;

layout(location = 0) in vec3 a_position;
layout(location = 3) in ivec4 a_boneIDs;
layout(location = 4) in vec4 a_weights;

const int MAX_BONES = 100;
uniform mat4 u_FinalBoneMatrices[MAX_BONES];
uniform bool u_animate = false;

void main()
{
    vec4 totalPosition = vec4(a_position, 1) * (1 - float(u_animate));
    
    for(int i = 0 ; i < 4 ; i++)
    {
        if(a_boneIDs[i] == -1) 
            continue;
        if(a_boneIDs[i] >= MAX_BONES) 
        {
            totalPosition = vec4(a_position, 1.0f);
            break;
        }
        vec4 localPosition = u_FinalBoneMatrices[a_boneIDs[i]] * vec4(a_position,1.0f);
        totalPosition += localPosition * a_weights[i];
    } 

	gl_Position = u_lightpos_mat * u_m_mat * vec4(totalPosition.xyz, 1.0);
}

