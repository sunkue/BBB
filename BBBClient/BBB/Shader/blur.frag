#version 450

uniform sampler2D u_screen_texture; 
uniform vec3 u_additionalcolor;

in vec2 texcoord;


out vec4 o_color;

float blur[9] = float[]
(
    1.0 / 16, 2.0 / 16, 1.0 / 16,
    2.0 / 16, 4.0 / 16, 2.0 / 16,
    1.0 / 16, 2.0 / 16, 1.0 / 16  
);

float blur2[9] = float[]
(
    0,      1.0/9, 0,
    1.0/9,  5.0/9, 1.0/9,
    0,      1.0/9, 0
);

vec4 kerneling(float kernel[9])
{
	const float offset = 1.0 / 300.0;  

    vec2 offsets[9] = vec2[](
        vec2(-offset,  offset), // ���� ���
        vec2( 0.0f,    offset), // �߾� ���
        vec2( offset,  offset), // ���� ���
        vec2(-offset,  0.0f),   // ���� �߾�
        vec2( 0.0f,    0.0f),   // ���߾�
        vec2( offset,  0.0f),   // ���� �߾�
        vec2(-offset, -offset), // ���� �ϴ�
        vec2( 0.0f,   -offset), // �߾� �ϴ�
        vec2( offset, -offset)  // ���� �ϴ�   
    );

    vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
	    sampleTex[i] = vec3( texture(u_screen_texture, texcoord.st + offsets[i]) * 1.4f);
    }

    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
        col += sampleTex[i] * kernel[i];
    
    return vec4(col, 1.0);
} 

void main()
{ 
    float gamma = 1.6f;
    o_color = vec4(pow(kerneling(blur2).rgb + u_additionalcolor, vec3(1.0 / gamma)), 1.0f);
}

