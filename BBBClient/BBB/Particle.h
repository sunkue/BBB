#pragma once

#include "Renderer.h"
#include "Shader.h"

////////////////////////////////////////////////////
// 
//			ParticleRenderer
// 
////////////////////////////////////////////////////

class ParticleRenderer
{
	friend class Renderer;
public:
	void draw(const TexturePtr& particle_mask, glm::vec3 pos, glm::vec3 scale = glm::vec3{ 1 }, glm::vec3 color = glm::vec3{ 1 }) const;

	float transitionSize = 0.000296;
	const ShaderPtr& shader = Shader::maskParticle();
};


////////////////////////////////////////////////////
// 
//				Particle
// 
////////////////////////////////////////////////////

struct Particle
{
	glm::vec3 pos;
	glm::vec3 scale;
	glm::vec3 color;
	TexturePtr mask;
	milliseconds lifetime;
	void draw(const ParticleRenderer& particleRenderer) const;
};