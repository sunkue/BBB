#pragma once
#include "Event.h"
#include "Item.h"
#include "Vehicle.h"

///////////////////////////////////////////////////////

class ItemEvent : public Event
{
public:
	ItemEvent() : action_time{ 2000ms } {};

private:
	virtual void Update(milliseconds elapsed) override
	{
		auto remain_time = action_time - elapsed;

		//TODO
	//	cout << remain_time << "  " << action_time << endl;

		//vehicle speed += 100, 0.5ms, ?

		// bool boost..


		action_time = remain_time;

		if (action_time <= 0ms)
			Event::done();
	};
	milliseconds action_time;
	//Obj& target;
};

///////////////////////////////////////////////////////

class DupleItemEvent : public ItemEvent
{
public:
	DupleItemEvent(VehicleObj* me) :user{ me } {};
private:
	virtual void Update(milliseconds elapsed) override
	{
		user->use_duple();
		Event::done();
	};
	VehicleObj* user;
};

class BoostItemEvent : public ItemEvent
{
public:
	BoostItemEvent(VehicleObj* me) :user{ me } {
		user->get_Audio()->PlaySound("volgue_dash_03.wav", true);
	};
private:
	virtual void Update(milliseconds elapsed) override
	{
		action_time -= elapsed;
		user->set_boost(true);
		if (action_time <= 0ms)
			Event::done();
	};
	virtual void Detroy() override
	{
		user->set_boost(false);
	};
	milliseconds action_time{3000ms};
	VehicleObj* user;
};

class SheildItemEvent : public ItemEvent
{
public:
	SheildItemEvent(VehicleObj* me) :user{ me } {};
private:
	virtual void Update(milliseconds elapsed) override
	{
		action_time -= elapsed;
		user->set_shield(true);
		if (action_time <= 0ms)
		Event::done();
	};
	virtual void Detroy() override
	{
		user->set_shield(false);
	};
	milliseconds action_time{ 3000ms };
	VehicleObj* user;
};
///////////////////////////////////////////////////////

struct ItemEventFactory
{
	static EventPtr createEvent(const ItemInstancePtr& instance,  VehicleObj* target,  VehicleObj* user);
};