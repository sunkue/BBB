#pragma once
#include "DynamicObj.h"

using VehiclePtr = shared_ptr<class VehicleObj>;
class VehicleObj : public DynamicObj
{
	friend class AutoVehicleObj;
private:
	virtual void save_file_impl(ofstream& file) override;
	virtual void load_file_impl(ifstream& file) override;

public:
	enum class CONTROLL { negative = -1, none = 0, positive = 1 };

public:
	explicit VehicleObj(size_t id, const ModelPtr& model)
		: DynamicObj{ model }, id_{ id }
	{
		load("car" + to_string(id));
		const_cast<Boundings&>(get_boundings()).load("carbounding");
	}

public:
	void update_state();
	void reset1();
	void reset2();
	virtual void update(milliseconds time_elapsed) override;

	virtual void update_speed(float time_elapsed) override;
	virtual void update_uniform_vars(const ShaderPtr& shader) const override;

	virtual void on_collide(Obj& other, OUT void* context = nullptr);
	virtual void draw_gui() override;
	virtual void draw(const ShaderPtr& shader) const;
private:

	virtual void update_camera(Camera* camera, float time_elpased) const override;

public:
	virtual bool process_input(const KeyBoardEventManager::key_event& key) override;
	//void process_input(InputKey key, bool press);
public:
	void regenerate();
	void regenerate(int nodeid);
public:
	GET(rank);
	SET(rank);
	GET(lab);
	SET(lab);
private:
	size_t id_;

	float angular_power_ = 1.5f;
	float acceleration_power_ = 16.f;
	float friction_power_ = 2.0f;
	float max_speed_ = 50.0f;

	float acceleration_ = 0.f;
	float friction_ = 0.f;

	CONTROLL accel_control_{ CONTROLL::none };
	CONTROLL angular_control_{ CONTROLL::none };

	bool front_on_ = false;
	bool back_on_ = false;
	bool right_on_ = false;
	bool left_on_ = false;

	bool brake_on_ = false;
	bool draft_on_ = false;
	bool item_use_on_ = false;
	float draft_time_ = 1;

	glm::vec3 rimcolor{};
	float rimpower{ 5 };
	float rimboundary{ 1 };

	// for draw effects.
public:
	SET(boost);
	void use_duple();
	SET(shield);

	GET(boost);
	GET(duple);
	GET(shield);
private:
	bool boost_ = false;
	bool duple_ = false;
	bool shield_ = false;

public:
	GET(outed);
	SET(outed);
private:
	bool outed_{ false };
public:
	glm::vec3 color_{};
	SET(included_node);
private:
	int included_node_ = 0;
	int rank_ = 1;
	int lab_ = -1;
public:
	// this < other
	bool rank_worse_than(VehicleObj& other);

public:
	enum class CHECK_POINT
	{
		none = 0,
		begin = 1,
		check1,
		check2,
	};

	GET(check_point);
	SET(check_point);

	void clear_lab();
	GET(clear_time);
	SET(clear_time);
private:
	CHECK_POINT check_point_ = CHECK_POINT::check2;
	milliseconds clear_time_ = {};
public:
	bool TryAcquireItem(ItemObj& itemObj);
	void OnUseItem();

	//for networking
	bool NetTryAcquireItem(ItemType itemtype, ObjID itemid);
	ItemType NetUseItem();

	GET_REF_UNSAFE(item_list);
private:
	DrawableItemInstanceList item_list_;

private:
	// for backmirror camera
	class BackMirror : public Obj
	{
		CREATE_UNIQUE(BackMirror);
	private:
		BackMirror(VehicleObj* ownner) :Obj{}, vehicle_{ ownner }{};
	public:
		virtual void update(milliseconds time_elapsed) override {}
		virtual void update_camera(Camera* camera, float time_elpased) const override;
	private:
		VehicleObj* vehicle_;
	};

public:
	GET_REF(backmirror);
private:
	unique_ptr<BackMirror> backmirror_ = BackMirror::create(this);
};



using AutoVehiclePtr = shared_ptr<class AutoVehicleObj>;
class AutoVehicleObj : public VehicleObj
{
	friend class GameScene;
public:
	virtual void save_file_impl(ofstream& file) override;
	virtual void load_file_impl(ifstream& file) override;
public:
	AutoVehicleObj(size_t id, const ModelPtr& model) :
		VehicleObj{ id, model } 
	{
		PredictForwardNodeCount_ += rand() % 3;
		StreightMoveBelowValue_ += 0 < rand() % 3 ? 0 : 0.05f;
		MoveHeadDirBlendAlpha_ += (rand() % 3) * 0.1f;
		max_speed_ = 80.0f;
	};
public:
	virtual void update(milliseconds time_elapsed) override;
	virtual void draw_gui() override;
	void DoAi(milliseconds time_elapsed);
private:
	bool AiOn_ = false;
	int PredictForwardNodeCount_ = 4;
	float JugmentValue = 0;
	float StreightMoveBelowValue_ = 0.1f;
	float MoveHeadDirBlendAlpha_ = 0.3f;
};