#include "stdafx.h"
#include <vector>
#include "AudioSystem.h"
#include "Sound.h"

AudioSystem::AudioSystem()
{
	Engine = irrklang::createIrrKlangDevice();
	if (!Engine)cerr << "[AUDIO_SYS_ERR]" << endl;
}

AudioSystem::~AudioSystem()
{
	for (auto& s : Sounds)
		delete s;
	Engine->drop();
}

Sound* AudioSystem::LoadSound(string_view fileName, bool Is3D)
{
	if (SoundSourceTable.contains(string{ fileName }))
	{
		return CreateSound(SoundSourceTable[string{ fileName }], Is3D);
	}
	else
	{
		Sound* s{};
		if (Is3D)
			s = new Sound{ Engine->play3D(fileName.data(),{},false,true,true) };
		else
			s = new Sound{ Engine->play2D(fileName.data(),false,true,true) };

		Sounds.push_back(s);
		return s;
	}
}

Sound* AudioSystem::CreateSound(ISoundSource* source, bool Is3D)
{
	Sound* s{};
	if (Is3D)
		s = new Sound{ Engine->play3D(source,{},false,true,true) };
	else
		s = new Sound{ Engine->play2D(source,false,true,true) };
	Sounds.push_back(s);
	return s;
}

void AudioSystem::PlaySound(Sound* sound)
{
	sound->Sound_->setPlayPosition(0);
	sound->Sound_->setIsPaused(false);
}

void AudioSystem::SetListener(const glm::vec3& pos, const glm::vec3& lookDir)
{
	Engine->setListenerPosition({ pos.x, pos.y, pos.z }, { lookDir.x, lookDir.y, lookDir.z });
}

void AudioSystem::SetDefault3DSoundMinDistance(float dist)
{
	Engine->setDefault3DSoundMinDistance(dist);
}
