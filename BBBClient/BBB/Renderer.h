#pragma once

#include "Camera.h"
#include "Scene.h"
#include "Animation.h"

//////////////////////////////////////////////////////

struct Screen
{
	static Screen& mainScreen() { static Screen instance; return instance; }
	//	static Screen& minimapScreen() { static Screen instance; return instance; }
	//	static Screen& backmirrorScreen() { static Screen instance; return instance; }
	static constexpr int g_WindowSizeX = 1240;
	static constexpr int g_WindowSizeY = 720;
	const GLsizei fixedwidth{ g_WindowSizeX }; // private // only get
	const GLsizei fixedheight{ g_WindowSizeY };
	GLfloat n{ 0.1f };
	GLfloat f{ 20000.f };
	glm::vec4 viewport{};
	GLfloat aspect()const { return (GLfloat)fixedwidth / fixedheight; }
	glm::mat4 proj_mat(const CameraPtr& camera)const { return glm::perspective(glm::radians(camera->get_fovy()), aspect(), n, f); }
};

//////////////////////////////////////////////////////

class Renderer
{
	SINGLE_TON(Renderer);
public:
	void draw();
	void reshape(int w, int h);
	void changeScene(Scene*);
	void changeSceneWithLoading(Scene*);
	GET(scene);
private:
	mutex sceneLock_;
	Scene* scene_;
};

