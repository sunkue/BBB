#pragma once

#include "texture.h"

struct Character {
	TexturePtr Texture;  // ID handle of the glyph texture
	glm::ivec2   Size;       // Size of glyph
	glm::ivec2   Bearing;    // Offset from baseline to left/top of glyph
	unsigned int Advance;    // Offset to advance to next glyph
};


class TextRenderer
{
public:
	TextRenderer(const char* font);
	float RenderText(std::string_view text, float x, float y, float scale, glm::vec3 color);
private:
	std::map<char, Character> Characters_;
	unsigned int Vao_, Vbo_;
	FT_Library FL_;
	FT_Face Face_;
};

