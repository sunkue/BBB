#include "stdafx.h"
#include "TextManager.h"
#include "Shader.h"

#define RGBA(r,g,b,a) ((UINT)( (((UINT)(r))<<24) | (((UINT)(g))<<16) | (((UINT)(b))<<8) | ((UINT)(a)) ))

TextRenderer::TextRenderer(const char* font)
{
	glGenVertexArrays(1, &Vao_);
	glGenBuffers(1, &Vbo_);
	glBindVertexArray(Vao_);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	if (FT_Init_FreeType(&FL_))
	{
		std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
	}

	if (FT_New_Face(FL_, font, 0, &Face_))
	{
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
	}

	FT_Set_Pixel_Sizes(Face_, 0, 60);

	///

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

	for (unsigned char c = 31; c < 127; c++)
	{
		// load character glyph 
		if (FT_Load_Char(Face_, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// generate texture
		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			Face_->glyph->bitmap.width,
			Face_->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			Face_->glyph->bitmap.buffer
		);
		// set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// now store character for later use
		Character character = {
			Texture::create(),
			glm::ivec2(Face_->glyph->bitmap.width, Face_->glyph->bitmap.rows),
			glm::ivec2(Face_->glyph->bitmap_left, Face_->glyph->bitmap_top),
			Face_->glyph->advance.x
		};
		character.Texture->id = texture;
		Characters_.insert(std::pair<char, Character>(c, character));
	}

	///

	FT_Done_Face(Face_);
	FT_Done_FreeType(FL_);
}

float TextRenderer::RenderText(std::string_view text, float _x, float _y, float _scale, glm::vec3 color)
{
	
	{
		float x = _x;
		float y = _y;
		float scale = _scale;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindVertexArray(Vao_);
		const auto& shader = Shader::text();
		// activate corresponding render state	
		shader->use();
		shader->set("u_color", glm::vec3{ 1 });
		glActiveTexture(GL_TEXTURE0);
		// iterate through all characters
		std::string_view::const_iterator c;
		for (c = text.cbegin(); c != text.cend(); c++)
		{
			const Character& ch = Characters_[*c];

			float xpos = x + ch.Bearing.x * scale;
			float ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

			float w = ch.Size.x * scale * 0.8;
			float h = ch.Size.y * scale * 0.8;

			xpos += ch.Size.x * scale * 0.1;
			ypos += ch.Size.y * scale * 0.1;

			// update VBO for each character
			float vertices[6][4] = {
				{ xpos,     ypos + h,   0.0f, 0.0f },
				{ xpos,     ypos,       0.0f, 1.0f },
				{ xpos + w, ypos,       1.0f, 1.0f },

				{ xpos,     ypos + h,   0.0f, 0.0f },
				{ xpos + w, ypos,       1.0f, 1.0f },
				{ xpos + w, ypos + h,   1.0f, 0.0f }
			};
			// render glyph texture over quad
			shader->set("u_texture", ch.Texture);
			// update content of VBO memory
			glBindBuffer(GL_ARRAY_BUFFER, Vbo_);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			// render quad
			glDrawArrays(GL_TRIANGLES, 0, 6);
			// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
			x += (ch.Advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_BLEND);
	}

	{
		float x = _x;
		float y = _y;
		float scale = _scale;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindVertexArray(Vao_);
		const auto& shader = Shader::text();
		// activate corresponding render state	
		shader->use();
		shader->set("u_color", glm::vec3{ 1 });
		glActiveTexture(GL_TEXTURE0);
		// iterate through all characters
		std::string_view::const_iterator c;
		for (c = text.cbegin(); c != text.cend(); c++)
		{
			const Character& ch = Characters_[*c];

			float xpos = x + ch.Bearing.x * scale;
			float ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

			float w = ch.Size.x * scale * 1.125;
			float h = ch.Size.y * scale * 1.125;

			xpos -= ch.Size.x * scale * 0.0675;
			ypos -= ch.Size.y * scale * 0.0675;

			// update VBO for each character
			float vertices[6][4] = {
				{ xpos,     ypos + h,   0.0f, 0.0f },
				{ xpos,     ypos,       0.0f, 1.0f },
				{ xpos + w, ypos,       1.0f, 1.0f },

				{ xpos,     ypos + h,   0.0f, 0.0f },
				{ xpos + w, ypos,       1.0f, 1.0f },
				{ xpos + w, ypos + h,   1.0f, 0.0f }
			};
			// render glyph texture over quad
			shader->set("u_texture", ch.Texture);
			// update content of VBO memory
			glBindBuffer(GL_ARRAY_BUFFER, Vbo_);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			// render quad
			glDrawArrays(GL_TRIANGLES, 0, 6);
			// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
			x += (ch.Advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_BLEND);
	}
	

	//

	float x = _x;
	float y = _y;
	float scale = _scale;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindVertexArray(Vao_);
	const auto& shader = Shader::text();
	// activate corresponding render state	
	shader->use();
	shader->set("u_color", color);
	glActiveTexture(GL_TEXTURE0);
	// iterate through all characters
	std::string_view::const_iterator c;
	for (c = text.cbegin(); c != text.cend(); c++)
	{
		const Character& ch = Characters_[*c];

		float xpos = x + ch.Bearing.x * scale;
		float ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

		float w = ch.Size.x * scale;
		float h = ch.Size.y * scale;
		// update VBO for each character
		float vertices[6][4] = {
			{ xpos,     ypos + h,   0.0f, 0.0f },
			{ xpos,     ypos,       0.0f, 1.0f },
			{ xpos + w, ypos,       1.0f, 1.0f },

			{ xpos,     ypos + h,   0.0f, 0.0f },
			{ xpos + w, ypos,       1.0f, 1.0f },
			{ xpos + w, ypos + h,   1.0f, 0.0f }
		};
		// render glyph texture over quad
		shader->set("u_texture", ch.Texture);
		// update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, Vbo_);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_BLEND);

	return x;
}


