#include "stdafx.h"
#include "Deco.h"
#include "GameScene.h"
#include "Game.h"

void Deco::save_file_impl(ofstream& file)
{
	Obj::save_file_impl(file);

	int subObjCount = SubObjs.size();
	SAVE_FILE(file, subObjCount);
}

void Deco::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);

	int subObjCount = 0;
	LOAD_FILE(file, subObjCount);

	for (int i = 0; i < subObjCount; i++)
	{
		auto filename = "decos/subObj/deco0subobj" + to_string(i);
		SubObjs.push_back(make_shared<Obj>(Model::box_red()));
		SubObjs.back()->boundings_.load("boxbounding");
		SubObjs.back()->load(filename);
	}
}

void Deco::draw(const ShaderPtr& shader) const
{
	Obj::draw(shader);

	if (Game::get().isEditMode())
	{
		for (auto& o : SubObjs)
		{
			o->update_uniform_vars(shader);
			o->draw(shader);
		}
	}
}

void Deco::draw_gui()
{
	gui::Begin("Deco0 (map) ");

	if (gui::Button("NewBoundingObj"))
	{
		auto pos = GameScene::get().get_player()->get_position();

		SubObjs.push_back(make_shared<Obj>(Model::box_red()));
		SubObjs.back()->boundings_.load("boxbounding");
		SubObjs.back()->move(pos);
	}

	if (gui::Button("SaveAll"))
	{
		save();
		for (int i = 0; i < SubObjs.size(); i++)
		{
			auto filename = "decos/subObj/deco0subobj" + to_string(i) + ".txt";
			SubObjs[i]->save(filename);
		}
		cout << "saved" << endl;
	}

	if (gui::Button("LoadAll"))
	{
		for (int i = 0; i < SubObjs.size(); i++)
		{
			SubObjs[i]->load();
		}
		cout << "loaded" << endl;
	}

	gui::End();
}
