#include "stdafx.h"
/*
	assimp 라이브러리를 사용하고 있음,, 5.0 버전임.
*/


#include "Model.h"
using namespace glm;
/*
void SetBoneTransforms(const ShaderPtr& shader, float TimeInSeconds)const
{
	if (!scene->mAnimations)
	{
		shader->set("u_skinning", false);
		return;
	}
	if (!scene->mAnimations[0])
	{
		shader->set("u_skinning", false);
		return;
	}
	cout << "!" << endl;
	glm::mat4 Identity(1);
	float TicksPerSecond = (float)(scene->mAnimations[0]->mTicksPerSecond != 0 ? scene->mAnimations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = TimeInSeconds * TicksPerSecond;
	float AnimationTimeTicks = fmod(TimeInTicks, (float)scene->mAnimations[0]->mDuration);

	ReadNodeHierarchy(AnimationTimeTicks, scene->mRootNode, Identity);
	vector<glm::mat4> Transforms(BoneInfos.size());

	for (uint i = 0; i < BoneInfos.size(); i++) {
		Transforms[i] = BoneInfos[i].finalTransform;
	}

	shader->set("u_gBones", Transforms);
	shader->set("u_skinning", true);
}
*/

void Model::draw(const ShaderPtr& shader) const
{
	for (const auto& m : meshes)
	{
		m.draw(shader);
	}
}

void Model::draw_without(string_view name, const ShaderPtr& shader) const
{
	for (const auto& m : meshes)
	{
		if (m.name_ == name) continue;
		m.draw(shader);
	}
}

void Model::draw_without(const unordered_set<string_view>& names, const ShaderPtr& shader) const
{
	for (const auto& m : meshes)
	{
		if (names.contains(m.name_)) continue;
		m.draw(shader);
	}
}

void Model::draw_only(string_view name, const ShaderPtr& shader) const
{
	for (const auto& m : meshes)
	{
		if (m.name_ != name) continue;
		m.draw(shader);
	}
}

void Model::draw_with_f(string_view name, const ShaderPtr& shader, function<void()> pre_f, function<void()> post_f) const
{
	for (const auto& m : meshes)
	{
		if (m.name_ == name)
		{
			pre_f();
			m.draw(shader);
			post_f();
		}
		else
		{
			m.draw(shader);
		}
	}
}

void Model::load_model(string_view path)
{
	Assimp::Importer importer;
	auto scene = importer.ReadFile(path.data()
		, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE
		|| !scene->mRootNode)
	{
		cerr << "ERROR::[Model::load_model]::ASSIMP::"
			<< importer.GetErrorString() << endl;
		return;
	}
	directory = path.substr(0, path.find_last_of('/'));

	process_node(scene->mRootNode, scene);
}

void Model::process_node(aiNode* node, const aiScene* scene)
{
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.emplace_back(process_mesh(mesh, scene));
	}
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		process_node(node->mChildren[i], scene);
	}
}

Mesh Model::process_mesh(aiMesh* mesh, const aiScene* scene)
{
	vector<Vertex> vertices_; vertices_.reserve(mesh->mNumVertices);
	vector<GLuint> indices_; indices_.reserve(mesh->mNumVertices);
	vector<TexturePtr> textures_; textures_.reserve(10);

	for (int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		SetVertexBoneDataToDefault(vertex);
		vertex.pos = { mesh->mVertices[i].x , mesh->mVertices[i].y, mesh->mVertices[i].z };
		vertex.nor = { mesh->mNormals[i].x , mesh->mNormals[i].y, mesh->mNormals[i].z };
		if (mesh->mTextureCoords[0])
		{
			vertex.tex = { mesh->mTextureCoords[0][i].x , mesh->mTextureCoords[0][i].y };
		}
		else
		{
			vertex.tex = { 0, 0 };
		}

		vertices_.emplace_back(move(vertex));
	}

	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++)
		{
			indices_.push_back(face.mIndices[j]);
		}
	}

	if (0 <= mesh->mMaterialIndex)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		vector<TexturePtr> albedo_maps = load_material_textures(material
			, aiTextureType_DIFFUSE, "albedo");
		textures_.insert(textures_.end(), albedo_maps.begin(), albedo_maps.end());
		vector<TexturePtr> specular_maps = load_material_textures(material
			, aiTextureType_SPECULAR, "specular");
		textures_.insert(textures_.end(), specular_maps.begin(), specular_maps.end());
		vector<TexturePtr> normal_maps = load_material_textures(material
			, aiTextureType_NORMALS, "normalmap");
		textures_.insert(normal_maps.end(), normal_maps.begin(), normal_maps.end());
	}

	ExtractBoneWeightForVertices(vertices_, mesh, scene);
	return Mesh(mesh->mName.C_Str(), (vertices_), (indices_), (textures_));
}


vector<TexturePtr> Model::load_material_textures(
	aiMaterial* mat, aiTextureType type, string typeName)
{
	vector<TexturePtr> textures_;
	for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for (const auto& t : textures_loaded)
		{
			if ((t->path == str.C_Str()) && (t->type == typeName))
			{
				textures_.push_back(t);
				skip = true;
				break;
			}
		}

		if (false == skip)
		{
			auto texture = Texture::create();
			//texture.id = CreatePngTexture(Dir.append(str.C_Str()).c_str());
			texture->id = load_texture_file(str.C_Str(), directory);
			texture->type = typeName;
			texture->path = str.C_Str();
			textures_.push_back(texture);
			textures_loaded.push_back(texture);
		}
	}
	return textures_;
}

void Model::SetVertexBoneDataToDefault(Vertex& vertex)
{
	for (int i = 0; i < MAX_BONE_WEIGHTS; i++)
	{
		vertex.boneIds[i] = -1;
		vertex.boneWeights[i] = 0.0f;
	}
}

void Model::SetVertexBoneData(Vertex& vertex, int boneID, float weight)
{
	for (int i = 0; i < MAX_BONE_WEIGHTS; ++i)
	{
		if (vertex.boneIds[i] < 0)
		{
			vertex.boneWeights[i] = weight;
			vertex.boneIds[i] = boneID;
			break;
		}
	}
}

void Model::ExtractBoneWeightForVertices(std::vector<Vertex>& vertices, aiMesh* mesh, const aiScene* scene)
{
	for (int boneIndex = 0; boneIndex < mesh->mNumBones; ++boneIndex)
	{
		int boneID = -1;
		std::string boneName = mesh->mBones[boneIndex]->mName.C_Str();
		if (m_BoneInfoMap.find(boneName) == m_BoneInfoMap.end())
		{
			BoneInfo newBoneInfo;
			newBoneInfo.id = m_BoneCounter;
			newBoneInfo.offset = AssimpGLMHelpers::ConvertMatrixToGLMFormat(mesh->mBones[boneIndex]->mOffsetMatrix);
			m_BoneInfoMap[boneName] = newBoneInfo;
			boneID = m_BoneCounter;
			m_BoneCounter++;
		}
		else
		{
			boneID = m_BoneInfoMap[boneName].id;
		}
		assert(boneID != -1);
		auto weights = mesh->mBones[boneIndex]->mWeights;
		int numWeights = mesh->mBones[boneIndex]->mNumWeights;

		for (int weightIndex = 0; weightIndex < numWeights; ++weightIndex)
		{
			int vertexId = weights[weightIndex].mVertexId;
			float weight = weights[weightIndex].mWeight;
			assert(vertexId <= vertices.size());
			SetVertexBoneData(vertices[vertexId], boneID, weight);
		}
	}
}