#include "stdafx.h"
#include "Ghost.h"
#include "Game.h"
#include "GameScene.h"
#include "TrackNode.h"


void GhostObj::update(milliseconds time_elapsed)
{
	auto tf = time_elapsed.count() / 1000.f;
	auto& camera = GameScene::get().get_main_camera();
	auto right = camera->get_right();
	auto up = camera->get_up();
	auto front = camera->get_look_dir();

	float speed = speed_ * tf;
	if (up_on_) { move(up * speed); };
	if (down_on_) { move(-up * speed); };
	if (front_on_) { move(front * speed); };
	if (back_on_) { move(-front * speed); };
	if (right_on_) { move(right * speed); };
	if (left_on_) { move(-right * speed); };

	if (selected_obj_)
	{
		auto set_arrow_to_target = [this](auto& x, const glm::vec3 v)
		{
			auto& t = selected_obj_;
			auto& scaler = t->get_boundings().L1;
			auto pos = t->get_position() - x->get_position();
			auto scale = glm::vec3(glm::compMax(glm::vec3{ scaler.radius })) * 0.625;
			x->move(pos);
			x->scaling(glm::vec3{ 1 } / x->get_scale());
			auto value = scale * v;
			x->scaling(value + glm::vec3{ 0.2 });
			x->move(value);

		};
		set_arrow_to_target(xAxis, X_DEFAULT);
		set_arrow_to_target(yAxis, Y_DEFAULT);
		set_arrow_to_target(zAxis, Z_DEFAULT);
	}
}

void GhostObj::update_camera(Camera* camera, float time_elpased) const
{
	auto ryaw = glm::radians(rotator_.yaw);
	auto rpitch = glm::radians(rotator_.pitch);

	glm::vec3 front;
	front.x = cos(ryaw) * cos(rpitch);
	front.y = sin(rpitch);
	front.z = sin(ryaw) * cos(rpitch);
	front = glm::normalize(front);

	auto position = get_position();
	camera->set_position(position);
	camera->set_target(position + front);
}
void GhostObj::draw_gui()
{
	// Obj::draw_gui();

	gui::Begin("Ghost");
	gui::Text("This is Ghost for free moving camera");
	gui::Checkbox("draw selected bounding", &draw_boundings_);
	if (gui::Button(+cntl_mode._to_string()))
	{
		if (cntl_mode._size() == cntl_mode._value + 1)
		{
			cntl_mode = CONTROLL_MODE::TRANSLATE;
		}
		else
		{
			cntl_mode._value++;
		}
	}
	gui::SliderFloat("speed", &speed_, 10.0f, 500.0f);
	gui::End();

	if (selected_obj_)
	{
		selected_obj_->draw_gui();
	}
}

void GhostObj::update_uniform_vars(const ShaderPtr& shader) const
{
	// none
}

void GhostObj::draw(const ShaderPtr& shader) const
{
	// draw selected obj arrows.
	switch (select_mode)
	{
	case GhostObj::SELECTED_MODE::NONE:
	{
		xAxis->update_uniform_vars(shader);
		xAxis->draw(shader);
		yAxis->update_uniform_vars(shader);
		yAxis->draw(shader);
		zAxis->update_uniform_vars(shader);
		zAxis->draw(shader);
	}
	CASE GhostObj::SELECTED_MODE::X :
	{
		xAxis->update_uniform_vars(shader);
		xAxis->draw(shader, Model::box_yellow());
		yAxis->update_uniform_vars(shader);
		yAxis->draw(shader);
		zAxis->update_uniform_vars(shader);
		zAxis->draw(shader);
	}
	CASE GhostObj::SELECTED_MODE::Y :
	{
		xAxis->update_uniform_vars(shader);
		xAxis->draw(shader);
		yAxis->update_uniform_vars(shader);
		yAxis->draw(shader, Model::box_yellow());
		zAxis->update_uniform_vars(shader);
		zAxis->draw(shader);
	}
	CASE GhostObj::SELECTED_MODE::Z :
	{
		xAxis->update_uniform_vars(shader);
		xAxis->draw(shader);
		yAxis->update_uniform_vars(shader);
		yAxis->draw(shader);
		zAxis->update_uniform_vars(shader);
		zAxis->draw(shader, Model::box_yellow());
	}
	CASE GhostObj::SELECTED_MODE::XYZ :
	{
		xAxis->update_uniform_vars(shader);
		xAxis->draw(shader, Model::box_yellow());
		yAxis->update_uniform_vars(shader);
		yAxis->draw(shader, Model::box_yellow());
		zAxis->update_uniform_vars(shader);
		zAxis->draw(shader, Model::box_yellow());
	}
	}
}

bool GhostObj::process_input(const MouseEventManager::scroll_event& scroll)
{
	// 오브젝트 선택 후 스크롤로 사이즈 조정, (클릭하고 있을 때,)
	auto& mm = MouseEventManager::get();

	if (mm.get_L_click())
	{

		return true;
	}

	if (mm.get_R_click())
	{

		return true;
	}

	if (mm.get_Wheel_click())
	{

		return true;
	}

	{
		auto& camera = GameScene::get().get_main_camera();
		auto front = camera->get_look_dir();

		auto speed = std::clamp(scroll.yoffset * speed_, -100., 100.);
		move(front * speed);
		return true;
	}
	return true;
}

bool GhostObj::process_input(const MouseEventManager::button_event& button)
{
	auto& mm = MouseEventManager::get();
	if (GLFW_MOUSE_BUTTON_MIDDLE == button.button)
	{
		if (GLFW_PRESS == button.action)
		{
			rotate(glm::inverse(get_rotation()));
		}
	}

	if (GLFW_MOUSE_BUTTON_LEFT == button.button)
	{
		select_mode = SELECTED_MODE::NONE;

		auto set_selected_obj = [&](ObjPtr obj)
		{
			if (selected_obj_ && draw_boundings_)
			{
				const_cast<Boundings&>(selected_obj_->get_boundings()).set_drawable(false);
			}
			selected_obj_ = obj;
			if (draw_boundings_)
			{
				const_cast<Boundings&>(obj->get_boundings()).set_drawable(true);
			}
		};

		if (GLFW_PRESS == button.action)
		{
			auto& gamescene = GameScene::get();
			auto mouse_ray = Ray::create(gamescene.get_main_camera(), mm.get_prev_x(), mm.get_prev_y());
			float dist;

			GameScene::get().get_backmirror_camera()->set_enable(false);

			if (xAxis->get_boundings().intersects(mouse_ray, dist))
			{
				select_mode = SELECTED_MODE::X;
				return true;
			}
			if (yAxis->get_boundings().intersects(mouse_ray, dist))
			{
				select_mode = SELECTED_MODE::Y;
				return true;
			}
			if (zAxis->get_boundings().intersects(mouse_ray, dist))
			{
				select_mode = SELECTED_MODE::Z;
				return true;
			}

			// 데코 선택
			for (const auto& item : gamescene.get_DecoObjects())
			{
				if (item->get_boundings().intersects(mouse_ray, dist))
				{
					set_selected_obj(item);
					return true;
				}

				for (auto& a : item->SubObjs)
				{
					if (a->get_boundings().intersects(mouse_ray, dist))
					{
						set_selected_obj(a);
						return true;
					}
				}
			}

			// 차량 선택
			for (const auto& car : gamescene.get_cars())
			{
				if (car->get_boundings().intersects(mouse_ray, dist))
				{
					set_selected_obj(car);
					GameScene::get().get_backmirror_camera()->set_enable(true);
					return true;
				}
			}

			// 아이템 선택
			for (const auto& item : gamescene.get_track()->get_items())
			{
				if (item->get_boundings().intersects(mouse_ray, dist))
				{
					set_selected_obj(item);
					return true;
				}
			}

			// 타이어선택
			for (const auto& item : gamescene.get_Tires())
			{
				if (item->get_boundings().intersects(mouse_ray, dist))
				{
					set_selected_obj(item);
					return true;
				}
			}

			// 트랙 선택
			for (const auto& track : gamescene.get_track()->get_tracks())
			{
				if (track->get_boundings().intersects(mouse_ray, dist))
				{
					set_selected_obj(track);
					return true;
				}
			}
		}
	}

	if (mm.get_L_click() && mm.get_R_click())
	{

		auto& gamescene = GameScene::get();
		auto mouse_ray = Ray::create(gamescene.get_main_camera(), mm.get_prev_x(), mm.get_prev_y());

		// 차량에 타기
		for (const auto& car : gamescene.get_cars())
		{
			float dist;
			if (car->get_boundings().intersects(mouse_ray, dist))
			{
				gamescene.set_ghost(car);
				gamescene.swap_player_ghost();
				break;
			}
		}
	}

	return false;
}

bool GhostObj::process_input(const MouseEventManager::pos_event& pos)
{
	auto& mm = MouseEventManager::get();

	if (mm.get_L_click())
	{
		// Axis 선택
		if (selected_obj_)
		{
			float xoffset = pos.xpos - mm.get_prev_x(); xoffset /= 1;
			float yoffset = pos.ypos - mm.get_prev_y(); yoffset /= 1;
			auto& gamescene = GameScene::get();
			auto mouse_ray_prev = Ray::create(gamescene.get_main_camera(), mm.get_prev_x(), mm.get_prev_y());
			auto mouse_ray = Ray::create(gamescene.get_main_camera(), pos.xpos, pos.ypos);
			auto diff = mouse_ray.dir - mouse_ray_prev.dir;



			switch (cntl_mode)
			{
			case CONTROLL_MODE::TRANSLATE:
			{
				constexpr float power = 20; diff *= power;
				switch (select_mode)
				{
				case GhostObj::SELECTED_MODE::NONE:
				{}
				CASE GhostObj::SELECTED_MODE::X :
				{
					selected_obj_->move(diff.x* X_DEFAULT);
				}
				CASE GhostObj::SELECTED_MODE::Y :
				{
					selected_obj_->move(diff.y* Y_DEFAULT);
				}
				CASE GhostObj::SELECTED_MODE::Z :
				{
					selected_obj_->move(diff.z* Z_DEFAULT);
				}
				CASE GhostObj::SELECTED_MODE::XYZ :
				{
					selected_obj_->move(diff);
				}
				}
			}
			CASE CONTROLL_MODE::ROTATE :
			{
				constexpr float power = 5; diff *= power;
				float r_click = mm.get_R_click() - 0.5;
				r_click /= abs(r_click);

				switch (select_mode)
				{
				case GhostObj::SELECTED_MODE::NONE:
				{}
				CASE GhostObj::SELECTED_MODE::X :
				{
					selected_obj_->rotate(glm::rotate(r_click* abs(diff.x), X_DEFAULT));
					//selected_obj_->rotate(glm::rotate(r_click * abs(diff.x), selected_obj_->get_head_dir()));
				}
				CASE GhostObj::SELECTED_MODE::Y :
				{
					selected_obj_->rotate(glm::rotate(r_click* abs(diff.y), Y_DEFAULT));
					//selected_obj_->rotate(glm::rotate(r_click * abs(diff.y), selected_obj_->get_up_dir()));
				}
				CASE GhostObj::SELECTED_MODE::Z :
				{
					selected_obj_->rotate(glm::rotate(r_click* abs(diff.z), Z_DEFAULT));
					//selected_obj_->rotate(glm::rotate(r_click * abs(diff.z), selected_obj_->get_right_dir()));
				}
				CASE GhostObj::SELECTED_MODE::XYZ :
				{

				}
				}
			}
			CASE CONTROLL_MODE::SCALE :
			{
				switch (select_mode)
				{
				case GhostObj::SELECTED_MODE::NONE:
				{}
				CASE GhostObj::SELECTED_MODE::X :
				{
					selected_obj_->scaling(diff.x* X_DEFAULT + glm::vec3(1));
				}
				CASE GhostObj::SELECTED_MODE::Y :
				{
					selected_obj_->scaling(diff.y* Y_DEFAULT + glm::vec3(1));
				}
				CASE GhostObj::SELECTED_MODE::Z :
				{
					selected_obj_->scaling(diff.z* Z_DEFAULT + glm::vec3(1));
				}
				CASE GhostObj::SELECTED_MODE::XYZ :
				{

				}
				}
			}
			}


		}
		return true;
	}


	if (mm.get_R_click())
	{
		{
			// 회전
			float xoffset = pos.xpos - mm.get_prev_x(); xoffset /= 10;
			float yoffset = pos.ypos - mm.get_prev_y(); yoffset /= 10;
			yoffset = -yoffset;

			rotator_.yaw += xoffset;
			rotator_.pitch += yoffset;

			if (bool constrainPitch = true)
			{
				if (rotator_.pitch > 89.0f)
					rotator_.pitch = 89.0f;
				if (rotator_.pitch < -89.0f)
					rotator_.pitch = -89.0f;
			}

			return true;
		}

	}
	return false;
}

bool GhostObj::process_input(const KeyBoardEventManager::key_event& key)
{
	bool pressed = (key.action != GLFW_RELEASE);
	switch (key.key)
	{
	case GLFW_KEY_W:
	{
		front_on_ = pressed;
	}
	CASE GLFW_KEY_S :
	{
		back_on_ = pressed;
	}
	CASE GLFW_KEY_A :
	{
		left_on_ = pressed;
	}
	CASE GLFW_KEY_D :
	{
		right_on_ = pressed;
	}
	CASE GLFW_KEY_E :
	{
		up_on_ = pressed;
	}
	CASE GLFW_KEY_F :
	{
		//auto& mm = MouseEventManager::get(); auto& gamescene = GameScene::get();
		//auto mouse_ray = Ray::create(gamescene.get_main_camera(), mm.get_prev_x(), mm.get_prev_y());
		//float dist;
		if (selected_obj_)
		{
			selected_obj_->move(GameScene::get().get_main_camera()->get_position() - selected_obj_->get_position());
		}
	}
	CASE GLFW_KEY_Q :
	{
		down_on_ = pressed;
	}
	CASE GLFW_KEY_LEFT_SHIFT :
	{
		speed_ += 5 * pressed;
	}
	CASE GLFW_KEY_SPACE :
	{
		move(GameScene::get().get_ghost()->get_position() - get_position());
	}
	CASE GLFW_KEY_APOSTROPHE : // '
	{
		if (pressed)
		{
			GameScene::get().get_main_camera()->set_target(glm::vec3{ 476.071,3.697,46.121 });
			set_position_nobounding({ 474.708,2.549,43.205 });

			auto& first = GameScene::get().get_cars()[1];
			auto& second = GameScene::get().get_cars()[2];
			auto& third = GameScene::get().get_cars()[3];

			first->set_position_nobounding({ 474.4,2.581,27.062 });
			first->set_scale_nobounding(glm::vec3{ 0.14f });
			first->set_rotation_nobounding({ -0.663, 0,0.749,0 });

			second->set_position_nobounding({ 470.908,1.773,26.042 });
			second->set_scale_nobounding(glm::vec3{ 0.135f });
			second->set_rotation_nobounding({ -0.690, 0,0.724,0 });

			third->set_position_nobounding({ 477.55,0.254,25.249 });
			third->set_scale_nobounding(glm::vec3{ 0.12f });
			third->set_rotation_nobounding({ 0.645, 0,-0.764,0 });
		}
	}
	break; default: return false;
	}
	return true;
}

