#pragma once

#include "Model.h"
#include "Bone.h"

struct AssimpNodeData
{
	glm::mat4 transformation;
	std::string name;
	int childrenCount;
	std::vector<AssimpNodeData> children;
};

class Animation
{
public:
	Animation() = default;
	Animation(const std::string& animationPath, Model* model)
	{
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(animationPath, aiProcess_Triangulate);
		assert(scene && scene->mRootNode);
		auto animation = scene->mAnimations[0];
		Duration = animation->mDuration;
		TicksPerSecond = animation->mTicksPerSecond;
		ReadHeirarchyData(RootNode, scene->mRootNode);
		ReadMissingBones(animation, *model);
	}
	~Animation()
	{
	}
	Bone* FindBone(const std::string& name)
	{
		auto iter = std::find_if(Bones.begin(), Bones.end(),
			[&](const Bone& Bone)
			{
				return Bone.GetBoneName() == name;
			}
		);
		if (iter == Bones.end()) return nullptr;
		else return &(*iter);
	}
	inline float GetTicksPerSecond() { return TicksPerSecond; }
	inline float GetDuration() { return Duration; }
	inline const AssimpNodeData& GetRootNode() { return RootNode; }
	inline const std::map<std::string, BoneInfo>& GetBoneIDMap()
	{
		return BoneInfoMap;
	}
private:
	void ReadMissingBones(const aiAnimation* animation, Model& model)
	{
		int size = animation->mNumChannels;

		auto& boneInfoMap = model.GetBoneInfoMap();//getting m_BoneInfoMap from Model class
		int& boneCount = model.GetBoneCount(); //getting the m_BoneCounter from Model class

		//reading channels(bones engaged in an animation and their keyframes)
		for (int i = 0; i < size; i++)
		{
			auto channel = animation->mChannels[i];
			std::string boneName = channel->mNodeName.data;

			if (boneInfoMap.find(boneName) == boneInfoMap.end())
			{
				boneInfoMap[boneName].id = boneCount;
				boneCount++;
			}
			Bones.push_back(Bone(channel->mNodeName.data,
				boneInfoMap[channel->mNodeName.data].id, channel));
		}

		BoneInfoMap = boneInfoMap;
	}
	void ReadHeirarchyData(AssimpNodeData& dest, const aiNode* src)
	{
		assert(src);

		dest.name = src->mName.data;
		dest.transformation = AssimpGLMHelpers::ConvertMatrixToGLMFormat(src->mTransformation);
		dest.childrenCount = src->mNumChildren;

		for (int i = 0; i < src->mNumChildren; i++)
		{
			AssimpNodeData newData;
			ReadHeirarchyData(newData, src->mChildren[i]);
			dest.children.push_back(newData);
		}
	}
	float Duration;
	int TicksPerSecond;
	std::vector<Bone> Bones;
	AssimpNodeData RootNode;
	std::map<std::string, BoneInfo> BoneInfoMap;
};


using AnimatorPtr = shared_ptr<class Animator>;
class Animator
{
	CREATE_SHARED(Animator);
public:
	Animator(Animation* Animation)
	{
		CurrentTime = 0.0;
		CurrentAnimation = Animation;

		FinalBoneMatrices.reserve(100);

		for (int i = 0; i < 100; i++)
			FinalBoneMatrices.push_back(glm::mat4(1.0f));
	}

	void UpdateAnimation(float dt)
	{
		DeltaTime = dt;
		if (CurrentAnimation)
		{
			CurrentTime += CurrentAnimation->GetTicksPerSecond() * dt;
			CurrentTime = fmod(CurrentTime, CurrentAnimation->GetDuration());
			CalculateBoneTransform(&CurrentAnimation->GetRootNode(), glm::mat4(1.0f));
		}
	}

	void PlayAnimation(Animation* pAnimation)
	{
		if (CurrentAnimation == pAnimation) return;
		CurrentAnimation = pAnimation;
		CurrentTime = 0.0f;
	}

	void CalculateBoneTransform(const AssimpNodeData* node, glm::mat4 parentTransform)
	{
		std::string nodeName = node->name;
		glm::mat4 nodeTransform = node->transformation;

		Bone* Bone = CurrentAnimation->FindBone(nodeName);

		if (Bone)
		{
			Bone->Update(CurrentTime);
			nodeTransform = Bone->GetLocalTransform();
		}

		glm::mat4 globalTransformation = parentTransform * nodeTransform;

		auto boneInfoMap = CurrentAnimation->GetBoneIDMap();
		if (boneInfoMap.find(nodeName) != boneInfoMap.end())
		{
			int index = boneInfoMap[nodeName].id;
			glm::mat4 offset = boneInfoMap[nodeName].offset;
			FinalBoneMatrices[index] = globalTransformation * offset;
		}

		for (int i = 0; i < node->childrenCount; i++)
			CalculateBoneTransform(&node->children[i], globalTransformation);
	}

	std::vector<glm::mat4> GetFinalBoneMatrices()
	{
		return FinalBoneMatrices;
	}

	bool IsAnimated() { return CurrentAnimation; }
private:
	std::vector<glm::mat4> FinalBoneMatrices;
	Animation* CurrentAnimation{};
	float CurrentTime;
	float DeltaTime;
};