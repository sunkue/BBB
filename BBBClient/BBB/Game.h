#pragma once
#include "timer.h"
#include "Renderer.h"
#include "GameScene.h"
#include "DynamicObj.h"
#include "Event.h"
#include "FileHelper.h"
#include "Networker.h"
#include "Player.h"

enum class GameMode
{
	TITLE,
	PLAY,
	EDIT
};

class Game : public IDataOnFile
{

	SINGLE_TON(Game)
	{
		load("[]GAME");
		cerr << "[@]version::" << version << endl;
		cerr << "[@]serverIP::" << serveraddr_ << endl;
	}
	~Game()
	{
		cerr << "[@]version::" << version << endl;
		constexpr string_view versionFile = "[]GAME.txt";
		ofstream ofile{ initpath(versionFile).data(), ios::out };
		save_file_impl(ofile);

		if (networker.joinable())
			networker.join();
	}

	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);

public:
	void update()
	{
		auto milli_tick = TimerSystem::get().tick_time();
		auto f_milli_tick = TimerSystem::get().tick_timef();
		eventmanager.update(milli_tick);
		renderer.get_scene()->update(milli_tick);
		TimerSystem::get().tick();
	}


public:
	bool set_mode_once = false;
	void setGameModePlay()
	{
		if (set_mode_once)
			return;
		gamemode_ = GameMode::PLAY;
		Networker::get().connect(serveraddr_.c_str());
		if (networker.joinable())networker.detach();
		networker = thread{ &Networker::do_recv, &Networker::get() };
		set_mode_once = true;
	}
	void setGameModeEdit()
	{
		if (set_mode_once)
			return;
		gamemode_ = GameMode::EDIT;
		set_mode_once = true;
		Renderer::get().changeSceneWithLoading(&GameScene::get());
	}

	bool isEditMode() { return GameMode::EDIT == gamemode_; }
	bool isPlayMode() { return GameMode::PLAY == gamemode_; }
public:
	Renderer& renderer = Renderer::get();
	TimerSystem& game_system = TimerSystem::get();
	EventManager& eventmanager = EventManager::get();
	GLFWwindow* window = nullptr;
	atomic<GameMode> gamemode_;
public:
	thread networker;
	mutex gamelock;
	string serveraddr_;
	size_t version;
	atomic_bool gameEnd{};
	GameRoomID roomid;
	InGameID playerid;
	array<Player, MAX_PLAYER + MAX_BOT> players;
};

