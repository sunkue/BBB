#pragma once

#include "DynamicObj.h"

#pragma warning(push)
#pragma warning(disable: 26812)
#pragma warning(disable: 26495)
BETTER_ENUM
(
	CONTROLL_MODE, int
	, TRANSLATE
	, ROTATE
	, SCALE
);
#pragma warning(pop)

using GhostPtr = unique_ptr<class GhostObj>;
class GhostObj : public DynamicObj
{
public:
	GhostObj(const ModelPtr& model) :DynamicObj{ model } {}
public:
	virtual void update(milliseconds time_elapsed) override;
	virtual void update_camera(Camera* camera, float time_elpased) const override;
	virtual void draw(const ShaderPtr& shader)const override;
	virtual void update_uniform_vars(const ShaderPtr& shader)const override;
	virtual void draw_gui() override;
	virtual bool process_input(const KeyBoardEventManager::key_event& key) override;
	virtual bool process_input(const MouseEventManager::scroll_event& scroll) override;
	virtual bool process_input(const MouseEventManager::button_event& button) override;
	virtual bool process_input(const MouseEventManager::pos_event& pos) override;

private:
	ObjPtr xAxis = make_shared<Obj>(Model::box_red());
	ObjPtr yAxis = make_shared<Obj>(Model::box_blue());
	ObjPtr zAxis = make_shared<Obj>(Model::box_green());

	enum class SELECTED_MODE
	{
		NONE,
		X,
		Y,
		Z,
		XYZ
	} select_mode;

public:
	CONTROLL_MODE cntl_mode = CONTROLL_MODE::TRANSLATE;
private:
	bool draw_boundings_ = true;
public:
	GET(selected_obj);
private:
	ObjPtr selected_obj_;
	rotator rotator_;

private:
	float speed_ = 150;
	bool up_on_ = false;
	bool down_on_ = false;
	bool front_on_ = false;
	bool back_on_ = false;
	bool right_on_ = false;
	bool left_on_ = false;
};


