#include "stdafx.h"
#include "AudioComponent.h"
#include "Game.h"
#include "AudioSystem.h"
#include "Obj.h"

AudioComponent::AudioComponent(Obj* owner, int updateOrder) :owner{ owner }
{

}

AudioComponent::~AudioComponent()
{

}

void AudioComponent::Update()
{
	for (auto& s : SoundTable)
		s.second->Set3DAttributes(owner->get_position());
}

void AudioComponent::OnUpdateWorldTransform()
{
}

void AudioComponent::PlaySound(const string& _name, bool another, float volume, bool loop)
{
	auto name = _name;

	if (another)
	{
		int i = 0;
		name += to_string(i);
		while (SoundTable.contains(name))
		{
			if (SoundTable[name]->GetPaused())
				break;;
			name = _name + to_string(++i);
		}
		SoundTable.insert(make_pair(name, AudioSystem::get().LoadSound(soundPath(_name))));
	}
	else if (!SoundTable.contains(name))
	{
		SoundTable.insert(make_pair(name, AudioSystem::get().LoadSound(soundPath(_name))));
	}

	SoundTable[name]->SetVolume(volume);
	SoundTable[name]->SetLoop(loop);
	SoundTable[name]->Play();
}

void AudioComponent::StopSound(const string& name)
{
	if (SoundTable.contains(name))
		SoundTable[name]->Stop();
}

void AudioComponent::StopAll()
{
	for (auto& s : SoundTable)
		s.second->Stop();
}
