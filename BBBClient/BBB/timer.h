#pragma once


/////////////////////////////////////////////////////////////

/* logic machine */
class TimerSystem
{
	SINGLE_TON(TimerSystem) = default;

public:
	void start() {
		elapsed_time_ = 0ms;
		server_delta_ = 0ms;
		prev_time_point_ = clk::now();
		game_time_ = 0ms;
	}
	void tick()
	{
		auto now = clk::now();
		elapsed_time_ = duration_cast<milliseconds>(now - prev_time_point_);
		prev_time_point_ = now;
		game_time_ += elapsed_time_;
	};

	milliseconds tick_time()
	{
		return elapsed_time_;
	}

	float tick_timef()
	{
		return elapsed_time_.count() / 1000.f;
	}

	clk::time_point GetServerTime()
	{
		return clk::now() + server_delta_;
	}

	milliseconds game_time()
	{
		return game_time_;
	}

	GET(server_delta);
	SET(server_delta);
	SET(start_time_point);
	GET(start_time_point);
private:
	milliseconds elapsed_time_{};
	milliseconds server_delta_{};
	milliseconds game_time_{0ms};
	clk::time_point prev_time_point_{};
	clk::time_point start_time_point_{};
};

/// //////////////////////////////////////////////////




///////////////////////////////////////


