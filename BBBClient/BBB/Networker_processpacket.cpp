#include "stdafx.h"
#include "Game.h"
#include "Networker.h"

void Networker::process_packet(const char* const packet)
{
	auto pck_base = reinterpret_cast<const packet_base<void>*>(packet);
	packet_size_t pck_size = pck_base->size;
	PACKET_TYPE pck_type = pck_base->packet_type;

	auto packet_type = reinterpret_cast<const packet_base<void>*>(packet)->packet_type;
	// cerr << "[RECV::" << +packet_type._to_string() << "]" << endl;

	switch (pck_type)
	{
	case PACKET_TYPE::Sc_servertime:
	{
		auto pck = reinterpret_cast<const sc_servertime*>(packet);
		auto serverTimeDelta = duration_cast<milliseconds>(clk::now() - pck->server_time);
		TimerSystem::get().set_server_delta(serverTimeDelta);
	}
	CASE PACKET_TYPE::Sc_accept:
	{
		cerr << "[ACEEPTED]" << endl;
		auto pck = reinterpret_cast<const sc_accept*>(packet);
		Game::get().roomid = pck->ur_gameroomid;
		Game::get().playerid = pck->ur_ingameid;
		Renderer::get().changeSceneWithLoading(&MatchingScene::get());
	}
	CASE PACKET_TYPE::Sc_new_matcher :
	{
		auto pck = reinterpret_cast<const sc_new_matcher*>(packet);
		auto newid = pck->new_matcher_ingameid;
		lock_guard lock{ Game::get().gamelock };
		Game::get().players[newid].Connect(GameScene::get().get_cars()[newid]);
		Game::get().players[newid].ChangeReady(pck->ready);
	}
	CASE PACKET_TYPE::Sc_change_ready :
	{
		auto pck = reinterpret_cast<const sc_change_ready*>(packet);
		auto newid = pck->ingameid;
		lock_guard lock{ Game::get().gamelock };
		Game::get().players[newid].ChangeReady(pck->ready);
	}
	CASE PACKET_TYPE::Sc_matchcomplete :
	{
		// ready...
		Game::get().renderer.changeSceneWithLoading(&GameScene::get());
		
		cs_ready2run ready2run;
		do_send(&ready2run);
	}
	CASE PACKET_TYPE::Sc_gamestart :
	{
		auto pck = reinterpret_cast<const sc_gamestart*>(packet);
		{
			lock_guard lock{ Game::get().gamelock };
			TimerSystem::get().set_start_time_point(pck->starttime);
			for (auto& item : GameScene::get().get_track()->get_items())
			{
				item->initseed(pck->seed);
			}
		}
	}
	CASE PACKET_TYPE::Sc_remove_player :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_remove_player*>(packet);
		lock_guard lock{ Game::get().gamelock };
		Game::get().players[pck->removed_player_ingameid].Disconnect();
	}
	CASE PACKET_TYPE::Sc_set_player_info :
	{
		if (Game::get().gameEnd) return;
		auto& game = Game::get();
		auto pck = reinterpret_cast<const sc_set_player_info*>(packet);
		if (pck->timestamp < clk::now()) // 이전에 받은 타입스탬프
		{
			//cout << duration_cast<milliseconds>(TimerSystem::get().GetServerTime() - pck->timestamp) << endl;
		}

		auto& vehicle = game.players[pck->ingameid].get_vehicle();
		vehicle->set_rank(pck->rank);

		if (pck->ingameid == game.playerid)
			return;

		// #define 으로 NET_DECODE, NET_ENCODE 만들기.
		auto linear_speed = decltype(pck->linear_speed)::decode<glm::vec3>(pck->linear_speed);
		auto rotate = decltype(pck->rotate)::decode<glm::quat>(pck->rotate);
		auto pos = decltype(pck->pos)::decode<glm::vec3>(pck->pos);
		auto constY = vehicle->get_position().y;
		pos.y = constY;

		auto currentServerTime = TimerSystem::get().GetServerTime();
		auto clientsPingTime = duration_cast<milliseconds>(currentServerTime - pck->timestamp);
		constexpr auto NET_PING_EXTRAPOLATION = 0.8f;
		auto extrapolationDeltaTime = clientsPingTime * NET_PING_EXTRAPOLATION;
		auto deltaTimef = extrapolationDeltaTime.count() / 1000.f;
		glm::vec3 targetPos = pos + linear_speed * deltaTimef;
		constexpr auto POSITION_EXTRAPOLATION_LERP = 0.8f;
		glm::vec3 newPos = glm::lerp(pos, targetPos, glm::vec3{ POSITION_EXTRAPOLATION_LERP, 0 ,POSITION_EXTRAPOLATION_LERP });
		constexpr auto SPEED_EXTRAPOLATION = 0.6f;
		glm::vec3 newLinearSpeed = linear_speed + (targetPos - vehicle->get_position()) * deltaTimef * SPEED_EXTRAPOLATION;

		constexpr float HARDSNAP_DIFF = 4;
		if (HARDSNAP_DIFF < glm::length(newPos - pos))
		{
			// cerr << "HARDSNAP" << (int)pck->ingameid << endl;
			lock_guard lock{ game.gamelock };
			if (vehicle->get_collided()) break;
			vehicle->move(pos - vehicle->get_position());
			vehicle->rotate(glm::inverse(vehicle->get_rotation()) * rotate);
			//vehicle->set_position_nobounding(pos);
			//vehicle->set_rotation_nobounding(rotate);
			vehicle->set_linear_speed(linear_speed);
		}
		else
		{
			lock_guard lock{ game.gamelock };
			if (vehicle->get_collided()) break;
			vehicle->move(newPos - vehicle->get_position());
			vehicle->rotate(glm::inverse(vehicle->get_rotation()) * rotate);
			//vehicle->set_position_nobounding(newPos);
			//vehicle->set_rotation_nobounding(rotate);
			vehicle->set_linear_speed(newLinearSpeed);
		}
	}
	CASE PACKET_TYPE::Sc_player_accquire_item :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_player_accquire_item*>(packet);
		auto vehicle = Game::get().players[pck->ingameid].get_vehicle();

		lock_guard lock{ Game::get().gamelock };
		bool success = vehicle->NetTryAcquireItem(+ItemType::_from_integral(pck->itemtype), pck->itemobjid);
		if (!success)
		{
			cerr << "PACKET_TYPE::Sc_player_accquire_item(vehicle::NetTryAcquireItem)::FAIL" << endl;
			terminate();
		}
	}
	CASE PACKET_TYPE::Sc_player_use_item :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_player_accquire_item*>(packet);
		auto vehicle = Game::get().players[pck->ingameid].get_vehicle();

		lock_guard lock{ Game::get().gamelock };
		auto used = vehicle->NetUseItem();
		if (+ItemType::_from_integral(pck->itemtype) != used)
		{
			cerr << "PACKET_TYPE::Sc_player_use_item(vehicle::NetUseItem)::MISMATCH" << endl;
			terminate();
		}
		// do item effect func.
	}
	CASE PACKET_TYPE::Sc_outed :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_outed*>(packet);
		auto vehicle = Game::get().players[pck->ingameid].get_vehicle();
		lock_guard lock{ Game::get().gamelock };
		vehicle->set_outed(true);
	}
	CASE PACKET_TYPE::Sc_regen :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_regen*>(packet);
		auto vehicle = Game::get().players[pck->ingameid].get_vehicle();
		lock_guard lock{ Game::get().gamelock };
		vehicle->regenerate(pck->includedtracknode);
	}
	CASE PACKET_TYPE::Sc_clearlab :
	{
		if (Game::get().gameEnd) return;
		auto pck = reinterpret_cast<const sc_clearlab*>(packet);
		auto vehicle = Game::get().players[pck->ingameid].get_vehicle();
		lock_guard lock{ Game::get().gamelock };
		vehicle->clear_lab();
		if (MAX_LAB <= vehicle->get_lab())
			vehicle->set_clear_time(pck->clearAt);
	}
	CASE PACKET_TYPE::Sc_gameend :
	{
		auto pck = reinterpret_cast<const sc_gameend*>(packet);
		auto endAt = duration_cast<milliseconds>(pck->endtime - TimerSystem::get().GetServerTime());
		EventManager::get().Add(move(make_shared<GameClearEvent>(endAt)));
	}
	break; default: break;
	}
}