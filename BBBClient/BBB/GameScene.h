#pragma once

#include "Renderer.h"
#include "Scene.h"
#include "DynamicObj.h"
#include "Camera.h"
#include "Shader.h"
#include "Sun.h"
#include "BO.h"
#include "TrackNode.h"
#include "Vehicle.h"
#include "texture.h"
#include "Ghost.h"
#include "Terrain.h"
#include "Particle.h"
#include "Deco.h"
#include "AudioComponent.h"
#include "AudioSystem.h"

//////////////////////////////////////////////////////

// [-1, 1] pos // [0,1] tex // screen covered quad.
class ScreenQuad
{
	SINGLE_TON(ScreenQuad);
public:
	// set using shader and uniforms first.
	void draw_quad();
private:
	GLuint quad_vao;
};

//////////////////////////////////////////////////////

struct GodRayParam : public IDataOnFile
{
protected:
	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);

public:
	bool enable_godray = true;

	float u_samples = 128;		// 샘플링수
	float u_decay = 0.98;		// 감쇠(샘플링 누적감쇠)
	float u_density = 0.9;		// 샘플링밀도
	float u_weight = 0.07;		// 가중치(샘플링 가중치)
	float u_exposure = 0.15;	// 최종노출도

	void draw_gui()
	{
		//gui::Begin("GodRay");
		gui::Text("Godray");
		GUISAVE(); GUILOAD();
		gui::Checkbox("Enable", &enable_godray);
		gui::DragFloat("samples", &u_samples, 0.1f, 0, 512, "%.f", 1);
		gui::DragFloat("decay", &u_decay, 0.001f, 0.9, 1, "%.3f", 1);
		gui::DragFloat("density", &u_density, 0.01f, 0, 1, "%.2f", 1);
		gui::DragFloat("weight", &u_weight, 0.001f, 0, 0.1, "%.3f", 1);
		gui::DragFloat("exposure", &u_exposure, 0.01f, 0, 1, "%.2f", 1);
		//gui::End();

	}

	void update_uniform_vars(const ShaderPtr& shader, const CameraPtr& camera);
};

class SunRenderer
{
public:
	friend class Renderer;

	GLuint sky_fbo;
	TexturePtr skypass_tbo;
	//	ShaderPtr skypass_shader;

		// /*
	GLuint godray_fbo;
	TexturePtr godraypass_tbo;
	ShaderPtr godraypass_shader;

	GodRayParam godray_param;
	/**/

	void init();

	void bind_sky_fbo()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, sky_fbo);
		glViewport(0, 0, Screen::mainScreen().fixedwidth, Screen::mainScreen().fixedheight);
		glClear(GL_COLOR_BUFFER_BIT);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

	}

	void draw_quad()
	{
		ScreenQuad::get().draw_quad();
	}

	void bind_godray_fbo()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, godray_fbo);
		glViewport(0, 0, Screen::mainScreen().fixedwidth, Screen::mainScreen().fixedheight);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void draw_godray(const CameraPtr& camera)
	{
		if (false == godray_param.enable_godray)
		{
			return;
		}

		godraypass_shader->use();
		godraypass_shader->set("skypass_texture", skypass_tbo);
		godray_param.update_uniform_vars(godraypass_shader, camera);

		draw_quad();
	}
};

struct ShadowParam : public IDataOnFile
{
	SINGLE_TON(ShadowParam) { load("ShadowParam"); }
	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);
public:
	float shadowbias1;
	float shadowbias2;
	int shadowsample;
	float shadowcamresolution;
	int preset;
	void draw_gui()
	{
		gui::Begin("shadowparam");

		gui::DragFloat("shadowbias1", &shadowbias1, 0.000001f, 0, 0.1, "%6f");
		gui::DragFloat("shadowbias2", &shadowbias2, 0.000001f, 0, 0.1, "%6f");
		gui::DragInt("shadowsample", &shadowsample, 1, 0, 4);
		gui::DragFloat("shadowcamresolution", &shadowcamresolution);

		GUISAVE();
		GUILOAD();

		for (int i = 0; i < preset; i++)
		{
			auto presetname = "ShadowParamPreset"s.append(to_string(i));
			if (gui::Button(presetname.c_str()))
			{
				ShadowParam temp;
				temp.load(presetname);
				shadowbias1 = temp.shadowbias1;
				shadowbias2 = temp.shadowbias2;
				shadowsample = temp.shadowsample;
				shadowcamresolution = temp.shadowcamresolution;
			}
		}


		gui::End();
	}

	void update_uniform_vars(const ShaderPtr& shader)
	{
		shader->use();

		shader->set("u_shadowbias1", shadowbias1);
		shader->set("u_shadowbias2", shadowbias2);
		shader->set("u_shadowsample", shadowsample);
	}
};

class gBufferRenderer
{
public:
	friend class Renderer;

	GLuint gbuffer_fbo;
	TexturePtr worldpos_tbo;
	TexturePtr normal_tbo;
	TexturePtr depth_tbo;
	TexturePtr albedospec_tbo;
	GLuint rboDepthStencil;


	GLuint lightpass_fbo;
	TexturePtr lightpass_tbo;

	ShaderPtr lightpass_shader;

	void init();

	void bind_gbuffer_fbo()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);
		glViewport(0, 0, Screen::mainScreen().fixedwidth, Screen::mainScreen().fixedheight);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // background color =>alpha값 체킹
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		// draw objs
	}

	void bind_lightpass_fbo()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, lightpass_fbo);
		glViewport(0, 0, Screen::mainScreen().fixedwidth, Screen::mainScreen().fixedheight);
		glClear(GL_COLOR_BUFFER_BIT);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		lightpass_shader->use();

		ShadowParam::get().update_uniform_vars(lightpass_shader);

		lightpass_shader->set("g_world_pos", worldpos_tbo);
		lightpass_shader->set("g_normal", normal_tbo);
		lightpass_shader->set("g_albedospec", albedospec_tbo);

		// set light uniforms..
	}

	void draw_quad()
	{
		lightpass_shader->use();
		ScreenQuad::get().draw_quad();
	}

};

class DepthRenderer
{
	// 미리계산가능한 섀도우맵은 미리 계산해두자,
public:
	friend class Renderer;
	static const GLuint SHADOW_WIDTH_H = 1024 * 15, SHADOW_HEIGHT_H = 1024 * 15;
	static const GLuint SHADOW_WIDTH_L = 1024, SHADOW_HEIGHT_L = 1024;

	GLuint directional_depthmap_fbo;
	TexturePtr directional_depthmap_tbo;
	vector<glm::mat4> directional_lightspace_mat;

	GLuint point_depthcubemap_fbo;
	TexturePtr point_depthcubemap_tbo;
	vector<array<glm::mat4, 6>> point_lightspace_mat;

	void init()
	{
		// directional_depthmap
		glGenFramebuffers(1, &directional_depthmap_fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, directional_depthmap_fbo);

		directional_depthmap_tbo = Texture::create();
		glGenTextures(1, &directional_depthmap_tbo->id);
		glBindTexture(GL_TEXTURE_2D, directional_depthmap_tbo->id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH_H, SHADOW_HEIGHT_H, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		float bordercolor[] = { 1.0,1.0,1.0,1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bordercolor);

		glBindFramebuffer(GL_FRAMEBUFFER, directional_depthmap_fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, directional_depthmap_tbo->id, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		// point_depthcubemap
		glGenFramebuffers(1, &point_depthcubemap_fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, point_depthcubemap_fbo);

		point_depthcubemap_tbo = Texture::create();
		glGenTextures(1, &point_depthcubemap_tbo->id);
		glBindTexture(GL_TEXTURE_CUBE_MAP, point_depthcubemap_tbo->id);
		for (GLuint i = 0; i < 6; ++i)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH_L, SHADOW_HEIGHT_L, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		glBindFramebuffer(GL_FRAMEBUFFER, point_depthcubemap_fbo);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, point_depthcubemap_tbo->id, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		//
	}


	int add_lightspace_mat(const PointLightPtr& light)
	{
		float aspect = (float)SHADOW_WIDTH_L / (float)SHADOW_HEIGHT_L;
		float n = Screen::mainScreen().n; float f = Screen::mainScreen().f;
		glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, n, f);
		auto lightpos = light->position;

		auto& mats = point_lightspace_mat.emplace_back();
		mats[0] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0));
		mats[1] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0));
		mats[2] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0));
		mats[3] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0));
		mats[4] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0));
		mats[5] = shadowProj * glm::lookAt(lightpos, lightpos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0));

		return point_lightspace_mat.size() - 1;
	}

	void bind_directional_depthmap_fbo()
	{
		glViewport(0, 0, SHADOW_WIDTH_H, SHADOW_HEIGHT_H);
		glBindFramebuffer(GL_FRAMEBUFFER, directional_depthmap_fbo);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	void set_directional_depthmap_shader(const ShaderPtr& depthmap_shader, int lightmat_index)
	{
		depthmap_shader->use();

		/// 동적그림자..
		auto resolution = ShadowParam::get().shadowcamresolution;
		float n = Screen::mainScreen().n; float f = Screen::mainScreen().f;
		glm::mat4 lightProjection = glm::ortho(-resolution, resolution, -resolution, resolution, n, f);
		glm::mat4 lightView = glm::lookAt(-Sky::get().get_sun_light()->direction * resolution, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 lightSpaceMatrix = lightProjection * lightView;

		depthmap_shader->set("u_lightpos_mat", lightSpaceMatrix);

		///

		//depthmap_shader->set("u_lightpos_mat", directional_lightspace_mat[lightmat_index]);
	}

	void bind_point_depthmap_fbo(const ShaderPtr& depthmap_shader, const PointLightPtr& light, int lightmat_index)
	{
		glViewport(0, 0, SHADOW_WIDTH_L, SHADOW_HEIGHT_L);
		glBindFramebuffer(GL_FRAMEBUFFER, point_depthcubemap_fbo);
		glClear(GL_DEPTH_BUFFER_BIT);

		depthmap_shader->use();
		depthmap_shader->set("u_lightpos_mat", point_lightspace_mat[lightmat_index]);
		depthmap_shader->set("u_lightpos", light->position);
		depthmap_shader->set("u_far_plane", Screen::mainScreen().f);
	}
};

class ScreenRenderer
{
public:
	friend class Renderer;

	GLuint color_fbo;
	TexturePtr color_tbo;

	GLuint emessive_fbo;
	TexturePtr emessive_tbo;
	GLuint emessiverboDepth;

	GLuint predraw_fbo;
	TexturePtr predraw_tbo;
	ShaderPtr predraw_shader;
	GLuint predrawrboDepth;
	ShaderPtr screen_shader;

	struct FogParameters
	{
		glm::vec3 color{ 0.6,0.6,0.9 };
		float density = 6.5;
	} fog;

public:
	void init();

	void blit_fbo(GLuint color_fbo, GLuint target_fbo)
	{
		auto& mainScreen = Screen::mainScreen();
		glBindFramebuffer(GL_READ_FRAMEBUFFER, color_fbo);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target_fbo);
		glBlitFramebuffer(0, 0, mainScreen.fixedwidth, mainScreen.fixedheight, 0, 0, mainScreen.fixedwidth, mainScreen.fixedheight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}

	void blit_depthstencil_fbo(GLuint depth_fbo, GLuint target_fbo = 0)
	{
		auto& mainScreen = Screen::mainScreen();
		glBindFramebuffer(GL_READ_FRAMEBUFFER, depth_fbo);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target_fbo);
		glBlitFramebuffer(0, 0, mainScreen.fixedwidth, mainScreen.fixedheight, 0, 0, mainScreen.fixedwidth, mainScreen.fixedheight, GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
	}

	void predraw_screen(TexturePtr& bg, TexturePtr& godray);


	
	void draw_screen(GLuint FRAMEBUFFER = 0, SHADER_MODE mode = SHADER_MODE::NONE);
};

class ParticleRenderer;

//////////////////////////////////////////////////////

class GameScene : public Scene
{
	SINGLE_TON(GameScene) { init(); };
public:
	virtual void enterScene() final;
	virtual void draw() final;
	virtual void update(milliseconds elapsed) final;
	virtual void leaveScene() final;
private:
	void ready_draw(const CameraPtr& camera);
public:
	void init();
	void init_player();
private:
	void init_shader();
	void init_resources();
	void load_model();
public:
	glm::mat4 proj_mat(const CameraPtr& camera)const { return screen_.proj_mat(camera); }
	glm::mat4 vp_mat(const CameraPtr& camera)const
	{
		auto p = proj_mat(camera);
		auto v = camera->view_mat();
		return p * v;
	}
	GET_REF(main_camera);
	GET_REF(backmirror_camera);
	GET_REF(minimap_camera);
	GET_REF(player);
	GET_REF(ghost);
	GET_REF(Tires);
	GET_REF(DecoObjects);
	SET(ghost);
	GET_REF_UNSAFE(cars);
	GET_REF(depth_renderer);
	GET_REF(screen_renderer);
	GET_REF(gbuffer_renderer);
	GET_REF(sun_renderer);
	GET_REF(particle_renderer);
	GET_REF_UNSAFE(particles);
	GET_REF_UNSAFE(track);
	GET_REF_UNSAFE(Terrain);
	void swap_player_ghost();
private:
	void update_ranks();
private:
	bool drawable_ = true;
	Screen& screen_ = Screen::mainScreen();
	CameraPtr main_camera_;
	CameraPtr backmirror_camera_;
	CameraPtr minimap_camera_;
	DirectionalLightPtr testing_directional_light_;
	PointLightPtr testing_point_light_;
	SpotLightPtr testing_spot_light_;
	unique_ptr<ScreenRenderer> screen_renderer_;
	unique_ptr<DepthRenderer> depth_renderer_;
	ShaderPtr directional_depthmap_shader_;
	ShaderPtr directional_grass_depthmap_shader_;
	ShaderPtr point_depthmap_shader_;
	unique_ptr<gBufferRenderer> gbuffer_renderer_;
	unique_ptr<SunRenderer> sun_renderer_;
	unique_ptr<ParticleRenderer> particle_renderer_;
	list<Particle> particles_;
	ObjPtr sun_;
	ShaderPtr sun_w_shader_;
	ShaderPtr sun_b_shader_;
	ShaderPtr skypass_shader_;
	UBO<glm::mat4> ubo_vp_mat;
	UBO<glm::mat4> ubo_inv_v_mat;
	UBO<glm::mat4> ubo_inv_p_mat;
	UBO<glm::mat4> ubo_lightspace_mat;
	UBO<DirectionalLight> ubo_sun;
	UBO<glm::vec2> ubo_default_buffer_resolution;
	UBO<glm::vec3> ubo_camerapos;
	ShaderPtr default_g_shader_;
	ShaderPtr normalmap_g_shader_;
	ShaderPtr grass_g_shader_;
	InstancingObjPtr grasses_;
	array<VehiclePtr, MAX_VEHICLE> cars_;
	array<ObjPtr, 6> Tires_;
	vector<DecoPtr> DecoObjects_;
	ObjPtr player_;
	ObjPtr ghost_;
	Track* track_;
	Terrain* Terrain_;
};