#pragma once
#include <string>
#include "Math.h"

enum class eSoundType
{
    None
    , BackGroundMusic
    , EffectSound
    , __Size
};

class Sound
{
protected:
    friend class AudioSystem;
    Sound(ISound* irrklangSoundInterface);
    ~Sound();
public:
    void Restart();
    void Play();
    void Stop();
    void Pause();
   
    void SetVolume(float value);
    void SetLoop(bool loop);

    bool GetPaused() const;
    float GetVolume() const;
    void Set3DAttributes(const glm::vec3& pos);
    GET(Type);
private:
    ISound* Sound_;
    eSoundType Type_;
};