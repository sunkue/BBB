#include "stdafx.h"
#include "Event.h"
#include "UI.h"
#include "Renderer.h"
#include "AudioSystem.h"
#include "Scene.h"

///////////////////////////////////////////////////////

void EventManager::update(milliseconds time)
{
	int i = 0;
	for (auto it = EventList.begin(), before_it = EventList.before_begin(); it != EventList.end();)
	{
		i++;
		if (!(*it)->enable)
		{
			(*it)->Detroy();
			it = EventList.erase_after(before_it);
			continue;
		}

		it->get()->Update(time);
		before_it = it; it++;
	}
}

void EventManager::Add(EventPtr e)
{
	EventList.emplace_front(::move(e))->Start();
}

///////////////////////////////////////////////////////



///////////////////////////////////////////////////////

void GameClearEvent::Start()
{
	// gamescene enable countdown messege
	sound = AudioSystem::get().LoadSound(soundPath("countdown.mp3"), false);
	AudioSystem::get().PlaySound(sound);
	AudioSystem::get().ForceUpdate();
}

void GameClearEvent::Update(milliseconds elapsed)
{
	clearAt -= elapsed;
	GameUI::get().get_textUi().countdown_ = clearAt.count() / 1000;
	// gamescene set coundown milliseconds

	if (5s < clearAt && clearAt <= 5.5s && !soundFlag)
	{
		soundFlag = true;
		sound->Pause();
		AudioSystem::get().ForceUpdate();
	}

	if (clearAt <= 5s && soundFlag)
	{
		soundFlag = false;
		sound->Play();
		AudioSystem::get().ForceUpdate();
	}

	if (clearAt <= 0ms)
		Event::done();
}

void GameClearEvent::Detroy()
{
	// scene change.
	
	Renderer::get().changeSceneWithLoading(&GameEndScene::get());
}
