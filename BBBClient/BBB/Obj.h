#pragma once

#include "Light.h"
#include "Model.h"
#include "MouseEvent.h"
#include "KeyboardEvent.h"
#include "Collision.h"
#include "Animation.h"
#include "AudioComponent.h"

/////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////
class Obj;
class Camera;

__interface IObj
{
	void update_uniform_vars(const ShaderPtr& shader)const;
	void update(milliseconds time_elapsed);
	void draw(const ShaderPtr& shader)const;
	void update_camera(class Camera* camera, float time_elpased) const;
	void on_collide(Obj& other, OUT void* context = nullptr); // 자기 자신의 변화만 적용.
	void on_uncollide(Obj& other, OUT void* context = nullptr);
	bool process_input(const KeyBoardEventManager::key_event& key);
	bool process_input(const MouseEventManager::scroll_event& scroll);
	bool process_input(const MouseEventManager::button_event& button);
	bool process_input(const MouseEventManager::pos_event& pos);
	void draw_gui();
};


/* 그려지는 모든 물체 */
class Obj : public IObj, public IDataOnFile
{
protected:
	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);
public:
	GET(objid);
	SET(objid);
private:
	ObjID objid_{ -1 };
public:

	explicit Obj(const ModelPtr& model = Model::no_model()
		, const unordered_map<string, Animation*>& animations = {}
		, const AnimatorPtr& Animator = Animator::create(nullptr)) 
		: model_{ model }, animator_{ Animator }, animations_{ animations }
	, Audio_{ make_unique<AudioComponent>(this) } {}
	virtual ~Obj() {}

	glm::mat4 model_mat()const { return glm::translate(translate_) * glm::toMat4(quaternion_) * glm::scale(scale_); }

	/* 바운딩을 건들여야 하므로 신중히 사용 할 것.*/
	void set_position_nobounding(glm::vec3 pos) { translate_ = pos; }
	void set_rotation_nobounding(glm::quat q) { quaternion_ = q; }
	void set_scale_nobounding(glm::vec3 sale) { scale_ = sale; }

	glm::vec3 get_position()const { return translate_; }
	glm::quat get_rotation()const { return quaternion_; }
	glm::vec3 get_scale()const { return scale_; }

	glm::vec3 get_head_dir()const { return get_rotation() * HEADING_DEFAULT; }
	glm::vec3 get_up_dir()const { return get_rotation() * UP_DEFAULT; }
	glm::vec3 get_right_dir()const { return get_rotation() * RIGHT_DEFAULT; }

	void rotate(const glm::quat& q) { quaternion_ *= q; boundings_.rotate(q); }
	void move(const glm::vec3& dif) { translate_ += dif; boundings_.move(dif); }
	void scaling(const glm::vec3& ratio) { scale_ *= ratio; boundings_.scaling(ratio); }

	void trans_boundings();

public:
	virtual void update_uniform_vars(const ShaderPtr& shader)const;

	virtual void update(milliseconds time_elapsed);

	virtual void draw(const ShaderPtr& shader)const;

	void draw(const ShaderPtr& shader, const ModelPtr& model) const;
	void draw_edge(const ShaderPtr& shader, const ModelPtr& model, float scalef = 1.0675f);

	virtual void draw_gui();

	bool collision_detect(const Obj& other)const;
	virtual void on_collide(Obj& other, OUT void* context = nullptr) {};
	virtual void on_uncollide(Obj& other, OUT void* context = nullptr) {};

	virtual void update_camera(Camera* camera, float time_elpased) const {}
	virtual bool process_input(const KeyBoardEventManager::key_event& key) { return false; };
	virtual bool process_input(const MouseEventManager::scroll_event& scroll) { return false; };
	virtual bool process_input(const MouseEventManager::button_event& button) { return false; };
	virtual bool process_input(const MouseEventManager::pos_event& pos) { return false; };

public:
	SET(model);
	GET(force);
	SET(force);
	GET(force2);
	SET(force2);
	GET(collided);
	SET(collided);

private:
	glm::vec3 translate_{};
	glm::quat quaternion_{};
	glm::vec3 scale_{ V3_DEFAULT };
	glm::vec3 force_{};
	glm::vec3 force2_{};
	bool collided_{ false };

public:
	GET_REF(model);
	GET_REF_UNSAFE(animator);
	GET_REF_UNSAFE(animations);
private:
	ModelPtr model_;
	AnimatorPtr animator_;
	unordered_map<string, Animation*> animations_;
public:
	GET_REF(boundings);
	GET_REF(Audio);
	Boundings boundings_;
private:
	unique_ptr<AudioComponent> Audio_;
public:
	GET(enable);
	SET(enable);
private:
	bool enable_ = true;
};
using ObjPtr = shared_ptr<Obj>;

///////////////////////////////////////////


class InstancingObj
{
public:
	void setup_mesh(const vector<Vertex>& vertices)
	{
		GLuint abo;
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &abo);

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, abo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, pos));

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, nor));

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, tex));

		glBindVertexArray(0);

		verticlesize_ = vertices.size();
	}

	void setup_instance_attribute(string_view name, const auto* data, GLenum type = GL_FLOAT, GLuint divisor = 1)
	{
		GLuint inst_abo;
		glGenBuffers(1, &inst_abo);
		glBindBuffer(GL_ARRAY_BUFFER, inst_abo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(*data) * num_inst_, data, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(vao);
		auto index = attbLoc_[name];
		//glGetAttribLocation(shader->get_shader_id(), name.data());
		glEnableVertexAttribArray(index);
		glBindBuffer(GL_ARRAY_BUFFER, inst_abo);
		glVertexAttribPointer(index, sizeof(*data) / 4, type, GL_FALSE, sizeof(*data), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glVertexAttribDivisor(index, divisor);
		glBindVertexArray(0);
	}

	void update_uniform_vars(const ShaderPtr& shader)const;


	void draw(const ShaderPtr& shader)const
	{
		shader->use();
		glBindVertexArray(vao);
		glDrawArraysInstanced(GL_TRIANGLES, 0, verticlesize_, num_inst_);
		glBindVertexArray(0);
		glUseProgram(0);
	}

	SET(num_inst);
	GET(num_inst);
	GET_REF(textures);

	void add_texture(TexturePtr& texture) { textures_.emplace_back(texture); }

	void add_instancing_attribute(string_view name, GLuint attbLoc) { attbLoc_[name] = attbLoc; }

private:
	GLuint vao;
	GLuint num_inst_;
	GLsizei verticlesize_;
	vector<TexturePtr> textures_;
	unordered_map<string_view, GLuint> attbLoc_;
};
using InstancingObjPtr = shared_ptr<InstancingObj>;

///////////////////////////////////////////
