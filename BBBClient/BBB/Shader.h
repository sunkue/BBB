#pragma once

using ShaderPtr = shared_ptr<class Shader>;
class Shader
{
public:
	inline static int binded_textures;
	void use()const;

	template<class T> void set(const string& uniform_var_name, const T& value)const;

	GET(shader_id);

	CREATE_SHARED(Shader);

	static ShaderPtr bouding()
	{
		vector<string> VS;
		vector<string> FS;
		vector<string> GS;
		VS.clear(); VS.emplace_back("./Shader/test_vertex.glsl"sv);
		FS.clear(); FS.emplace_back("./Shader/default_fragment.glsl"sv);
		GS.clear(); 
		static ShaderPtr basic = Shader::create(VS, FS, GS);
		return basic;
	}
	
	static ShaderPtr basic()
	{
		vector<string> VS;
		vector<string> FS;
		vector<string> GS;
		VS.clear(); VS.emplace_back("./Shader/gbuffer.vert"sv);
		FS.clear(); FS.emplace_back("./Shader/default.frag"sv);
		GS.clear();
		static ShaderPtr basic = Shader::create(VS, FS, GS);
		return basic;
	}

	static ShaderPtr maskParticle()
	{
		vector<string> VS;
		vector<string> FS;
		vector<string> GS;
		VS.clear(); VS.emplace_back("./Shader/gbuffer.vert"sv);
		FS.clear(); FS.emplace_back("./Shader/maskParticle.frag"sv);
		GS.clear();
		static ShaderPtr basic = Shader::create(VS, FS, GS);
		return basic;
	}

	static ShaderPtr gbufferNormalmap()
	{
		vector<string> VS;
		vector<string> FS;
		vector<string> GS;

		VS.clear(); VS.emplace_back("./Shader/gbuffer.vert"sv);
		FS.clear(); FS.emplace_back("./Shader/gbuffer_with_normalmap.frag"sv);
		GS.clear();
		static ShaderPtr basic = Shader::create(VS, FS, GS);
		return basic;
	}
	

	static ShaderPtr blur()
	{
		vector<string> VS;
		vector<string> FS;
		vector<string> GS;
		VS.clear(); VS.emplace_back("./Shader/quad.vert"sv);
		FS.clear(); FS.emplace_back("./Shader/blur.frag"sv);
		GS.clear();
		static ShaderPtr basic = Shader::create(VS, FS, GS);
		return basic;
	}

	static ShaderPtr ui()
	{
		vector<string> VS, FS, GS;
		VS.emplace_back("./Shader/ui.vert"sv);
		FS.emplace_back("./Shader/ui.frag"sv);
		static ShaderPtr ui = Shader::create(VS, FS, GS);
		return ui;
	}

	static ShaderPtr uimask()
	{
		vector<string> VS, FS, GS;
		VS.emplace_back("./Shader/ui.vert"sv);
		FS.emplace_back("./Shader/uiMask.frag"sv);
		static ShaderPtr ui = Shader::create(VS, FS, GS);
		return ui;
	}

	static ShaderPtr uimix()
	{
		vector<string> VS, FS, GS;
		VS.emplace_back("./Shader/ui.vert"sv);
		FS.emplace_back("./Shader/uimix.frag"sv);
		static ShaderPtr ui = Shader::create(VS, FS, GS);
		return ui;
	}

	static ShaderPtr text()
	{
		vector<string> VS, FS, GS;
		VS.emplace_back("./Shader/text.vert"sv);
		FS.emplace_back("./Shader/text.frag"sv);
		static ShaderPtr ui = Shader::create(VS, FS, GS);
		return ui;
	}

private:
	Shader(vector<string>& filenameVS, vector<string>& filenameFS, vector<string>& filenameGS);
	
private:
	void add_shader(GLuint ShaderProgram, const char* pShaderText, GLenum shader_type);
	GLuint compile_shader(vector<string>& filenameVS, vector<string>& filenameFS, vector<string>& filenameGS);

private:
	GLuint shader_id_;
};


#include "Shader.hpp"