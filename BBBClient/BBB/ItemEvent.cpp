#include "stdafx.h"
#include "ItemEvent.h"

///////////////////////////////////////////////////////



///////////////////////////////////////////////////////

EventPtr ItemEventFactory::createEvent(const ItemInstancePtr& instance,  VehicleObj* target,  VehicleObj* user)
{
	//cout << instance->get_type()._to_string() << endl;
	switch (instance->get_type())
	{
	case ItemType::rocket:
	{
		return ::move(make_shared<ItemEvent>());
	}
	CASE ItemType::rocket3 :
	{
		return ::move(make_shared<ItemEvent>());
	}
	CASE ItemType::swap :
	{
		return ::move(make_shared<ItemEvent>());
	}
	CASE ItemType::steal :
	{
		return ::move(make_shared<ItemEvent>());
	}
	CASE ItemType::shield :
	{
		return ::move(make_shared<SheildItemEvent>(user));
	}
	CASE ItemType::repair :
	{
		return ::move(make_shared<ItemEvent>());
	}
	CASE ItemType::boost :
	{
		return ::move(make_shared<BoostItemEvent>(user));
	}
	CASE ItemType::duple :
	{
		return ::move(make_shared<DupleItemEvent>(user));
	}
	break; default: cerr << "err! ItemType" << endl; break;
	}
}
