#pragma once
#include "Obj.h"


// ��ǻ� ���� �Ǿ������.. ����

class Deco : public Obj
{
private:
	virtual void save_file_impl(ofstream& file);
	virtual void load_file_impl(ifstream& file);
public:
	Deco(string name, const ModelPtr& model = Model::no_model(), const unordered_map<string, Animation*>& animations = {}, const AnimatorPtr& Animator = Animator::create(nullptr))
		: Obj{ model, animations, Animator } 
	{
		name = "decos/" + name;
		static int i = 0;
		load(name + to_string(i));
		boundings_.load(name + "_bounding" + to_string(i));
		i++;
	};
public:
	virtual void draw(const ShaderPtr& shader)const override;
	virtual void draw_gui() override;

	vector<ObjPtr> SubObjs; // boundings
};

using DecoPtr = shared_ptr<Deco>;