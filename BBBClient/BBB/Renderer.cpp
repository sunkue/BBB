#include "stdafx.h"
#include "Renderer.h"
#include "GameScene.h"

Renderer::Renderer()
{
	auto& mainScreen = Screen::mainScreen();
	glClearDepth(1.f);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT, GL_FILL);
	glDisable(GL_BLEND);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_MULTISAMPLE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0, 0, mainScreen.fixedwidth, mainScreen.fixedheight);

	MatchingScene::get();
	TitleScene::get();
}

void Renderer::draw()
{
	scene_->draw();
}

void Renderer::reshape(const int w, const int h)
{
	auto& mainScreen = Screen::mainScreen();
	const auto s_w = mainScreen.fixedwidth;
	const auto s_h = mainScreen.fixedheight;
	const float s_aspect = mainScreen.aspect();
	const float aspect = (float)w / h;

	if (aspect < s_aspect)
	{
		const float ratio_W = (float)w / s_w;
		const int new_h = static_cast<int>(s_h * ratio_W);
		mainScreen.viewport = { 0, (h - new_h) / 2, w, new_h };
	}
	else
	{
		const float ratio_h = (float)h / s_h;
		const int new_w = static_cast<int>(s_w * ratio_h);
		mainScreen.viewport = { (w - new_w) / 2, 0, new_w, h };
	}

	draw();
}

void Renderer::changeScene(Scene* target)
{
	lock_guard{ sceneLock_ };
	if(scene_) scene_->leaveScene();
	scene_ = target;
	scene_->enterScene();
}

void Renderer::changeSceneWithLoading(Scene* target)
{
	lock_guard{ sceneLock_ };
	if (scene_) scene_->leaveScene();

	if (&LoadingScene::get() != scene_)
		LoadingScene::get().prevScene = scene_;
	else
		LoadingScene::get().prevScene = LoadingScene::get().nextScene;

	LoadingScene::get().nextScene = target;
	scene_ = &LoadingScene::get();
	scene_->enterScene();
}
