#pragma once

unsigned char* loadBMPRaw(const char* imagepath, unsigned int& outWidth, unsigned int& outHeight);
GLuint CreatePngTexture(const char* filePath);
GLuint CreateBmpTexture(const char* filePath);

GLuint load_texture_file(const char* path, const string& directory, bool flip = false);
inline GLuint load_texture_file(const string& path, const string& directory, bool flip = false) { return load_texture_file(path.c_str(), directory, flip); }
GLuint load_cube_texture_file(const vector<string_view>& textures, string_view directory);

using TexturePtr = shared_ptr<struct Texture>;
struct Texture
{
	GLuint id;
	string type;
	string path;

	CREATE_SHARED(Texture);

#define COLOR(c)\
	static TexturePtr color_##c()\
	{\
		static TexturePtr texture = create(load_texture_file("color/"s.append(#c)+".png"s, "./Resource/Texture"s));\
		return texture; \
	}

#define ITEM(type)\
	static TexturePtr item_##type()\
	{\
		static TexturePtr texture = create(load_texture_file("item/"s.append(#type)+".png"s, "./Resource/Texture"s));\
		return texture; \
	}

	ITEM(rocket);
	ITEM(rocket3);
	ITEM(swap);
	ITEM(steal);
	ITEM(shield);
	ITEM(repair);
	ITEM(boost);
	ITEM(duple);

	COLOR(cobalt);
	COLOR(red);
	COLOR(green);
	COLOR(redpurple);
	COLOR(white);

private:
	explicit Texture() noexcept = default;
	explicit Texture(GLuint id)noexcept :id{ id } {};
};




