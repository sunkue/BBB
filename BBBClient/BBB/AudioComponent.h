#pragma once
#include "Sound.h"
#include <string>
#include <vector>

class Obj;

class AudioComponent
{
public:
	AudioComponent(Obj* owner, int updateOrder = 200);
	AudioComponent() = default;
	~AudioComponent();

	void Update();
	void OnUpdateWorldTransform();

	void PlaySound(const string& name, bool another = false, float volume = 1, bool loop = false);
	void StopSound(const string& name);
	void StopAll();

	Sound* GetSound(const string& name) { return SoundTable.contains(name) ? SoundTable[name] : nullptr; }
private:
	std::unordered_map<string, Sound*> SoundTable;
	Obj* owner{};
};

