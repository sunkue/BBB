#include "stdafx.h"
#include "Item.h"
#include "TrackNode.h"
#include "Vehicle.h"
#include "Game.h"

///////////////////////////////////////////////////

void ItemObj::save_file_impl(ofstream& file)
{
	Obj::save_file_impl(file);
	SAVE_FILE(file, included_node_);
	SAVE_FILE(file, probability_table_);
}

void ItemObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
	LOAD_FILE(file, included_node_);
	LOAD_FILE(file, probability_table_);
	update(duration_cast<milliseconds>(rotate_period_ * SUNKUE::GetRandomValueZeroToOne(random)));
	randomize_type();
}

///////////////////////////////////////////////////

void ItemObj::init()
{
	Track::get().init_include_obj(*this, true);
	normalize_probability_table();
	randomize_type();
}

void ItemObj::normalize_probability_table()
{
	// normalize table.
	float length = 0;

	for (auto& probability : probability_table_)
	{
		length += probability;
	}

	for (auto& probability : probability_table_)
	{
		probability /= length;
	}
}

void ItemObj::randomize_type()
{
	auto randomValue = SUNKUE::GetRandomValueZeroToOne(random);
	for (int i = 0; i < probability_table_.size(); i++)
	{
		if (randomValue <= probability_table_[i])
		{
			type_ = ItemType::_from_integral(i);
			break;
		}
		randomValue -= probability_table_[i];
	}
	set_model(ItemTypeHelper::toItemObj(type_));
}

ItemInstancePtr ItemObj::to_instance()
{
	return ItemInstance::create(type_);
}

void ItemObj::update(milliseconds time_elapsed)
{
	constexpr auto float_speed = rotate_max_ / rotate_period_.count();

	float diff = float_speed * time_elapsed.count();

	Obj::rotate(glm::rotate(diff, Y_DEFAULT));

	// re able.
	if (!get_enable())
	{
		disabled_time_ += time_elapsed;
		if (regen_period_ < disabled_time_)
		{
			set_enable(true);
			disabled_time_ = 0ms;
			randomize_type();
		}
	}
}

void ItemObj::on_collide(Obj& other, OUT void* context)
{
	if (!get_enable()) return;
	// vehicle
	if (auto vehicle = dynamic_cast<VehicleObj*>(&other))
	{
		bool acquired = vehicle->TryAcquireItem(*this);
		set_enable(!acquired);
	}
}

void ItemObj::draw(const ShaderPtr& shader) const
{
	if (get_enable())
	{
		Obj::draw(shader);
	}
	else
	{
		if (Game::get().isEditMode())
		{
			Obj::draw(shader, Model::box_gray());
		}
		else
		{
			Obj::draw(shader, Model::no_model());
		}
	}
}

void ItemObj::draw_gui()
{
	Obj::draw_gui();
	gui::Begin("itemobj");
	gui::InputInt("included node", &included_node_);
	bool enable = get_enable();
	gui::Checkbox("enable", &enable);
	set_enable(enable);
	GUISAVE();
	GUILOAD();
	gui::BeginChild("Probability Table");
	for (int i = 0; i < probability_table_.size(); i++)
	{
		gui::InputFloat(ItemType::_from_integral(i)._to_string(), &probability_table_[i]);
	}
	if (gui::Button("normalizeTable"))
	{
		normalize_probability_table();
	}
	gui::EndChild();

	if (gui::Button("init"))
	{
		init();
	}
	gui::End();
}

///////////////////////////////////////////////////



ItemObjPtr ItemInstance::to_obj(glm::vec3 pos)
{
	auto ret = ItemObj::create();
	ret->move(pos);
	return ret;
}

///////////////////////////////////////////////////

bool ItemInstanceList::TryPush(const ItemInstancePtr& item)
{
	if (doubled)
	{
		doubled = false;
		itemQ.try_push(ItemInstance::create(item->get_type()));
	}
	return itemQ.try_push(item);
}

optional<ItemInstancePtr> ItemInstanceList::Pop()
{
	if (itemQ.empty())
		return nullopt;

	auto ret = itemQ.front(); itemQ.pop();
	return ret;
}

void DrawableItemInstanceList::draw(const Obj& ownner, const ShaderPtr& shader) const
{
	glDisable(GL_CULL_FACE);
	auto ownnertail = -1 * ownner.get_head_dir();
	auto ownnerright = ownner.get_right_dir();
	auto ownnerpos = ownner.get_position();
	auto ownnerrotate = ownner.get_rotation();

	int size = ItemInstanceList::size();
	auto step = originscale / 2 + idxtransferpos * originscale / 2;
	int step_multiplier = 1 - size; // 1 / -1 / -2
	//int multiplier = 1 - size; //  / 1 / 0
	//int multiplier = 1 - size; //  /  / +2
	for (auto& item : itemQ._Get_container())
	{
		glm::vec3 pos = ownnerpos;
		glm::quat q = glm::rotation(X_DEFAULT, ownnertail);
		glm::vec3 scale = glm::vec3{ originscale };
		pos += ownnertail * diff.x; pos.y += diff.y;
		pos += ownnerright * step * step_multiplier;

		glm::mat4 m = glm::translate(pos) * glm::toMat4(q) * glm::scale(scale);
		shader->use();
		shader->set("u_m_mat", m);
		shader->set("u_material.albedo1", ItemTypeHelper::toTexture(item->get_type()));
		model->draw(shader);
		// emessive ����
		step_multiplier += 2;
	}
	glEnable(GL_CULL_FACE);
}

void DrawableItemInstanceList::draw_gui()
{
	gui::Begin("itemList");
	gui::DragFloat("scale", &originscale);
	gui::DragFloat("idxtransferpos", &idxtransferpos);
	gui::DragFloat2("diff", glm::value_ptr(diff));
	gui::End();
}
