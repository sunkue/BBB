

#include "stdafx.h"
#include "Renderer.h"
#include "Obj.h"
#include "Camera.h"
#include "Game.h"
#include "KeyboardEvent.h"
#include "MouseEvent.h"
#include "Networker.h"
#include "UI.h"
#include "stb_image.h"



void DrawGui()
{
	auto& mainScreen = Screen::mainScreen();

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	gui::NewFrame();

	GameScene::get().get_player()->draw_gui();
	GameScene::get().get_main_camera()->draw_gui("main Cam");
	GameScene::get().get_backmirror_camera()->draw_gui("backmirror Cam");
	GameScene::get().get_minimap_camera()->draw_gui("minimap Cam");
	GameScene::get().get_track()->draw_gui();
	GameScene::get().get_DecoObjects()[0]->draw_gui();
	//GameScene::get().get_cars()[0]->get_item_list().draw_gui();

	gui::Begin("SUNKUE_ENGINE");
	int version = Game::get().version;
	gui::InputInt("version", &version, 0);
	gui::Text("MOUSE_BUTTON_CHECK");
	gui::Checkbox("L", &MouseEventManager::get().get_L_click());
	gui::Checkbox("R", &MouseEventManager::get().get_R_click());
	gui::Checkbox("Wheel", &MouseEventManager::get().get_Wheel_click());
	gui::Text("MOUSE_POS_CHECK");
	float wx = MouseEventManager::get().get_prev_x();
	float wy = MouseEventManager::get().get_prev_y();
	gui::InputFloat("wx", &wx);
	gui::InputFloat("wy", &wy);
	auto raynds = Ray::create2D(wx, wy);
	gui::InputFloat("ndsx", &raynds.x);
	gui::InputFloat("ndsy", &raynds.y);
	gui::End();
	/*
	auto ray = Ray::create(x, y);
	gui::InputFloat3("mouse_ray", &ray.dir.x);
	*/

	if (gui::GetIO().WantCaptureMouse)
	{
		//cout << "!" << endl;
	}
	else
	{
		//cout << "?" << endl;
	}

	// gui texture buffers
	const float gui_tex_buffer_ratio = 0.1f;
	auto gui_texture_size = ImVec2(mainScreen.fixedwidth * gui_tex_buffer_ratio, mainScreen.fixedheight * gui_tex_buffer_ratio);

	ShadowParam::get().draw_gui();

	if(false)
	{
		auto& particleTransitionSize = GameScene::get().get_particle_renderer()->transitionSize;
		gui::Begin("particle");
		gui::DragFloat("particleTransitionSize", &particleTransitionSize, 0.00000125f, 0, 1, "%lf");
		gui::End();
	}

	if (false)
	{
		auto& fog = GameScene::get().get_screen_renderer()->fog;
		gui::Begin("fog");
		gui::DragFloat3("fogColor", glm::value_ptr(fog.color), 0.000125f, 0, 1, "%lf");
		gui::DragFloat("fogDensity", &fog.density, 0.0125f, 0, 20, "%lf");
		gui::End();
	}

	//
	if (false && GameScene::get().get_backmirror_camera()->get_enable())
	{
		auto buffer = GameUI::get().get_backmirror().get_main()->id;
		auto& startpos = GameUI::get().get_backmirror().get_startpos();
		auto& size = GameUI::get().get_backmirror().get_size();
		auto& startposFrame = GameUI::get().get_backmirror().get_startposFrame();
		auto& sizeFrame = GameUI::get().get_backmirror().get_sizeFrame();
		gui::Begin("backmirror");
		gui::Image((void*)buffer, gui_texture_size, ImVec2(0, 1), ImVec2(1, 0));
		gui::DragFloat2("start_pos", glm::value_ptr(startpos), 0.00125f, -1, 1.2, "%lf");
		gui::DragFloat2("size", glm::value_ptr(size), 0.00125f, 0, 3, "%lf");
		gui::DragFloat2("startposFrame", glm::value_ptr(startposFrame), 0.00125f, -1, 1.2, "%lf");
		gui::DragFloat2("sizeFrame", glm::value_ptr(sizeFrame), 0.00125f, 0, 3, "%lf");
		gui::End();
	}

	if (false && GameScene::get().get_minimap_camera()->get_enable())
	{
		auto buffer = GameUI::get().get_minimap().get_main()->id;
		auto& startpos = GameUI::get().get_minimap().get_startpos();
		auto& size = GameUI::get().get_minimap().get_size();
		gui::Begin("minimap");
		gui::Image((void*)buffer, gui_texture_size, ImVec2(0, 1), ImVec2(1, 0));
		gui::DragFloat2("start_pos", glm::value_ptr(startpos), 0.0125f, -1, 1.2, "%lf");
		gui::DragFloat2("size", glm::value_ptr(size), 0.0125f, 0, 3, "%lf");
		gui::End();
	}

	if (auto TitleButtonUi = false)
	{
		auto& center = TitleUI::get().get_gamemodebutton().center_;
		auto& diff = TitleUI::get().get_gamemodebutton().diff_;
		auto& size = TitleUI::get().get_gamemodebutton().size_;
		gui::Begin("titlebutton");
		gui::DragFloat2("center", glm::value_ptr(center), 0.0125f, -0.8, 0.8, "%lf");
		gui::DragFloat2("diff", glm::value_ptr(diff), 0.0125f, -0.8, 0.8, "%lf");
		gui::DragFloat2("size", glm::value_ptr(size), 0.0125f, -0.8, 0.8, "%lf");
		gui::End();
	}

	if (auto MatchingProfileUI = false)
	{
		auto& center = MatchingUI::get().get_profile().center_;
		auto& diff = MatchingUI::get().get_profile().diff_;
		auto& size = MatchingUI::get().get_profile().size_;

		gui::Begin("profile");
		gui::DragFloat2("center", glm::value_ptr(center), 0.0125f, -1, 1, "%lf");
		gui::DragFloat2("diff", glm::value_ptr(diff), 0.0125f, -1, 1, "%lf");
		gui::DragFloat2("size", glm::value_ptr(size), 0.0125f, -1, 1, "%lf");
		gui::End();
	}

	if (auto MatchingReadyButtonUI = false)
	{
		auto& sp = MatchingUI::get().get_readybutton().sp_;
		auto& size = MatchingUI::get().get_readybutton().size_;

		gui::Begin("readybutton");
		gui::DragFloat2("sp", glm::value_ptr(sp), 0.0125f, -1, 1, "%lf");
		gui::DragFloat2("size", glm::value_ptr(size), 0.0125f, -1, 1, "%lf");
		gui::End();
	}

	if (auto textUI = false)
	{
		auto& lab_sp = GameUI::get().get_textUi().lab_startpos_;
		auto& lab_size = GameUI::get().get_textUi().lab_size_;
		auto& time_sp = GameUI::get().get_textUi().time_startpos_;
		auto& time_size = GameUI::get().get_textUi().time_size_;
		auto& rank_sp = GameUI::get().get_textUi().rank_startpos_;
		auto& rank_size = GameUI::get().get_textUi().rank_size_;
		auto& countdown_sp = GameUI::get().get_textUi().countdown_startpos_;
		auto& countdown_size = GameUI::get().get_textUi().countdown_size_;
		auto& color = GameUI::get().get_textUi().color_;

		gui::Begin("textUI");
		gui::DragFloat3("color", glm::value_ptr(color), 0.0125f, 0, 1, "%lf");
		gui::DragFloat2("lab_sp", glm::value_ptr(lab_sp), 0.000125f, -1, 1, "%lf");
		gui::DragFloat("lab_size", &lab_size, 0.000125f, -1, 1, "%lf");
		gui::DragFloat2("time_sp", glm::value_ptr(time_sp), 0.000125f, -1, 1, "%lf");
		gui::DragFloat("time_size", &time_size, 0.000125f, -1, 1, "%lf");
		gui::DragFloat2("rank_sp", glm::value_ptr(rank_sp), 0.000125f, -1, 1, "%lf");
		gui::DragFloat("rank_size", &rank_size, 0.000125f, -1, 1, "%lf");
		gui::DragFloat2("countdown_sp", glm::value_ptr(countdown_sp), 0.000125f, -1, 1, "%lf");
		gui::DragFloat("countdown_size", &countdown_size, 0.000125f, -1, 1, "%lf");

		auto& endrank_sp = GameUI::get().get_textUi().end_rank_sp_;
		auto& endrank_diff = GameUI::get().get_textUi().end_diff_;
		auto& endrank_size = GameUI::get().get_textUi().end_rank_size_;
		gui::DragFloat2("endrank_sp", glm::value_ptr(endrank_sp), 0.000125f, -1, 1, "%lf");
		gui::DragFloat2("endrank_diff", glm::value_ptr(endrank_diff), 0.000125f, -1, 1, "%lf");
		gui::DragFloat("endrank_size", &endrank_size, 0.000125f, -1, 1, "%lf");

		gui::End();
	}

	if (auto LoadingScene = false)
	{
		auto& color = LoadingScene::get().color_;
		auto& size = LoadingScene::get().size_;
		gui::Begin("LoadingScene");
		gui::DragFloat3("color", glm::value_ptr(color), 0.000125f, -10, 10, "%lf");
		gui::DragFloat("size", &size, 0.000125f, 0, 1, "%lf");

		gui::End();
	}
	// gui::ShowMetricsWindow();

	gui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(gui::GetDrawData());
}

void DoNextFrame()
{
	KeyBoardEventManager::get().ProcessInput();
	MouseEventManager::get().ProcessInput();

	{
		lock_guard lock{ Game::get().gamelock };
		Game::get().update();
		Renderer::get().draw();
	}
	
	if (Game::get().isEditMode())
	{
		DrawGui();
	}

	glfwPollEvents();
	auto fps = 1000 / (TimerSystem::get().tick_time().count() + 1);
	string title = "BBB"s;
	if (Game::get().isEditMode())
		title += "("s + to_string(fps) + " fps)"s;
	glfwSetWindowTitle(Game::get().window, title.c_str());

	glfwSwapBuffers(Game::get().window);
}



void BindDefaultInputFuncs()
{
	//KEY_BOARD_EVENT_MANAGER::get().BindKeyFunc(GLFW_KEY_LEFT_SHIFT,
	//	[](auto&) { Game::get().renderer.get_main_camera()->camera_shake(0.2f); });
}


int main(int argc, char*argv[])
{
	GLFWwindow* window;

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	auto& mainScreen = Screen::mainScreen();
	window = glfwCreateWindow(mainScreen.fixedwidth, mainScreen.fixedheight, "SUNKUE", NULL, NULL);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}



	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow* window, int w, int h)
		{ Renderer::get().reshape(w, h); DoNextFrame(); });
	glfwSetScrollCallback(window,
		[](GLFWwindow* window, double xoffset, double yoffset)
		{ MouseEventManager::get().Scroll(window, xoffset, yoffset);  });
	glfwSetMouseButtonCallback(window,
		[](GLFWwindow* window, int key, int action, int modifiers)
		{ MouseEventManager::get().MouseButton(window, key, action, modifiers);  });
	glfwSetCursorPosCallback(window,
		[](GLFWwindow* window, double xpos, double ypos)
		{ MouseEventManager::get().CursorPosition(window, xpos, ypos);  });
	glfwSetKeyCallback(window,
		[](GLFWwindow* window, int key, int code, int action, int modifiers)
		{ KeyBoardEventManager::get().KeyBoard(window, key, code, action, modifiers);  });

	////
	Game::get().window = window;

	{
		IMGUI_CHECKVERSION();
		gui::CreateContext();

		ImGuiIO& io = gui::GetIO();

		gui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init("#version 450");
	}
	
	Renderer::get().changeScene(&TitleScene::get());

	GameScene::get(); //preloading
	LoadingScene::get();

	Renderer::get().reshape(mainScreen.fixedwidth, mainScreen.fixedheight);

	

	BindDefaultInputFuncs();

	// MAIN LOOP
	while (!glfwWindowShouldClose(window))
	{
		DoNextFrame();
	}

	if (Game::get().isEditMode())
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		gui::DestroyContext();
	}

	glfwTerminate();
}

