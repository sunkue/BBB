#include "stdafx.h"
#include "Obj.h"
#include "Renderer.h"
#include "timer.h"


void Obj::load_file_impl(ifstream& file)
{
	LOAD_FILE(file, translate_);
	LOAD_FILE(file, quaternion_);
	LOAD_FILE(file, scale_);
	LOAD_FILE(file, objid_);

	trans_boundings();
}

void Obj::save_file_impl(ofstream& file)
{
	SAVE_FILE(file, translate_);
	SAVE_FILE(file, quaternion_);
	SAVE_FILE(file, scale_);
	SAVE_FILE(file, objid_);
}

void Obj::trans_boundings()
{
	boundings_.move(translate_);
	boundings_.rotate(quaternion_);
	boundings_.scaling(scale_);
}

void Obj::update_uniform_vars(const ShaderPtr& shader)const
{
	if (!enable_)return;
	shader->use();
	glm::mat4 m = model_mat();
	shader->set("u_m_mat", m);

	if (animator_)
	{
		shader->set("u_animate", animator_->IsAnimated());
		auto transforms = animator_->GetFinalBoneMatrices();
		for (int i = 0; i < transforms.size(); ++i)
		{
			shader->set("u_FinalBoneMatrices[" + std::to_string(i) + "]", transforms[i]);
		}
	}

	glUseProgram(0);
}

void Obj::update(milliseconds time_elapsed)
{
	if (animator_)
		animator_->UpdateAnimation(time_elapsed.count() / 1000.f);

	if (Audio_)
		Audio_->Update();
}

void Obj::draw(const ShaderPtr& shader) const
{
	if (!enable_)return;
	model_->draw(shader);
	boundings_.draw(shader);
}

void Obj::draw(const ShaderPtr& shader, const ModelPtr& model) const
{
	if (!enable_)return;
	model->draw(shader);
	boundings_.draw(shader);
}

void Obj::draw_edge(const ShaderPtr& shader, const ModelPtr& model, float scalef)
{
	glCullFace(GL_FRONT);
	auto obj = const_cast<Obj*>(this);
	float scale{ scalef };
	obj->scaling(glm::vec3{ scale });
	update_uniform_vars(shader);

	Obj::draw(shader, model);
	obj->scaling(glm::vec3{ 1 / scale });
	glCullFace(GL_BACK);
	// ����ȿ��..
	/*
	glDisable(GL_DEPTH_TEST);
	glCullFace(GL_FRONT);
	obj = const_cast<Obj*>(this);
	float scale{ scalef };
	obj->scaling(glm::vec3{ scale });
	update_uniform_vars(shader);
	Obj::draw(shader, model);
	obj->scaling(glm::vec3{ 1 / scale });
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	*/
}

void Obj::draw_gui()
{
	gui::Begin("Obj");
	GUISAVE(); GUILOAD();

	static bool move_collision = true;

	gui::Checkbox("move_collision", &move_collision);

	auto translate = translate_;
	if (gui::DragFloat3("translate", glm::value_ptr(translate), 0.0625))
	{
		auto diff = translate - translate_;
		translate_ = translate;

		if (move_collision)
		{
			boundings_.move(diff);
			boundings_.trans_gui();
		}
	}

	auto prev_quat = quaternion_;
	auto current_quat = quaternion_;
	if (gui::DragFloat4("quaternion", glm::value_ptr(current_quat), 0.0625))
	{
		auto iprev_quat = glm::inverse(prev_quat);
		current_quat = glm::normalize(current_quat);
		quaternion_ *= iprev_quat;
		quaternion_ *= current_quat;
		prev_quat = current_quat;

		if (move_collision)
		{
			boundings_.rotate(iprev_quat);
			boundings_.rotate(current_quat);
			boundings_.trans_gui();
		}
	}

	auto scale = scale_;
	if (gui::DragFloat3("scale", glm::value_ptr(scale), 0.01, 0.001, 50))
	{
		auto origin = scale_;
		scale_ = scale;

		if (move_collision)
		{
			boundings_.scaling(scale / origin);
			boundings_.trans_gui();
		}
	}

	gui::End();

	boundings_.draw_gui();
};

bool Obj::collision_detect(const Obj& other) const
{
	if (!enable_)return false;
	if (!other.get_enable())return false;
	if (&other == this)return false;
	bool isCollide = get_boundings().intersects(other.get_boundings());
	return isCollide;
}


/// ////////////////////////////////////////////

void InstancingObj::update_uniform_vars(const ShaderPtr& shader) const
{
	shader->use();
	auto gametime = static_cast<float>(TimerSystem::get().game_time().count()) / 1000.f;
	shader->set("u_time", gametime);
	shader->set("u_tex_sampler", get_textures());
	glUseProgram(0);
}
