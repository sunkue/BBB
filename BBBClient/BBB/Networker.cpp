#include "stdafx.h"
#include "Game.h"
#include "Networker.h"

/// ///////////////////////

Networker::Networker()
{
	WSADATA WSAData;
	int res = WSAStartup(MAKEWORD(2, 3), &WSAData);
	if (SOCKET_ERROR == res)
	{
		SocketUtil::ReportError("InitWsa FAIL.");
		exit(-1);
	}
}

Networker::~Networker()
{
	::closesocket(socket_);
	WSACleanup();
}

/// ///////////////////////

void Networker::connect(const char* server_ip)
{
	WSACleanup();
	WSADATA WSAData;
	int res = WSAStartup(MAKEWORD(2, 3), &WSAData);

	socket_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int option = TRUE;
	setsockopt(socket_, IPPROTO_TCP, TCP_NODELAY, (const char*)&option, sizeof(option));

	SOCKADDR_IN server_addr; ZeroMemory(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = ::htons(SERVER_PORT);
	auto ret = ::inet_pton(AF_INET, server_ip, &server_addr.sin_addr);
	char addr[30];
	::inet_ntop(AF_INET, &server_addr.sin_addr, addr, sizeof(addr));
	cout << addr << endl;
	if (1 != ret)
	{
		SocketUtil::ReportError("inet_pton fail.");
		SocketUtil::DisplayError(WSAGetLastError());
		exit(-1);
	}

	ret = ::connect(socket_, reinterpret_cast<sockaddr*>(&server_addr), sizeof(server_addr));

	if (SOCKET_ERROR == ret)
	{
		SocketUtil::ReportError("connect fail, maybe 서버 안 연 듯.");
		SocketUtil::DisplayError(WSAGetLastError());
		exit(-1);
	}
}

/// ///////////////////////
extern bool g_alive;

void Networker::do_recv()
{
	cs_connect connect;
	connect.version = Game::get().version;
	do_send(&connect);

	u_long nonBlockingMode = 1;
	ioctlsocket(socket_, FIONBIO, &nonBlockingMode);

	auto prerecved = 0;
	recv_buf_.fill(0);
	while (Game::get().gamemode_ != GameMode::TITLE && !glfwWindowShouldClose(Game::get().window))
	{
		auto recved_bytes = recv(socket_, recv_buf_.data() + prerecved, MAX_PACKET_SIZE - prerecved, NULL);

		if (SOCKET_ERROR == recved_bytes)
		{
			auto err = WSAGetLastError();
			if (WSAEWOULDBLOCK == err)
			{
				continue;
			}

			// SocketUtil::DisplayError(err);
		}

		if (0 == recved_bytes)
		{
			cout << "disconnected server" << endl;
			exit(-1);
		}

		auto pck_start = recv_buf_.data();
		auto remain_bytes = recved_bytes + prerecved;

		// process completed packets.
		for (auto need_bytes = *reinterpret_cast<packet_size_t*>(pck_start);
			need_bytes <= remain_bytes;)
		{
			process_packet(pck_start);
			pck_start += need_bytes;
			remain_bytes -= need_bytes;
			need_bytes = *reinterpret_cast<packet_size_t*>(pck_start);

			if (0 == remain_bytes || 0 == need_bytes)
			{
				prerecved = 0;
				break;
			}
		}

		// remain_bytes가 남아있으면 미완성패킷의 데이터임. prerecv 해주기.
		if (0 != remain_bytes)
		{
			prerecved = remain_bytes;
			memmove(recv_buf_.data(), pck_start, remain_bytes);
		}
	}
}

/// ///////////////////////

void Networker::do_send(const void* const packet)
{
	auto packet_type = reinterpret_cast<const packet_base<void>*>(packet)->packet_type;
//	cerr << "[SEND::" << +packet_type._to_string() << "]" << endl;

	//cout << "try lock" << endl;
	lock_guard lock{ send_lock };
	//cout << "locked" << endl;
	auto pck = reinterpret_cast<const packet_base<void>*const>(packet);

	auto ret = ::send(socket_, reinterpret_cast<const char*>(pck), pck->size, NULL);
	//cout << "sended" << endl;
	if (SOCKET_ERROR == ret)
	{
		// SocketUtil::ReportError("send error,,??");
		// SocketUtil::DisplayError(WSAGetLastError());
		// exit(-1);
	}
}