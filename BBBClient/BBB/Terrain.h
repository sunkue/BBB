#pragma once

#include "Mesh.h"
#include "Model.h"

/////////////////////////////////////////////////////////
//using TTT = uint8_t;
using HeightRawMap_t = uint16_t;
class HeightMapImage
{
public:
	HeightMapImage(string_view fileName, int width, int length, glm::vec3 scale);

	float GetHeight(float x, float z)const;
	glm::vec3 GetNormal(int x, int z)const;
	HeightRawMap_t* GetPixels()const { return HeightMapPixels_.get(); }

	GET(Scale);
	GET(Width);
	GET(Length);
private:
	bool OutOfRange(int w, int l)const { return (w < 0.0f) || (l < 0.0f) || (Width_ <= w) || (Length_ <= l); }
	bool OutOfRange(float w, float l)const { return (w < 0.0f) || (l < 0.0f) || (Width_ <= w) || (Length_ <= l); }

private:
	unique_ptr<HeightRawMap_t[]> HeightMapPixels_;
	int Width_;
	int Length_;
	glm::vec3 Scale_;
};

/////////////////////////////////////////////////////////

class HeighMapGridMesh : public Mesh
{
	friend class Terrain;
public:
	HeighMapGridMesh(string_view fileName, int width, int length, glm::vec3 scale = glm::vec3{ 1 });

	float GetHeight(int x, int z)const;

protected:
	unique_ptr<HeightMapImage> HeightMapImage_;
};

/////////////////////////////////////////////////////////

class Terrain
{
public:
	Terrain(string_view fileName, int width, int length, glm::vec3 scale = glm::vec3{ 1 });

public:
	float GetHeight(float x, float z)const { return HeightMapGridMesh_.GetHeight(x / Scale_.x, z / Scale_.z) * Scale_.y; }
	float GetWorldHeight(float x, float z)const;
	glm::vec3 GetNormal(float x, float z)const { return HeightMapGridMesh_.HeightMapImage_->GetNormal(int(x / Scale_.x), int(z / Scale_.z)); }

	int GetHeightMapWidth()const { return HeightMapGridMesh_.HeightMapImage_->get_Width(); }
	int GetHeightMapLength()const { return HeightMapGridMesh_.HeightMapImage_->get_Length(); }

	glm::vec3 GetScale()const { return Scale_; }
	float GetWidth()const { return Width_ * Scale_.x; }
	float GetLength()const { return Length_ * Scale_.z; }

	GET_REF(HeightMapGridMesh);
	glm::mat4 model_mat()const { return glm::translate(translate_); }

	void update_uniform_vars(const ShaderPtr& shader)const;

	void draw(const ShaderPtr& shader) const;

	void draw_gui();

private:
	HeighMapGridMesh HeightMapGridMesh_;
	int Width_;
	int Length_;
	glm::vec3 Scale_;
	glm::vec3 translate_{ -1175, -1.125,-1100 };
	TexturePtr AlbedoTex_;
	TexturePtr NormalTex_;

};