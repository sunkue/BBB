#include "stdafx.h"
#include "GameScene.h"
#include "Renderer.h"
#include "Sun.h"
#include "UI.h"
#include "Game.h"
#include "Deco.h"


void GameScene::draw()
{
	if (!drawable_) return;
	struct DRAW_PARAMS
	{
		bool grass = true;
		bool sky = true;
		bool map = true;
		bool car = true;
		bool deco = true;   /*    !!!!!!!!!!       */
		bool track = true;
		bool effect = true;
	};

	auto draw_depthmap = [&](DRAW_PARAMS param = {})
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);

		depth_renderer_->bind_directional_depthmap_fbo();
		glCullFace(GL_FRONT);

		depth_renderer_->set_directional_depthmap_shader(directional_depthmap_shader_, 0);

		if (param.map)
		{
			glDisable(GL_CULL_FACE);
			Terrain_->update_uniform_vars(directional_depthmap_shader_);
			Terrain_->draw(directional_depthmap_shader_);
			glEnable(GL_CULL_FACE);
		}

		if (param.car)
		{
			for (auto& car : cars_)
			{
				car->update_uniform_vars(directional_depthmap_shader_);
				car->draw(directional_depthmap_shader_);
			}
		}

		if (param.deco)
		{
			for (auto& deco : DecoObjects_)
			{
				deco->update_uniform_vars(directional_depthmap_shader_);
				deco->draw(directional_depthmap_shader_);
			}
		}

		if (false && param.grass)
		{
			depth_renderer_->set_directional_depthmap_shader(directional_grass_depthmap_shader_, 0);

			grasses_->update_uniform_vars(directional_grass_depthmap_shader_);
			grasses_->draw(directional_grass_depthmap_shader_);
		}

		for (auto& t : Tires_)
		{
			t->update_uniform_vars(directional_depthmap_shader_);
			t->draw(directional_depthmap_shader_);
		}

		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);
	};
	auto erase_depthmap = [&]()
	{
		depth_renderer_->bind_directional_depthmap_fbo();
	};
	auto draw_gamescene = [&](const CameraPtr& camera, GLuint TargetBuffer = 0, DRAW_PARAMS param = {})
	{
		glBindFramebuffer(GL_FRAMEBUFFER, TargetBuffer);

		ready_draw(camera);

		// 1. first render to depth map 

		glCullFace(GL_BACK);

		/// // 2. draw_gbuffers
		{
			gbuffer_renderer_->bind_gbuffer_fbo();

			if (param.map)
			{
				glDisable(GL_CULL_FACE);
				Terrain_->update_uniform_vars(normalmap_g_shader_);
				Terrain_->draw(normalmap_g_shader_);
				glEnable(GL_CULL_FACE);
			}

			player_->update_uniform_vars(default_g_shader_);
			player_->draw(default_g_shader_);

			if (param.car)
			{
				for (auto& car : cars_)
				{
					car->update_uniform_vars(default_g_shader_);
					car->draw(default_g_shader_);
				}
				default_g_shader_->use();
				default_g_shader_->set("u_rimcolor", glm::vec3{ 0 });
			}


			if (param.deco)
			{
				for (auto& deco : DecoObjects_)
				{
					deco->update_uniform_vars(default_g_shader_);
					deco->draw(default_g_shader_);
				}
			}

			if (param.grass)
			{
				glDisable(GL_CULL_FACE);
				grasses_->update_uniform_vars(grass_g_shader_);
				grasses_->draw(grass_g_shader_);
				glEnable(GL_CULL_FACE);
			}

			if (param.track)
			{
				track_->get().draw(default_g_shader_);
			}

			for (auto& t : Tires_)
			{
				t->update_uniform_vars(default_g_shader_);
				t->draw(default_g_shader_);
			}
		}

		/// // sky_god_ray_____

		sun_renderer_->bind_sky_fbo();

		if (param.sky)
		{
			Sky::get().update_uniform_vars(skypass_shader_);

			skypass_shader_->use();
			skypass_shader_->set("obj_texture", gbuffer_renderer_->normal_tbo); // normal_tbo

			sun_renderer_->draw_quad();
		}

		sun_renderer_->bind_godray_fbo();

		if (param.sky)
		{
			sun_renderer_->draw_godray(camera);
		}

		/// // 3. do_lights_pass

		gbuffer_renderer_->bind_lightpass_fbo();
		auto& light_shader = gbuffer_renderer_->lightpass_shader;

		if (0 == TargetBuffer)
		{
			testing_spot_light_->direction = camera->get_look_dir();
			testing_spot_light_->position = camera->get_position();
		}
		light_shader->set("u_point_light", testing_point_light_);
		light_shader->set("u_directinal_light", testing_directional_light_);
		light_shader->set("u_spot_light", testing_spot_light_);
		light_shader->set("u_shadowmap", depth_renderer_->directional_depthmap_tbo);

		gbuffer_renderer_->draw_quad();

		// 5. render screen pass
		screen_renderer_->blit_fbo(gbuffer_renderer_->lightpass_fbo, screen_renderer_->color_fbo);
		screen_renderer_->predraw_screen(sun_renderer_->skypass_tbo, sun_renderer_->godraypass_tbo);

		// 6. render edges.. on light pass.
		if (camera != minimap_camera_)
		{
			screen_renderer_->blit_depthstencil_fbo(gbuffer_renderer_->gbuffer_fbo, screen_renderer_->predraw_fbo);

			glBindFramebuffer(GL_FRAMEBUFFER, screen_renderer_->predraw_fbo);

			// draw edges (emessive?)
			for (auto& item : track_->get_items())
			{
				item->draw_edge(Shader::basic(), Model::box_orange());
			}

			glBindFramebuffer(GL_FRAMEBUFFER, screen_renderer_->predraw_fbo);

			// draw effects.. stencil...
			for (auto& p : particles_)
			{
				p.draw(*particle_renderer_);
			}

			for (auto& car : cars_)
			{
				if (car->get_shield())
				{
					glEnable(GL_STENCIL_TEST);
					glEnable(GL_DEPTH_TEST);
					glDepthMask(TRUE);
					glClear(GL_STENCIL_BUFFER_BIT);
					glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

					glStencilFunc(GL_ALWAYS, 1, 0xFF);
					float scale{ 1.125 };
					car->scaling(glm::vec3{ scale });
					car->update_uniform_vars(Shader::basic());
					car->draw(Shader::basic());
					car->scaling(glm::vec3{ 1 / scale });

					glStencilFunc(GL_EQUAL, 1, 0xFF);
					glDepthMask(FALSE);
					Shader::blur()->use();
					Shader::blur()->set("u_screen_texture", screen_renderer_->color_tbo);
					Shader::blur()->set("u_additionalcolor", glm::vec3{ 0,0,0.2 });
					ScreenQuad::get().draw_quad();
					glDepthMask(TRUE);
					glDisable(GL_STENCIL_TEST);
				}

				if (car->get_duple())
				{
					glClear(GL_STENCIL_BUFFER_BIT);
					glEnable(GL_STENCIL_TEST);
					glEnable(GL_DEPTH_TEST);
					glDepthMask(TRUE);
					glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

					glStencilFunc(GL_ALWAYS, 1, 0xFF);
					float scale{ 1.0625 };
					car->scaling(glm::vec3{ scale });
					car->update_uniform_vars(Shader::basic());
					car->draw(Shader::basic());
					car->scaling(glm::vec3{ 1 / scale });

					glStencilFunc(GL_EQUAL, 1, 0xFF);
					glDepthMask(FALSE);
					Shader::blur()->use();
					Shader::blur()->set("u_screen_texture", screen_renderer_->color_tbo);
					Shader::blur()->set("u_additionalcolor", glm::vec3{ 0,0.3,0.3 });
					ScreenQuad::get().draw_quad();
					glDepthMask(TRUE);
					glDisable(GL_STENCIL_TEST);
				}
			}
		}

		//mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w;

		screen_renderer_->draw_screen(TargetBuffer, camera->mode);
	};

	////////////////////////////////////////////////////////////////////////////

	erase_depthmap();

	// minimap camera 
	if (minimap_camera_->get_enable())
	{
		DRAW_PARAMS param;
		param.grass = false;
		param.sky = false;
		param.map = false;
		param.effect = false;
		param.deco = false;
		auto minimap = GameUI::get().get_minimap().get_fbo();

		constexpr float vehicle_scale = 30;
		constexpr float player_scale = 1;
		for (auto car : cars_)
		{
			car->scaling(glm::vec3{ vehicle_scale });
		}
		player_->scaling(glm::vec3{ player_scale });

		draw_gamescene(minimap_camera_, minimap, param);

		for (auto car : cars_)
		{
			car->scaling(glm::vec3{ 1 / vehicle_scale });
		}
		player_->scaling(glm::vec3{ 1 / player_scale });
	}

	////////////////////////////////////////////////////////////////////////////

	draw_depthmap();

	// backmirror camera
	if (backmirror_camera_->get_enable())
	{
		auto backmirrorfbo = GameUI::get().get_backmirror().get_fbo();
		draw_gamescene(backmirror_camera_, backmirrorfbo);
	}

	// main camera
	if (main_camera_->get_enable())
	{
		draw_gamescene(main_camera_);
	}

	////////////////////////////////////////////////////////////////////////////

	// Render UI
	{
		GameUI::get().update();
		GameUI::get().draw();
	}
}

void GameScene::update(milliseconds elapsed)
{
	// cout << elapsed << endl;
	float elapsedf = elapsed.count() / 1000.f;

	AudioSystem::get().SetListener(main_camera_->get_position(), -main_camera_->get_look_dir());

	if (Game::get().isPlayMode())
	{
		static milliseconds info_interval_{};
		info_interval_ += elapsed;
		constexpr auto syncPeriod = 1000ms * (1.f / 32.f);
		if (syncPeriod < info_interval_)
		{
			info_interval_ = 0ms;

			auto& vehicle = Game::get().players[Game::get().playerid].get_vehicle();

			cs_player_info info;
			info.timestamp = TimerSystem::get().GetServerTime();
			info.pos = decltype(info.pos)::encode(vehicle->get_position());
			info.rotate = decltype(info.rotate)::encode(vehicle->get_rotation());
			info.linear_speed = decltype(info.linear_speed)::encode(vehicle->get_linear_speed());
			Networker::get().do_send(&info);
		}
	}

	track_->update(elapsed);

	if (Game::get().isEditMode())
	{
		update_ranks();
	}

	for (auto& c : cars_)
	{
		if (c == player_)
			continue;

		c->update(elapsed);
	}

	for (auto& d : DecoObjects_)
	{
		d->update(elapsed);
	}

	player_->update(elapsed);

	main_camera_->update(elapsedf);
	minimap_camera_->update(elapsedf);
	backmirror_camera_->update(elapsedf);

	for (auto p = particles_.begin(), e = particles_.end(); p != e; p++)
	{
		p->lifetime -= elapsed;

		if (p->lifetime < 0ms)
		{
			particles_.erase(p);
			p--;
			continue;
		}
	}
}

void GameScene::update_ranks()
{
	array<InGameID, MAX_VEHICLE> carids;
	std::iota(ALLOF(carids), 0);

	std::sort(carids.begin(), carids.end(),
		[&](InGameID a, InGameID b)
		{
			return !cars_[a]->rank_worse_than(*cars_[b]);
		});

	for (int rank = 0; rank < carids.size(); rank++)
	{
		cars_[carids[rank]]->set_rank(rank + 1);
	}
}
///////////////////////////////////////////////////////////////////

void MouseWheel(const MouseEventManager::scroll_event& scroll)
{
	auto& camera = GameScene::get().get_main_camera();
	if (scroll.yoffset > 0)
	{
		auto diff = camera->get_diff();
		camera->set_diff(diff * 0.875f);
		//- 1.31538 0.789227 0
		//- 1.4798 0.88788 0
	}
	else
	{
		auto diff = camera->get_diff();
		camera->set_diff(diff * 1.125f);
	}
}

void GameScene::enterScene()
{
	while (clk::now() <= TimerSystem::get().get_start_time_point() - 1s)
	{
		;;;
	}

	if (Game::get().isPlayMode())
	{
	}

	AudioSystem::get().SetDefault3DSoundMinDistance(10);

	TimerSystem::get().start();

	for (auto& c : cars_)
	{
		switch (rand() % 4)
		{
		case 0: {c->get_Audio()->PlaySound("idle1.mp3", false, 0.6, true); } break;
		case 1: {c->get_Audio()->PlaySound("idle2.wav", false, 0.8, true); } break;
		case 2: {c->get_Audio()->PlaySound("idle3.wav", false, 0.5, true); } break;
		case 3: {c->get_Audio()->PlaySound("desert.mp3", false, 0.7, true); } break;
		}
	}

	Game::get().players[Game::get().playerid].get_vehicle()->get_Audio()->PlaySound("inside.wav", false, 1, true);

	KeyBoardEventManager::get().BindMainKeyFunc(
		[this](const KeyBoardEventManager::key_event& key)->bool
		{ return get_player()->process_input(key); });
	KeyBoardEventManager::get().BindKeyFunc(GLFW_KEY_ESCAPE,
		[this](const auto& key) { if (key.action == GLFW_PRESS) drawable_ = !drawable_; });

	MouseEventManager::get().BindScrollFunc(
		[this](const MouseEventManager::scroll_event& scroll)->bool
		{ return get_player()->process_input(scroll); });
	MouseEventManager::get().BindButtonFunc(
		[this](const MouseEventManager::button_event& button)->bool
		{ return get_player()->process_input(button); });
	MouseEventManager::get().BindPosFunc(
		[this](const MouseEventManager::pos_event& pos)->bool
		{ return get_player()->process_input(pos); });

	if (Game::get().isEditMode())
	{
		KeyBoardEventManager::get().BindKeyFunc(GLFW_KEY_ESCAPE,
			[](const auto& key) { if (key.action != GLFW_RELEASE) GameScene::get().swap_player_ghost(); });

		MouseEventManager::get().BindScrollFunc(
			[](const auto& scroll) { MouseWheel(scroll); });
	}
}

void GameScene::leaveScene()
{
	Scene::leaveScene();
	for (auto& c : cars_)
		c->get_Audio()->StopAll();
	drawable_ = true;
}

///////////////////////////////////////////////////////////////////

void GameScene::ready_draw(const CameraPtr& camera)
{
	auto p = proj_mat(camera);
	auto v = camera->view_mat();
	auto camerapos = camera->get_position();
	auto inv_p = glm::inverse(p);
	auto inv_v = glm::inverse(v);
	auto vp = p * v;

	ubo_vp_mat.update(glm::value_ptr(vp));
	ubo_inv_p_mat.update(glm::value_ptr(inv_p));
	ubo_inv_v_mat.update(glm::value_ptr(inv_v));
	ubo_camerapos.update(glm::value_ptr(camerapos));

	glm::vec2 default_buffer_resolution = { screen_.fixedwidth, screen_.fixedheight };

	ubo_default_buffer_resolution.update(glm::value_ptr(default_buffer_resolution));

	auto resolution = ShadowParam::get().shadowcamresolution;
	float n = screen_.n; float f = screen_.f;
	glm::mat4 lightProjection = glm::ortho(-resolution, resolution, -resolution, resolution, n, f);
	glm::mat4 lightView = glm::lookAt(-Sky::get().get_sun_light()->direction * resolution, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 lightSpaceMatrix = lightProjection * lightView;
	ubo_lightspace_mat.update(glm::value_ptr(lightSpaceMatrix));
}



////////////////////////////


void GameScene::swap_player_ghost()
{
	player_.swap(ghost_);
	main_camera_->set_ownner(player_.get());
	if (auto payervehicle = dynamic_cast<VehicleObj*>(player_.get()))
	{
		backmirror_camera_->set_enable(true);
		backmirror_camera_->set_ownner(payervehicle->get_backmirror().get());
	}
	else
	{
		backmirror_camera_->set_enable(false);
	}
}

void GameScene::init()
{
	init_shader();
	init_resources();
	init_player();
}

void GameScene::init_shader()
{
	vector<string> VS;
	vector<string> FS;
	vector<string> GS;

	VS.clear(); VS.emplace_back("./Shader/H_grass_vert.glsl"sv); VS.emplace_back("./Shader/grass_vert.glsl"sv);
	FS.clear(); FS.emplace_back("./Shader/png.frag"sv);
	GS.clear();
	grass_g_shader_ = Shader::create(VS, FS, GS);

	VS.clear(); VS.emplace_back("./Shader/directional_depthmap.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/directional_depthmap.frag"sv);
	GS.clear();
	directional_depthmap_shader_ = Shader::create(VS, FS, GS);

	VS.clear(); VS.emplace_back("./Shader/H_grass_vert.glsl"sv); VS.emplace_back("./Shader/directional_grass_depthmap_vert.glsl"sv);
	FS.clear(); FS.emplace_back("./Shader/directional_grass_depthmap.frag"sv);
	GS.clear();
	directional_grass_depthmap_shader_ = Shader::create(VS, FS, GS);

	VS.clear(); VS.emplace_back("./Shader/point_depthmap.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/point_depthmap.frag"sv);
	GS.clear(); GS.emplace_back("./Shader/point_depthmap.geom"sv);
	point_depthmap_shader_ = Shader::create(VS, FS, GS);

	VS.clear(); VS.emplace_back("./Shader/gbuffer.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/gbuffer.frag"sv);
	GS.clear(); //GS.emplace_back("./Shader/gbuffer_geomatry.glsl"sv);
	default_g_shader_ = Shader::create(VS, FS, GS);

	normalmap_g_shader_ = Shader::gbufferNormalmap();

	VS.clear(); VS.emplace_back("./Shader/quad.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/sun_w_fragment.glsl"sv);
	GS.clear(); //GS.emplace_back("./Shader/gbuffer_geomatry.glsl"sv);
	sun_w_shader_ = Shader::create(VS, FS, GS);

	//	sun_b_shader_ = Shader::create(VS, FS, GS);//

	VS.clear(); VS.emplace_back("./Shader/quad.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/sky.frag"sv);
	GS.clear(); //GS.emplace_back("./Shader/gbuffer_geomatry.glsl"sv);
	skypass_shader_ = Shader::create(VS, FS, GS);

	screen_renderer_ = make_unique<ScreenRenderer>();
	screen_renderer_->init();
	depth_renderer_ = make_unique<DepthRenderer>();
	depth_renderer_->init();
	gbuffer_renderer_ = make_unique<gBufferRenderer>();
	gbuffer_renderer_->init();
	sun_renderer_ = make_unique<SunRenderer>();
	sun_renderer_->init();
	particle_renderer_ = make_unique<ParticleRenderer>();
}

void GameScene::init_resources()
{
	track_ = &Track::get();
	load_model();

	testing_directional_light_ = DirectionalLight::create();
	testing_point_light_ = PointLight::create();
	testing_spot_light_ = SpotLight::create();

	ubo_vp_mat.bind(grass_g_shader_, "VP_MAT");
	ubo_vp_mat.bind(default_g_shader_, "VP_MAT");
	ubo_vp_mat.bind(Shader::basic(), "VP_MAT");
	ubo_vp_mat.bind(Track::get().get_Spline_G_Shader(), "VP_MAT");

	ubo_inv_v_mat.bind(skypass_shader_, "INV_V_MAT");
	ubo_inv_p_mat.bind(skypass_shader_, "INV_P_MAT");

	ubo_camerapos.bind(default_g_shader_, "CAMERA_POS");
	ubo_camerapos.bind(gbuffer_renderer_->lightpass_shader, "CAMERA_POS");

	ubo_default_buffer_resolution.bind(skypass_shader_, "RESOLUTION");

	ubo_lightspace_mat.bind(gbuffer_renderer_->lightpass_shader, "LIGHTSPACE_MAT");

}

void GameScene::load_model()
{
	auto bluecar = Model::create("./Resource/Model/made/Car/BlueCar/Car.fbx");
	auto redcar = Model::create("./Resource/Model/made/Car/RedCar/Car.fbx");
	auto greencar = Model::create("./Resource/Model/made/Car/GreenCar/Car.fbx");
	auto pinkcar = Model::create("./Resource/Model/made/Car/PinkCar/Car.fbx");
	auto whitecar = Model::create("./Resource/Model/made/Car/WhiteCar/Car.fbx");
	//	auto bluecar = Model::create("./Resource/Model/forza/forza_blue/low_forza.obj");
//	auto redcar = Model::create("./Resource/Model/forza/forza_red/low_forza.obj");
//	auto greencar = Model::create("./Resource/Model/forza/forza_green/low_forza.obj");
//	auto pinkcar = Model::create("./Resource/Model/forza/forza_pink/low_forza.obj");
	//auto bludycar = Model::create("./Resource/Model/forza/forza_bludy/low_forza.obj");
	//auto orangecar = Model::create("./Resource/Model/forza/forza_orange/low_forza.obj");
	//auto whitecar = Model::create("./Resource/Model/forza/forza_white/low_forza.obj");
	//auto purplecar = Model::create("./Resource/Model/forza/forza_purple/low_forza.obj");
	//auto map = Model::create("./Resource/Model/default_map/default_map.obj");
	auto tire = Model::create("./Resource/Model/tire/Tire_Hankook.obj");

	Model::box_orange();
	Model::item_rocket();
	Model::item_rocket3();
	Model::item_swap();
	Model::item_steal();
	Model::item_shield();
	Model::item_repair();
	Model::item_boost();
	Model::item_duple();
	Texture::item_rocket();
	Texture::item_rocket3();
	Texture::item_swap();
	Texture::item_steal();
	Texture::item_shield();
	Texture::item_repair();
	Texture::item_boost();
	Texture::item_duple();

	Terrain_ = new Terrain("Resource/Terrain/terrain.raw"
		, 513, 513
		, glm::vec3{ 4,0.025,4 });

	for (int i = 0; auto & t : Tires_)
	{
		t = make_shared<Obj>(tire);
		t->load("tire" + to_string(i));
		t->boundings_.load("tire_bounding");
		i++;
	}


	//auto bench = Model::create("./Resource/Model/made/Bench/Bench.fbx");
	//auto streetLight = Model::create("./Resource/Model/made/Street_Light_001/Street_Light_001.fbx");
	//auto thing = Model::create("./Resource/Model/made/Thing/Thing.fbx");
	//auto trashCan = Model::create("./Resource/Model/made/Trash_Can/Trash_Can.fbx");
//	auto goalLine = Model::create("./Resource/Model/made/Goal_Line/Goal_Line.fbx");
//	unordered_map<string, Animation*> goalLineAnimations;
//	goalLineAnimations["IDLE"] = new Animation{ "./Resource/Model/made/Goal_Line/Goal_Line_Idle.fbx", goalLine.get() };

//	auto& d = DecoObjects_.emplace_back(make_shared<Deco>("deco", goalLine));
//	d->get_animations() = goalLineAnimations;
//	d->get_animator()->PlayAnimation(goalLineAnimations["IDLE"]);

	auto map = Model::create("./Resource/Model/made/TEST_M/TEST_M.fbx");
	unordered_map<string, Animation*> mapAnimations;
	mapAnimations["IDLE"] = new Animation{ "./Resource/Model/made/TEST_M/TEST_M_A.fbx", map.get() };

	auto& d = DecoObjects_.emplace_back(make_shared<Deco>("deco", map));
	d->get_animations() = mapAnimations;
	d->get_animator()->PlayAnimation(mapAnimations["IDLE"]);

	//unordered_map<string, Animation*> ThingAnimations;
	//ThingAnimations["IDLE"] = new Animation{ "./Resource/Model/made/Thing/Thing_Idle.fbx", thing.get() };

	unordered_map<string, Animation*> CarAnimations;
	CarAnimations["IDLE"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Idle.fbx", bluecar.get() };
	CarAnimations["D"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Drive.fbx", bluecar.get() };
	CarAnimations["D_L"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Drive-switch-Left.fbx", bluecar.get() };
	CarAnimations["D_R"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Drive-switch-Right.fbx", bluecar.get() };
	CarAnimations["IDLE_L"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Drive-ing-Left.fbx", bluecar.get() };
	CarAnimations["IDLE_R"] = new Animation{ "./Resource/Model/made/Car/Animation/Car_A_Drive-ing-Right.fbx", bluecar.get() };

	ModelPtr carmodels[]
	{
		bluecar,redcar,greencar,pinkcar
		,whitecar,whitecar,whitecar,whitecar
		,whitecar,whitecar,whitecar,whitecar
		,whitecar,whitecar,whitecar,whitecar
		,whitecar,whitecar,whitecar,whitecar
		,whitecar,whitecar,whitecar,whitecar
	};

	glm::vec3 carcolors[]
	{
		{23,24,143},{273,27,36},{0,165,0},{174,9,89}
		,{125,0,0},{255,127,39},{220,220,220},{163,73,164}
	};

	for (InGameID id = 0; id < MAX_PLAYER; id++)
	{
		cars_[id] = make_shared<VehicleObj>(id, carmodels[id]);
		cars_[id]->color_ = carcolors[id] / 255.f;
		cars_[id]->get_animations() = CarAnimations;
		cars_[id]->get_animator()->PlayAnimation(CarAnimations["IDLE"]);
	}

	for (InGameID _id = 0; _id < MAX_BOT; _id++)
	{
		auto id = _id + MAX_PLAYER;
		cars_[id] = make_shared<AutoVehicleObj>(id, carmodels[id]);
		cars_[id]->color_ = glm::vec3{ 120,120,120 } / 255.f;
		cars_[id]->get_animations() = CarAnimations;
		cars_[id]->get_animator()->PlayAnimation(CarAnimations["IDLE"]);
	}

	for (auto& car : cars_)
	{
		track_->init_include_obj(*car.get(), true);
	}

	// camera
	main_camera_ = make_shared<Camera>();
	main_camera_->set_ownner(player_.get());
	main_camera_->load("main_camera");
	main_camera_->update(0.001f);

	backmirror_camera_ = make_shared<Camera>();
	if (auto payervehicle = dynamic_cast<VehicleObj*>(player_.get()))
	{
		backmirror_camera_->set_enable(true);
		backmirror_camera_->set_ownner(payervehicle->get_backmirror().get());
	}
	else
	{
		backmirror_camera_->set_enable(false);
	}
	backmirror_camera_->load("backmirror_camera");


	minimap_camera_ = make_shared<Camera>();
	minimap_camera_->load("minimap_camera");
	minimap_camera_->set_ownner(player_.get());

	/// /
	grasses_ = make_shared<InstancingObj>();

	vector<Vertex> grassvertices;
	for (auto& v : cross_billboard_3)
	{
		grassvertices.push_back(v);
	}

	const auto grass_count = 68000;
	const auto grass_range = 700;

	grasses_->set_num_inst(grass_count);
	grasses_->setup_mesh(grassvertices);

	vector<float> scales; scales.reserve(grass_count);
	vector<float> yaw; yaw.reserve(grass_count);
	vector<float> shearseed; shearseed.reserve(grass_count);
	vector<glm::vec3> translate; translate.reserve(grass_count);

	for (int i = 0; i < grass_count; i++)
	{
		glm::vec3 pos = { rand() % (2 * grass_range) - grass_range, 0.f, rand() % (2 * grass_range) - grass_range };
		const glm::vec3 default_pos = { -HALF_ROOT3, -1.f, -1.f };
		auto initPos = pos + default_pos;
		translate.push_back(initPos);
		shearseed.push_back(translate[i].x / 10);
		scales.push_back(0.25f + i % 3 * 0.05f);
		yaw.push_back(glm::radians(static_cast<float>(rand() % 360)));
	}

	auto texture = Texture::create(); texture->id = CreatePngTexture("./Resource/Texture/grass/grass0.png"); grasses_->add_texture(texture);
	texture = Texture::create(); texture->id = CreatePngTexture("./Resource/Texture/grass/grass1.png");		grasses_->add_texture(texture);
	texture = Texture::create(); texture->id = CreatePngTexture("./Resource/Texture/grass/blueflower.png"); grasses_->add_texture(texture);
	texture = Texture::create(); texture->id = CreatePngTexture("./Resource/Texture/grass/redflower.png");	grasses_->add_texture(texture);

	grasses_->add_instancing_attribute("a_scale", 10);
	grasses_->add_instancing_attribute("a_yaw", 11);
	grasses_->add_instancing_attribute("a_shearseed", 12);
	grasses_->add_instancing_attribute("a_translate", 13);

	grasses_->setup_instance_attribute("a_scale", scales.data());
	grasses_->setup_instance_attribute("a_yaw", yaw.data());
	grasses_->setup_instance_attribute("a_translate", translate.data());
	grasses_->setup_instance_attribute("a_shearseed", shearseed.data());

	/// ///////

	ghost_ = make_unique<GhostObj>(Model::no_model());
}

void GameScene::init_player()
{
	for (int i = 0; i < MAX_VEHICLE; i++)
	{
		cars_[i]->set_enable(false);
	}

	if (Game::get().isEditMode())
	{
		Game::get().playerid = 0;

		for (int i = 0; i < Game::get().players.size(); i++)
		{
			Game::get().players[i].Connect(GameScene::get().get_cars()[i]);
		}
		cout << "EDITMODE" << endl;
	}
	else
	{
		auto playetid = Game::get().playerid;
		player_ = cars_[playetid];
		Game::get().players[playetid].Connect(cars_[playetid]);
	}

	if (Game::get().isPlayMode())
	{
		for (int i = MAX_PLAYER; i < Game::get().players.size(); i++)
		{
			Game::get().players[i].Connect(cars_[i]);
		}
		cout << "PLAYMODE" << endl;
	}

	main_camera_->set_ownner(player_.get());
	main_camera_->update(1);
	minimap_camera_->set_ownner(player_.get());
	minimap_camera_->update(1);

	if (auto payervehicle = dynamic_cast<VehicleObj*>(player_.get()))
	{
		backmirror_camera_->set_enable(true);
		backmirror_camera_->set_ownner(payervehicle->get_backmirror().get());
	}
	else
	{
		backmirror_camera_->set_enable(false);
	}
	backmirror_camera_->update(1);
}
/////////////////////////////////////////////////////////////////////

void ScreenRenderer::init()
{
	auto& mainScreen = Screen::mainScreen();
	glGenFramebuffers(1, &color_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, color_fbo);

	color_tbo = Texture::create();
	glGenTextures(1, &color_tbo->id);
	glBindTexture(GL_TEXTURE_2D, color_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_tbo->id, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cerr << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/////////////////////////////////////////

	glGenFramebuffers(1, &predraw_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, predraw_fbo);

	predraw_tbo = Texture::create();
	glGenTextures(1, &predraw_tbo->id);
	glBindTexture(GL_TEXTURE_2D, predraw_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, predraw_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	predrawrboDepth;
	glGenRenderbuffers(1, &predrawrboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, predrawrboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, mainScreen.fixedwidth, mainScreen.fixedheight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, predrawrboDepth);



	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cerr << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	vector<string> VS; VS.emplace_back("./Shader/quad.vert"sv);
	vector<string> FS; FS.emplace_back("./Shader/predraw_screen.frag"sv);
	vector<string> GS;
	predraw_shader = Shader::create(VS, FS, GS);

	VS.clear(); VS.emplace_back("./Shader/quad.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/screen.frag"sv);
	GS.clear();
	screen_shader = Shader::create(VS, FS, GS);
}

void ScreenRenderer::predraw_screen(TexturePtr& bg, TexturePtr& godray)
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, predraw_fbo);
	glViewport(0, 0, mainScreen.fixedwidth, mainScreen.fixedheight);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glDepthMask(FALSE);
	glDisable(GL_DEPTH_TEST);
	predraw_shader->use();
	predraw_shader->set("color_texture", color_tbo);
	predraw_shader->set("bg_texture", bg);
	predraw_shader->set("godray_texture", godray);
	predraw_shader->set("u_fog.color", fog.color);
	predraw_shader->set("u_fog.density", fog.density);
	predraw_shader->set("u_depth", GameScene::get().get_gbuffer_renderer()->depth_tbo);
	ScreenQuad::get().draw_quad();
	glEnable(GL_DEPTH_TEST);
	glDepthMask(TRUE);
}
void ScreenRenderer::draw_screen(GLuint FRAMEBUFFER, SHADER_MODE mode)
{
	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, FRAMEBUFFER);
	if (0 == FRAMEBUFFER)
	{
		glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	}
	else
	{
		glViewport(0, 0, mainScreen.fixedwidth, mainScreen.fixedheight);
	}
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glDepthMask(FALSE);
	glDisable(GL_DEPTH_TEST);
	screen_shader->use();
	screen_shader->set("predraw_texture", predraw_tbo);
	screen_shader->set("mode", static_cast<int>(mode));

	ScreenQuad::get().draw_quad();
	glEnable(GL_DEPTH_TEST);
	glDepthMask(TRUE);
}

/////////////////////////////////////////////////////////////////////

void gBufferRenderer::init()
{
	auto& mainScreen = Screen::mainScreen();
	glGenFramebuffers(1, &gbuffer_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);

	//position
	worldpos_tbo = Texture::create();
	glGenTextures(1, &worldpos_tbo->id);
	glBindTexture(GL_TEXTURE_2D, worldpos_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, worldpos_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//normal
	normal_tbo = Texture::create();
	glGenTextures(1, &normal_tbo->id);
	glBindTexture(GL_TEXTURE_2D, normal_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normal_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// color spec
	albedospec_tbo = Texture::create();
	glGenTextures(1, &albedospec_tbo->id);
	glBindTexture(GL_TEXTURE_2D, albedospec_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, albedospec_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//depth 
	depth_tbo = Texture::create();
	glGenTextures(1, &depth_tbo->id);
	glBindTexture(GL_TEXTURE_2D, depth_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RED, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, depth_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering 
	GLuint attachments[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	glDrawBuffers(4, attachments);

	// add depth bufer,, etc..

	rboDepthStencil;
	glGenRenderbuffers(1, &rboDepthStencil);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepthStencil);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, mainScreen.fixedwidth, mainScreen.fixedheight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rboDepthStencil);
	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	////
	glGenFramebuffers(1, &lightpass_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, lightpass_fbo);

	lightpass_tbo = Texture::create();
	glGenTextures(1, &lightpass_tbo->id);
	glBindTexture(GL_TEXTURE_2D, lightpass_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lightpass_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	/*
	GLuint _rboDepth;
	glGenRenderbuffers(1, &_rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, _rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, mainScreen.fixedwidth, mainScreen.fixedheight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _rboDepth);
	// finally check if framebuffer is complete
	*/
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cerr << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	vector<string> VS; VS.emplace_back("./Shader/quad.vert"sv);
	vector<string> FS; FS.emplace_back("./Shader/H_light.glsl"sv); FS.emplace_back("./Shader/lightpass_frag.glsl"sv);
	vector<string> GS;
	lightpass_shader = Shader::create(VS, FS, GS);
}

///////////////////////////////////////////////

ScreenQuad::ScreenQuad()
{
	glGenVertexArrays(1, &quad_vao);
	GLuint vbo;
	glGenBuffers(1, &vbo);
	struct QUAD
	{
		glm::vec2 pos;
		//		glm::vec2 tex;  // gl_Position.y / 2 + 0.5f
	};

	QUAD quadv[] =
	{
		{ {-1,-1}}//,{0,0} }
		,{{ 1, 1}}//,{1,1} }
		,{{-1, 1}}//,{0,1} }
		,{{-1,-1}}//,{0,0} }
		,{{ 1,-1}}//,{1,0} }
		,{{ 1, 1}}//,{1,1} }
	};

	glBindVertexArray(quad_vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadv), quadv, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(QUAD), (const GLvoid*)offsetof(QUAD, pos));
	//glEnableVertexAttribArray(1);
	//glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(QUAD), (const GLvoid*)offsetof(QUAD, tex));

	glBindVertexArray(0);
}

void ScreenQuad::draw_quad()
{
	glBindVertexArray(quad_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

///////////////////////////////////////////////

void SunRenderer::init()
{
	auto& mainScreen = Screen::mainScreen();
	godray_param.load("godray");

	glGenFramebuffers(1, &sky_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, sky_fbo);

	skypass_tbo = Texture::create();
	glGenTextures(1, &skypass_tbo->id);
	glBindTexture(GL_TEXTURE_2D, skypass_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, skypass_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	/*
	GLuint rboDepth;
	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screen.width, screen.height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
	*/

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cerr << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/// /
	glGenFramebuffers(1, &godray_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, godray_fbo);

	godraypass_tbo = Texture::create();
	glGenTextures(1, &godraypass_tbo->id);
	glBindTexture(GL_TEXTURE_2D, godraypass_tbo->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mainScreen.fixedwidth, mainScreen.fixedheight, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, godraypass_tbo->id, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cerr << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/// / 
	vector<string> VS;
	vector<string> FS;
	vector<string> GS;

	VS.clear(); VS.emplace_back("./Shader/quad.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/godray.frag"sv);
	GS.clear();
	godraypass_shader = Shader::create(VS, FS, GS);
}



/////////////////////////


/////////////////////////


void GodRayParam::update_uniform_vars(const ShaderPtr& shader, const CameraPtr& camera)
{
	auto vp = GameScene::get().vp_mat(camera);
	glm::vec3 lightdir = Sky::get().get_sun_light()->direction;
	glm::vec3 camerapos = camera->get_position();
	glm::vec3 cameralook = camera->get_look_dir();
	glm::vec3 lightpos = camerapos - lightdir * 1e6f;

	glm::vec4 pos = vp * glm::translate(lightpos) * glm::vec4(0.0, 60.0, 0.0, 1.0);
	pos = pos / pos.w;
	pos = pos * 0.5f + 0.5f;

	glm::vec2 texcoord_lightpos = pos;

	shader->use();

	shader->set("u_texcoord_lightpos", texcoord_lightpos);
	shader->set("u_light_dot_camera", glm::dot(-lightdir, cameralook));

	shader->set("u_samples", u_samples);
	shader->set("u_density", u_density);
	shader->set("u_decay", u_decay);
	shader->set("u_weight", u_weight);
	shader->set("u_exposure", u_exposure);
}

void GodRayParam::load_file_impl(ifstream& file)
{
	LOAD_FILE(file, enable_godray);
	LOAD_FILE(file, u_samples);
	LOAD_FILE(file, u_decay);
	LOAD_FILE(file, u_density);
	LOAD_FILE(file, u_weight);
	LOAD_FILE(file, u_exposure);
}

void GodRayParam::save_file_impl(ofstream& file)
{
	SAVE_FILE(file, enable_godray);
	SAVE_FILE(file, u_samples);
	SAVE_FILE(file, u_decay);
	SAVE_FILE(file, u_density);
	SAVE_FILE(file, u_weight);
	SAVE_FILE(file, u_exposure);
}

/////////////////////////////////////

void ShadowParam::save_file_impl(ofstream& file)
{
	SAVE_FILE(file, shadowbias1);
	SAVE_FILE(file, shadowbias2);
	SAVE_FILE(file, shadowsample);
	SAVE_FILE(file, shadowcamresolution);
	SAVE_FILE(file, preset);
}

void ShadowParam::load_file_impl(ifstream& file)
{
	LOAD_FILE(file, shadowbias1);
	LOAD_FILE(file, shadowbias2);
	LOAD_FILE(file, shadowsample);
	LOAD_FILE(file, shadowcamresolution);
	LOAD_FILE(file, preset);
}
