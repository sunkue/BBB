#pragma once

class Event;
using EventPtr = shared_ptr<Event>;

class EventManager
{
	friend Event;
	using ListType = forward_list<EventPtr>;
	SINGLE_TON(EventManager) = default;
public:
	void update(milliseconds time);
	void Add(EventPtr e);
private:
	ListType EventList;
};

inline int nextEventID()
{
	static int idx = 0;
	return idx++;
}

class Event
{
	friend EventManager;
public:
	Event() :id{ nextEventID ()} {};
	virtual ~Event() = default;
protected:
	void done() { enable = false; }
	int id{};
private:
	bool enable{ true };
	virtual void Start() {};
	virtual void Update(milliseconds time) = 0;
	virtual void Detroy() {};
};

class GameClearEvent : public Event
{
	friend EventManager;
public:
	GameClearEvent(milliseconds clearAt) :clearAt{ clearAt } {};
	virtual ~GameClearEvent() = default;
protected:
	void done() { enable = false; }
	int id{};
private:
	bool enable{ true };
	milliseconds clearAt;
	bool soundFlag{};
	class Sound* sound;
	virtual void Start() override;
	virtual void Update(milliseconds time) override;
	virtual void Detroy() override;
};

///////////////////////////////////////////////////////

