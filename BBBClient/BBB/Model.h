#pragma once


#include "Mesh.h"
#include "Shader.h"


using ModelPtr = shared_ptr<class Model>;

class Model
{
public:
	CREATE_SHARED(Model);

	void draw(const ShaderPtr& shader)const;
	void draw_without(string_view name, const ShaderPtr& shader)const;
	void draw_without(const unordered_set<string_view>& names, const ShaderPtr& shader)const;
	void draw_only(string_view name, const ShaderPtr& shader)const;
	void draw_with_f(string_view name, const ShaderPtr& shader, function<void()> pre_f, function<void()> post_f) const;


	static ModelPtr quad() { return rect(); }
	static ModelPtr rect()
	{
		static ModelPtr quad = create("./Resource/Model/default/rect/rect.obj");
		return quad;
	}

#define SPHERE(color)\
	static ModelPtr sphere_##color()\
	{\
		static ModelPtr sphere = create("./Resource/Model/default/sphere/sphere_"s+#color+"/sphere.obj");\
		return sphere;\
	}\

	SPHERE(yellow);

#define BOX(color)\
	static ModelPtr box_##color()\
	{\
		static ModelPtr box = create("./Resource/Model/default/box/box_"s+#color+"/box.obj");\
		return box;\
	}\

	BOX(red);
	BOX(blue);
	BOX(green);
	BOX(yellow);
	BOX(purple);
	BOX(bludyred);
	BOX(redpurple);
	BOX(orange);
	BOX(gray);
	BOX(cacy);


#define ITEM(type)\
	static ModelPtr item_##type()\
	{\
		static ModelPtr item = create("./Resource/Model/item/item_"s+#type+"/box.obj");\
		return item;\
	}\

	ITEM(rocket);
	ITEM(rocket3);
	ITEM(swap);
	ITEM(steal);
	ITEM(shield);
	ITEM(repair);
	ITEM(boost);
	ITEM(duple);


	static ModelPtr no_model()
	{
		static ModelPtr no_model = create("");
		return no_model;
	}

private:
	Model(string_view path)
	{
		load_model(path);
	}

public:
	size_t texture_size()const {
		return textures_loaded.size();
	}
	const vector<TexturePtr>& get_textures() { return textures_loaded; }
private:
	vector<Mesh> meshes;
	string directory;
	vector<TexturePtr> textures_loaded;

private:
	void load_model(string_view path);
	void process_node(aiNode* node, const aiScene* scene);
	Mesh process_mesh(aiMesh* mesh, const aiScene* scene);
	vector<TexturePtr> load_material_textures(aiMaterial* mat, aiTextureType type, string typeName);
public:
private:
    std::map<string, BoneInfo> m_BoneInfoMap;
    int m_BoneCounter = 0;
public:
    auto& GetBoneInfoMap() { return m_BoneInfoMap; }
    int& GetBoneCount() { return m_BoneCounter; }
	void SetVertexBoneDataToDefault(Vertex& vertex);
	void SetVertexBoneData(Vertex& vertex, int boneID, float weight);
	void ExtractBoneWeightForVertices(std::vector<Vertex>& vertices, aiMesh* mesh, const aiScene* scene);
};

