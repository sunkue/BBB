#include "stdafx.h"
#include "Scene.h"
#include "UI.h"
#include "MouseEvent.h"
#include "Renderer.h"
#include "Collision.h"
#include "Game.h"
#include "GameScene.h"

////////////////////////////////////////////////////////////

void Scene::leaveScene()
{
	MouseEventManager::get().UnBindPosFunc();
	MouseEventManager::get().UnBindButtonFunc();
	MouseEventManager::get().UnBindScrollFunc();
	KeyBoardEventManager::get().UnBindKeyFunc();
	KeyBoardEventManager::get().BindMainKeyFunc(
		[this](const KeyBoardEventManager::key_event& key)->bool
		{ return false; });
}

////////////////////////////////////////////////////////////

void TitleScene::draw()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	TitleUI::get().update();
	TitleUI::get().draw();
}

void TitleScene::update(milliseconds elapsed)
{
}

namespace TITLE_SCENE_EVENTFUNC
{
	void MousePos(const MouseEventManager::pos_event& pos)
	{
		// 마우스가 버튼 위에 있으면 ui size_ 변경하는 거시기
		auto ray_nds = Ray::create2D(pos.xpos, pos.ypos);
		auto& button = TitleUI::get().get_gamemodebutton();
		auto size = button.size_;
		auto sp1 = button.getStartPos1();
		auto sp2 = button.getStartPos2();
		// vec2 pos =  a_position * u_size + u_startpos;
		glm::vec2 ru = { 0, 0 }; ru *= size;
		glm::vec2 ld = { 1,-1 }; ld *= size;

		glm::vec2 ru1 = ru + sp1;
		glm::vec2 ld1 = ld + sp1;

		glm::vec2 ru2 = ru + sp2;
		glm::vec2 ld2 = ld + sp2;

		auto x = ray_nds.x;
		auto y = ray_nds.y;
		if (ru1.x < x && x < ld1.x && y < ru1.y && ld1.y < y)
		{
			TitleScene::get().selected_button_ = TitleScene::SelectedButton::play;
			return;
		}

		if (ru2.x < x && x < ld2.x && y < ru2.y && ld2.y < y)
		{
			TitleScene::get().selected_button_ = TitleScene::SelectedButton::edit;
			return;
		}

		TitleScene::get().selected_button_ = TitleScene::SelectedButton::none;
	}

	void MouseButton(const MouseEventManager::button_event& button)
	{
		if (button.action == GLFW_RELEASE)
		{
			switch (TitleScene::get().selected_button_)
			{
			case TitleScene::SelectedButton::play:
			{
				Game::get().setGameModePlay();
			}
			CASE TitleScene::SelectedButton::edit :
			{
				Game::get().setGameModeEdit();
			}
			}
		}
	}
}

void TitleScene::enterScene()
{
	MouseEventManager::get().BindButtonFunc(
		[](const auto& button) { TITLE_SCENE_EVENTFUNC::MouseButton(button); });

	MouseEventManager::get().BindPosFunc(
		[](const auto& pos) { TITLE_SCENE_EVENTFUNC::MousePos(pos); });
}

void TitleScene::leaveScene()
{
	Scene::leaveScene();
	GameScene::get().init_player();
}

///////////////////////////////////////////////////////////////

namespace MATCHING_SCENE_EVENTFUNC
{
	bool on_readybutton{ false };

	void MousePos(const MouseEventManager::pos_event& pos)
	{
		// 마우스가 버튼 위에 있으면 ui size_ 변경하는 거시기
		auto ray_nds = Ray::create2D(pos.xpos, pos.ypos);
		auto& button = MatchingUI::get().get_readybutton();
		auto size = button.size_;
		auto sp = button.sp_;
		// vec2 pos =  a_position * u_size + u_startpos;
		glm::vec2 ru = { 0, -0.159 }; ru *= size;
		glm::vec2 ld = { 1,-0.647 }; ld *= size;
		ru += sp;	// -0.210, -0.742 
		ld += sp;	// 0.215, -0.986

		auto x = ray_nds.x;
		auto y = ray_nds.y;
		if (ru.x < x && x < ld.x && y < ru.y && ld.y < y)
		{
			on_readybutton = true;
		}
		else
		{
			on_readybutton = false;
		}
	}

	void MouseButton(const MouseEventManager::button_event& button)
	{
		if (button.action == GLFW_RELEASE)
		{
			if (on_readybutton)
			{
				auto& game = Game::get();
				bool ready = game.players[game.playerid].IsReady();
				ready = !ready;
				game.players[game.playerid].ChangeReady(ready);

				cs_change_ready readypacket;
				readypacket.ready = ready;
				Networker::get().do_send(&readypacket);
			}
		}
	}
}

void MatchingScene::enterScene()
{
	MouseEventManager::get().BindPosFunc(
		[](const auto& pos) { MATCHING_SCENE_EVENTFUNC::MousePos(pos); });

	MouseEventManager::get().BindButtonFunc(
		[](const auto& button) { MATCHING_SCENE_EVENTFUNC::MouseButton(button); });

	for (int i = MAX_PLAYER; i < MAX_VEHICLE; i++)
		Game::get().players[i].Connect(GameScene::get().get_cars()[i]);

	auto& cars = GameScene::get().get_cars();
	for (int i = 0; i < MAX_PLAYER; i++)
	{
		chairs_[i].vehicle = make_shared<Obj>(cars[i]->get_model());
		chairs_[i].vehicle->move(Z_DEFAULT * i * 45);
		chairs_[i].vehicle->move(X_DEFAULT * -10);
		chairs_[i].vehicle->rotate(glm::rotate(glm::radians(-90.f), X_DEFAULT));
	}

	chairs_[1].vehicle->move(X_DEFAULT * -40);
	chairs_[2].vehicle->move(X_DEFAULT * -40);

	for (int i = 0; i < MAX_PLAYER; i++)
	{
		chairs_[i].spot = SpotLight::create();
		chairs_[i].spot->position = chairs_[i].vehicle->get_position() + Y_DEFAULT * 0.5f;
		chairs_[i].spot->direction = chairs_[i].spot->position - chairs_[i].vehicle->get_position();
		chairs_[i].spot->set_out_cutoff(0.5);
		chairs_[i].spot->set_in_cutoff(1);
		chairs_[i].spot->attenuation *= 1.5f;
	}

	floor_ = make_shared<Obj>(Model::box_gray());
	floor_->scaling(glm::vec3{ 300, 1, 300 });
	floor_->move({ 0,-6.438,0 });

	camera_ = make_shared<Camera>();
	camera_->set_target(glm::vec3{ -139.3, 29.2, 66.763 });
	camera_->set_position(glm::vec3{ -141.9, 29.461, 66.755 });
	camera_->set_fovy(51);

	vector<string> VS;
	vector<string> FS;
	vector<string> GS;
	VS.clear(); VS.emplace_back("./Shader/default.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/H_light.glsl"), FS.emplace_back("./Shader/matching.frag"sv);
	GS.clear();
	shader_ = Shader::create(VS, FS, GS);
}

void MatchingScene::ready_draw()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	auto& viewport = Screen::mainScreen().viewport;
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
	glClearColor(0.0f, 0.0f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	auto p = Screen::mainScreen().proj_mat(camera_);
	auto v = camera_->view_mat();
	auto camerapos = camera_->get_position();
	auto vp = p * v;

	shader_->use();
	shader_->set("u_vp_mat", vp);
	shader_->set("u_camera_pos", camerapos);
}

void MatchingScene::draw()
{
	ready_draw();

	floor_->update_uniform_vars(shader_);
	floor_->draw(shader_);

	for (int i = 0; i < MAX_PLAYER; i++)
	{
		auto& c = chairs_[i];
		shader_->use();
		shader_->set("u_spotlight" + to_string(i), c.spot);
		c.vehicle->update_uniform_vars(shader_);
		c.vehicle->draw(shader_);
	}

	MatchingUI::get().update();
	MatchingUI::get().draw();

	GameUI::get().get_textUi().drawMatchingSceneUi();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	// MatchingScene::draw_gui();
}

void MatchingScene::draw_gui()
{
	if (Renderer::get().get_scene() != this)
		return;

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	gui::NewFrame();

	camera_->draw_gui("MatchingCamera");

	auto& rsp = GameUI::get().get_textUi().readybutton_sp_;
	auto& rsize = GameUI::get().get_textUi().readybutton_size_;
	gui::Begin("readybutton");
	gui::DragFloat2("sp", glm::value_ptr(rsp), 0.0125f, -1, 1, "%lf");
	gui::DragFloat("size", &rsize, 0.0125f, -1, 1, "%lf");
	gui::End();

	gui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(gui::GetDrawData());
}

void MatchingScene::update(milliseconds elapsed)
{
	for (int i = 0; i < MAX_PLAYER; i++)
	{
		if (Game::get().players[i].IsFree())
		{
			chairs_[i].spot->specular = glm::vec3{ -0.3 };
			chairs_[i].spot->ambient = glm::vec3{ -0.3 };
			chairs_[i].spot->diffuse = glm::vec3{ -0.3 };
		}
		else if (Game::get().players[i].IsReady())
		{
			chairs_[i].spot->specular = glm::vec3{ 1, 1, 0.6 };
			chairs_[i].spot->ambient = glm::vec3{ 1, 1, 0.6 };
			chairs_[i].spot->diffuse = glm::vec3{ 1, 1, 0.6 };
		}
		else
		{
			chairs_[i].spot->specular = glm::vec3{ 1 };
			chairs_[i].spot->ambient = glm::vec3{ 1 };
			chairs_[i].spot->diffuse = glm::vec3{ 1 };
		}

		chairs_[i].vehicle->rotate(glm::rotate(elapsed.count() / 1000.f, Z_DEFAULT));
	}
}

void MatchingScene::leaveScene()
{
	Scene::leaveScene();
}



///////////////////////////////////////////////////////////////

namespace GAME_END_SCENE_EVENTFUNC
{
	void MouseButton(const MouseEventManager::button_event& button)
	{
		if (button.action == GLFW_RELEASE)
		{
			thread t{ system, "BBB" };
			t.detach();
			exit(0);
		}
	}
}

void GameEndScene::enterScene()
{
	Game::get().gameEnd = true;

	for (auto& c : GameScene::get().get_cars())
	{
		c->reset1();
	}

	MouseEventManager::get().BindButtonFunc(
		[](const auto& button) { GAME_END_SCENE_EVENTFUNC::MouseButton(button); });

	// Renderer::get().chagneScene(&TitleScene::get());

	GameScene::get().get_main_camera()->set_target(glm::vec3{ 474.411,2.481,42.251 });
	GameScene::get().get_main_camera()->set_position(glm::vec3{ 474.708,2.549,43.205 });
	GameScene::get().get_main_camera()->set_fovy(45);
	GameScene::get().get_main_camera()->set_ownner(nullptr);

	for (auto& c : GameScene::get().get_cars())
	{
		// - 0.563
		if (c->get_objid() == Game::get().playerid)
		{
			c->set_position_nobounding({ 477.517, -0.876, 30.227 });
			c->set_scale_nobounding(glm::vec3{ 0.05f });
			c->set_rotation_nobounding({ 0.339, 0,-0.941,0 });
		}

		switch (c->get_rank())
		{
		case 1:
			c->set_position_nobounding({ 474.4,2.581 - 0.563,27.062 });
			c->set_scale_nobounding(glm::vec3{ 0.14f });
			c->set_rotation_nobounding({ -0.663, 0,0.749,0 });
			break;
		case 2:
			c->set_position_nobounding({ 470.908,1.773 - 0.563,26.042 });
			c->set_scale_nobounding(glm::vec3{ 0.135f });
			c->set_rotation_nobounding({ -0.690, 0,0.724,0 });
			break;
		case 3:
			c->set_position_nobounding({ 477.55,0.254 - 0.563,25.249 });
			c->set_scale_nobounding(glm::vec3{ 0.12f });
			c->set_rotation_nobounding({ 0.645, 0,-0.764,0 });
			break;
		}
	}
}

void GameEndScene::draw()
{
	GameScene::get().draw();
}

void GameEndScene::update(milliseconds elapsed)
{

	// 애니메이션
}

void GameEndScene::leaveScene()
{
	Scene::leaveScene();
	Game::get().set_mode_once = false;
	Game::get().gamemode_ = GameMode::TITLE;
	Networker::get().close();
	for (int i = 0; i < MAX_VEHICLE; i++)
	{
		Game::get().players[i].Disconnect();
	}
	for (auto& c : GameScene::get().get_cars())
	{
		c->reset2();
	}
}

///////////////////////////////////////////////////////////////

LoadingScene::LoadingScene()
{
	vector<string> VS;
	vector<string> FS;
	vector<string> GS;
	VS.clear(); VS.emplace_back("./Shader/Loading.vert"sv);
	FS.clear(); FS.emplace_back("./Shader/Loading.frag"sv);
	GS.clear();
	shader_ = Shader::create(VS, FS, GS);

	SetQuadvao(quad_vao_, 2, 2);
}

void LoadingScene::enterScene()
{
	loadingTimeInterVal = {};
	size_ = 2;
	closed = false;
	prevScene->update(1ms);
}

void LoadingScene::draw()
{
	if (!closed)
		prevScene->draw();
	else
		nextScene->draw();


	auto& mainScreen = Screen::mainScreen();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(mainScreen.viewport.x, mainScreen.viewport.y, mainScreen.viewport.z, mainScreen.viewport.w);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	//  draw circle
	shader_->use();
	// 2~-1
	shader_->set("u_size", size_);
	shader_->set("u_color", color_);
	glBindVertexArray(quad_vao_);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

void LoadingScene::update(milliseconds elapsed)
{
	loadingTimeInterVal += elapsed;

	size_ -= (1 - closed * 2) * (static_cast<float>(elapsed.count()) / loadingTime.count()) * 3.f;
	size_ = glm::clamp(size_, -1.f, 2.f);

	if (loadingTime < loadingTimeInterVal)
	{
		if (closed = !closed)
		{
			nextScene->enterScene();
			nextScene->update(1ms);
			loadingTimeInterVal = {};
		}
		else
		{
			Renderer::get().changeScene(nextScene);
			nextScene->update(1ms);
			nextScene->draw();
		}
	}

}

void LoadingScene::leaveScene()
{
	Scene::leaveScene();
}
