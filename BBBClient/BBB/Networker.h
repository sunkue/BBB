#pragma once
class Networker
{
	SINGLE_TON(Networker);

	~Networker();

public:
	void connect(const char* server_ip);
	void close()
	{
		cout << "reaccept" << endl;
		::closesocket(socket_);
	}
	void do_recv();

	void do_send(const void* const packet);

	void process_packet(const char* const packet);

private:
	SOCKET socket_;
	mutex send_lock;
	array<char, MAX_BUFFER_SIZE> recv_buf_{};
};

