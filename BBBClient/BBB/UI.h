#pragma once

#include "texture.h"
#include "Shader.h"
#include "TextManager.h"

void SetQuadvao(GLuint& vao, float w = 1, float h = 1);

class GameUI
{
	SINGLE_TON(GameUI) = default;

public:
	void draw();
	void update();

private:
	class RankingUI
	{
		const glm::vec2 start_pos = { -0.975, 0.625 };
		const glm::vec2 diff = { 0 , 0.0625 };
	public:
		RankingUI();

		void draw();
		void update();
	private:
	public:
		GLuint quad_vao_;
		TexturePtr profile_gray_;
		array<TexturePtr, MAX_VEHICLE> car_textures_;
		vector<glm::vec2> dynamic_sps_; // 재밌게 움직이는 ui. 실제 ui 위치. 플레이어 순.
		vector<glm::vec2> static_sps_; // 고정석. ui들의 최종 목적지. 순위순.
		ShaderPtr shader_{ Shader::uimix() };
		glm::vec2 size_{ 0.125, 0.0625 };
	};

	class TextUI
	{
	public:
		TextUI();

		void draw();
		void drawEndSceneUi();
		void drawMatchingSceneUi();
		void update();
	private:
	public:
		TextRenderer TextRenderer_;
		glm::vec2 lab_startpos_{ -0.973875f , 0.830375f };
		float lab_size_{ 0.00275 };
		glm::vec2 time_startpos_{ -0.970375f , 0.695625f };
		float time_size_{ 0.001625 };
		glm::vec2 rank_startpos_{ -0.970375f , 0.577875f };
		float rank_size_{ 0.001625 };
		glm::vec2 countdown_startpos_{ -0.09375f , 0.303875f };
		float countdown_size_{ 0.00475 };
		int countdown_{ -1 };
		glm::vec3 color_{};

		// endscene
		glm::vec2 end_rank_sp_{ -0.8525 , 0.76375f };
		glm::vec2 end_diff_{ 0 , -0.155625 };
		float end_rank_size_{ 0.001375 };

		// matchingscene
		glm::vec2 ready_sp_[4];
		float ready_size_{ 0.001375 };
		glm::vec2 readybutton_sp_{ -0.175, -0.8875 };
		float readybutton_size_{ 0.0015 };
	};

	class HpUI
	{
	public:

	private:
		TexturePtr frame_;
		TexturePtr main_;
		ShaderPtr shader_;
	};

	class WarningUI
	{
	public:

	private:
		TexturePtr frame_;
		TexturePtr main_;
		ShaderPtr shader_;
	};

	class BackMirrorUI
	{
	public:
		BackMirrorUI();

		void draw();
		void update();
	public:
		GET_REF(fbo);
		SET(fbo);

		GET_REF(main);

		GET_REF_UNSAFE(startpos);
		GET_REF_UNSAFE(size);
		GET_REF_UNSAFE(startposFrame);
		GET_REF_UNSAFE(sizeFrame);
	private:
		GLuint quad_vao_;
		GLuint fbo_;
		TexturePtr mask_;
		TexturePtr main_;
		TexturePtr frame_;
		ShaderPtr shaderFrame_{ Shader::ui() };
		ShaderPtr shader_{ Shader::uimask() };
		glm::vec2 startpos_{ -0.37375, 1.15125f };
		glm::vec2 size_{ 0.77125, 0.6975 };
		glm::vec2 startposFrame_{ -0.39125, 1.0775f };
		glm::vec2 sizeFrame_{ 0.805, 0.5575 };
	};


	class MinimapUI
	{
	public:
		MinimapUI();

		void draw();
		void update() {};
	public:
		GET_REF(fbo);
		SET(fbo);

		GET_REF(main);

		GET_REF_UNSAFE(startpos);
		GET_REF_UNSAFE(size);
	private:
		GLuint quad_vao_;
		GLuint fbo_;
		TexturePtr frame_;
		TexturePtr main_;
		ShaderPtr shader_{ Shader::uimask() };
		glm::vec2 startpos_{ 0.475f , 0.975f };
		glm::vec2 size_{ 0.6, 0.6 };
		//		glm::vec2 startpos_{ 0.3875f , 0.6625f };
		//		glm::vec2 size_{ 0.725, 0.725 };
	};


public:
	GET_REF_UNSAFE(backmirror);
	GET_REF_UNSAFE(minimap);
	GET_REF_UNSAFE(textUi);
	GET_REF_UNSAFE(ranking);
private:
	RankingUI ranking_;
	HpUI hp_;
	WarningUI warning_;
	BackMirrorUI backmirror_;
	MinimapUI minimap_;
	TextUI textUi_;
};


class TitleUI
{
	SINGLE_TON(TitleUI) = default;

public:
	void draw();
	void update() {};

private:
	class GameModeButtonUI
	{
	public:
		GameModeButtonUI();

		glm::vec2 getStartPos1()
		{
			auto center = center_;
			auto diff = diff_;
			auto size = size_;
			glm::vec2 ancher{ -size.x / 2, size.y / 2 };
			return center - diff + ancher;
		}

		glm::vec2 getStartPos2()
		{
			auto center = center_;
			auto diff = diff_;
			auto size = size_;
			glm::vec2 ancher{ -size.x / 2, size.y / 2 };
			return center + diff + ancher;
		}

		void draw();

		void update() {};

	public:
		GLuint quad_vao_;
		TexturePtr playButton_;
		TexturePtr editButton_;
		glm::vec2 center_{ 0, 0 };
		glm::vec2 diff_{ 0.300000 , 0 }; // - +
		glm::vec2 size_{ 0.425000,0.562500 };
		ShaderPtr shader_{ Shader::ui() };
	};
public:
	GET_REF_UNSAFE(gamemodebutton)
private:
	GameModeButtonUI gamemodebutton_;
};



class MatchingUI
{
	SINGLE_TON(MatchingUI) = default;

public:
	void draw();
	void update()
	{
		profile_.update();
		readybutton_.update();
	};

private:
	class ProfileUI
	{
	public:
		ProfileUI();
		void draw();
		void update();

	public:
		GLuint quad_vao_;
		TexturePtr profile_gray_;
		TexturePtr ready_frame_gray_;
		glm::vec2 center_{ -0.85, 0.275 };
		glm::vec2 diff_{ 0.425000 , 0.3875 };
		glm::vec2 size_{ 0.425000 , 0.562500 };
		ShaderPtr shader_{ Shader::uimix() };
	};

	class ReadyButtonUI
	{
	public:
		ReadyButtonUI();
		void draw();
		void update();

	public:
		GLuint quad_vao_;
		TexturePtr readybutton_gray_;
		glm::vec2 sp_{ -0.2125, -0.6625 };
		glm::vec2 size_{ 0.425 , 0.5 };
		ShaderPtr shader_{ Shader::uimix() };
	};
public:
	GET_REF_UNSAFE(profile);
	GET_REF_UNSAFE(readybutton);
private:
	ProfileUI profile_;
	ReadyButtonUI readybutton_;
};