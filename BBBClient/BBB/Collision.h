#pragma once

#include "FileHelper.h"
#include "Camera.h"
#include "Model.h"
// 구, 사각형, 레이, 프리스텀

struct Ray
{
	glm::vec3 origin{};
	glm::vec3 dir{};
	static Ray create(const CameraPtr& camera, int wx, int wy);
	static glm::vec2 create2D(int wx, int wy);
};

struct BoundingSphere
{
	glm::vec3 center{};
	float radius{ 1 };
	bool intersects(const struct BoundingSphere& sh) const;
	bool intersects(const struct BoundingFrustum& fr) const;
	bool intersects(const struct Ray& ray)const;
};

struct BoundingBox
{
	glm::vec3 center{};
	glm::vec3 extents{ 1 };
	glm::quat orientation{};
	bool intersects(const struct BoundingBox& box) const;
	bool intersects(const struct BoundingFrustum& fr) const;
	bool intersects(const struct Ray& ray, float& dist)const;
private:
	bool getSeparatingPlane(const glm::vec3& RPos, const glm::vec3& Plane, const BoundingBox& box) const;
	glm::vec3 AxisX()const { return glm::rotate(orientation, X_DEFAULT); }
	glm::vec3 AxisY()const { return glm::rotate(orientation, Y_DEFAULT); }
	glm::vec3 AxisZ()const { return glm::rotate(orientation, Z_DEFAULT); }
};


struct BoundingFrustum
{
	glm::vec3 origin{};
	glm::quat orientation{};

	float right_slope;
	float left_slope;
	float top_slope;
	float bottom_slope;
	float n, f;

	bool intersects(const struct BoundingSphere& sh) const;
	bool intersects(const struct BoundingBox& box) const;
	bool intersects(const struct BoundingFrustum& fr) const;
	bool intersects(const struct Ray& ray)const;

	static void CreateFromMatrix(glm::mat4 projection);
};

///////////////////////////////////

class Boundings : public IDataOnFile
{
protected:
	virtual void save_file_impl(ofstream& file) final;
	virtual void load_file_impl(ifstream& file) final;
private:
	BoundingSphere L1_current;
	BoundingSphere L1_prev;
	BoundingBox L2_current;
	BoundingBox L2_prev;
public:
	// L1 should contain L2.
	BoundingSphere L1;
	BoundingBox L2;
	ModelPtr sphere_{ Model::sphere_yellow() };
	ModelPtr box_{ Model::box_yellow() };
	void rotate(const glm::quat& q) { L2.orientation *= q; }
	void move(const glm::vec3& dif) { L1.center += dif; L2.center += dif; }
	void scaling(const glm::vec3& ratio) { L2.extents *= ratio; L1.radius *= (compMax(ratio) > 1.f) ? glm::compMax(ratio) : glm::compMin(ratio); }
	bool intersects(const Ray& ray, float& dist) const
	{
		return L2.intersects(ray, dist);
	}
	bool intersects(const Boundings& other) const
	{
		if (L1.intersects(other.L1))
		{
			return L2.intersects(other.L2);
		}
		return false;
	}

	void ToggleShow()const { L1_on = !L1_on; L2_on = !L2_on; }
private:
	mutable bool L1_on = false;
	mutable bool L2_on = false;
public:
	void set_drawable(bool state) { L1_on = state; L2_on = state; }
public:
	void trans_gui();
	void draw_gui();
	void draw(const ShaderPtr& shader)const;
};
