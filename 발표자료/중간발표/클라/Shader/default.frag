#version 450

struct Material
{
	sampler2D albedo1;
	sampler2D specular1;
	float shininess;
};

in VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} fs_in;

uniform Material u_material;

out vec4 o_color;

void main()
{
	vec2 texcoord = fs_in.texcoord;

	o_color = texture(u_material.albedo1, texcoord);
}

