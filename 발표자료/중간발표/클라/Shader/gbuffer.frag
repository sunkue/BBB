#version 450

struct Material
{
	sampler2D albedo1;
	sampler2D specular1;
	sampler2D normalmap1;
	float shininess;
};

layout(std140) uniform CAMERA_POS
{
	uniform vec3 u_camera_pos;
};

layout(location = 0) out vec4 g_world_pos;
layout(location = 1) out vec4 g_normal;
layout(location = 2) out vec4 g_albedospec;
layout(location = 3) out float g_depth;

in VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} fs_in;

uniform Material u_material;
uniform vec3 u_rimcolor = vec3(0);
uniform float u_rimpower = 0;
uniform float u_rimboundary = 0;

vec3 caculate_rim(vec3 view_dir, vec3 normal, vec3 rimcolor, float boundary, float rimpower)
{
	float rim = clamp(dot(view_dir, normal), 0, 1);

	float positiveOnTrue = rim - boundary; // boundary < rim //	+/-
	rim = positiveOnTrue / abs(positiveOnTrue + 0.00001); // +1, -1

	return (pow(1 - rim, rimpower) * rimcolor);
}

void main()
{
	vec3 world_pos = fs_in.world_pos;
	vec3 normal = fs_in.normal;
	vec2 texcoord = fs_in.texcoord;
	vec3 view_dir = u_camera_pos - world_pos;

	g_world_pos = vec4(world_pos, 0);
	g_normal = vec4(normal, 0);
	g_albedospec.rgb = texture(u_material.albedo1, texcoord).rgb;
	g_albedospec.rgb += caculate_rim(view_dir, normal, u_rimcolor, u_rimboundary, u_rimpower);
	g_albedospec.a = texture(u_material.specular1, texcoord).r;

	g_depth = gl_FragCoord.z;
}

