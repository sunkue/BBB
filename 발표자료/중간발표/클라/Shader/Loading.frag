#version 450

uniform vec3 u_color; 
uniform float u_size; 

in vec2 texcoord; 

out vec4 o_flagcolor;

void main() 
{ 
    float x = (texcoord.x * 2 - 1) * 2;
    float y = (texcoord.y * 2 - 1);

    float a = clamp(sqrt(x*x + y*y) - u_size, 0 , 1);
    o_flagcolor = vec4(u_color, a);
} 

