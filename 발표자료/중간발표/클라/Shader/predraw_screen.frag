#version 450

uniform sampler2D color_texture; 
uniform sampler2D bg_texture;
uniform sampler2D godray_texture;

in vec2 texcoord; 

out vec4 o_flagcolor;

struct FogParameters
{
	vec3 color;
	float density;
};

uniform FogParameters u_fog;
uniform sampler2D u_depth;

const vec2 invViewportSize = vec2(1.f/1240.f, 1.f/720.f);
const vec2 cameraRange = vec2(0.1f, 20000.f);

float calc_depth(in float z)
{
    return (2.0 * cameraRange.x) / (cameraRange.y + cameraRange.x - z*(cameraRange.y - cameraRange.x));
}
void main() 
{ 
    float gamma = 1.6f;
    vec4 color = texture(color_texture, texcoord) + mix(texture(bg_texture,texcoord), texture(godray_texture,texcoord) ,0.4f) * 1.4;
    
    vec2 coords = gl_FragCoord.xy * invViewportSize;
    float z = calc_depth(texture(u_depth, coords).r);
    float fogFactor = exp2(- u_fog.density * u_fog.density * z * z * 1.442695); // log2
    fogFactor = clamp(fogFactor, 0.0, 1.0);
    color.rgb = mix(u_fog.color, color.rgb, fogFactor);

    o_flagcolor = color;
} 

