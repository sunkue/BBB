#version 450
#pragma optionNV(unroll none)
layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 85) out;
//layout (line_strip, triangle_strip, max_vertices = 128) out;

uniform int tess = 30;

vec4 hermit(float t, vec4 p0, vec4 v0, vec4 p1, vec4 v1)
{
/*
	const mat4 m =
	mat4
	(
		 2, -3,  0, 1,
		 1, -2,  1, 1,
		-2,  3,  0, 0,
		 1, -1,  0, 0
	);
*/

	vec4 Tmat = vec4(pow(t,3), pow(t,2), t, 1);
	mat4 params =  mat4(p0, v0, p1, v1);

	float s = 1 - t;
	float s2 = pow(s, 2);
	float t2 = pow(t, 2);

	vec4 ret = s2*(1+2*t)*p0 + t2*(1+2*s)*p1 + s2*t*v0 - s*t2*v1;

//	vec4 ret = params *  m * Tmat;
	return ret;
}

in vec4 outliner[];
in vec4 inliner[];

in GS_IN
{
	vec4 world_pos_in;
	vec4 world_pos_out;
	vec3 normal;
	vec2 texcoord;
} gs_in[];

out VS_OUT
{
	vec3 world_pos;
	vec3 normal;
	vec2 texcoord;
} gs_out;

void main(void) 
{
	float dt = 1 / (float(tess));
	float tension = 0;

//	vec4 v[2];
	vec4 vo[4];
	vec4 vi[4];
	
	vec4 vwo[4];
	vec4 vwi[4];

//	v[0] = (1 - tension) * (gl_in[2].gl_Position - gl_in[0].gl_Position) / 2;
//	v[1] = (1 - tension) * (gl_in[3].gl_Position - gl_in[1].gl_Position) / 2;

	vo[1] = (1 - tension) * (outliner[2] - outliner[0]) / 2;
	vo[2] = (1 - tension) * (outliner[3] - outliner[1]) / 2;

	vi[1] = (1 - tension) * (inliner[2] - inliner[0]) / 2;
	vi[2] = (1 - tension) * (inliner[3] - inliner[1]) / 2;

	vwo[1] = (1 - tension) * (gs_in[2].world_pos_out - gs_in[0].world_pos_out) / 2;
	vwo[2] = (1 - tension) * (gs_in[3].world_pos_out - gs_in[1].world_pos_out) / 2;

	vwi[1] = (1 - tension) * (gs_in[2].world_pos_in - gs_in[0].world_pos_in) / 2;
	vwi[2] = (1 - tension) * (gs_in[3].world_pos_in - gs_in[1].world_pos_in) / 2;

	for (int i = 0; i <= tess; i++)
	{
		float t = dt * i;

		vec4 pointIn = hermit(t, inliner[1], vi[1], inliner[2], vi[2]);
		vec4 worldposIn = hermit(t, gs_in[1].world_pos_in, vwi[1], gs_in[2].world_pos_in, vwi[2]);
		gs_out.texcoord = vec2(0, i%2);
		gs_out.world_pos = worldposIn.rgb;
		gs_out.normal = vec3(0, 1, 0);
		gl_Position =  pointIn;
		EmitVertex();

		vec4 pointOut = hermit(t, outliner[1], vo[1], outliner[2], vo[2]);
		vec4 worldposOut = hermit(t, gs_in[1].world_pos_out, vwo[1], gs_in[2].world_pos_out, vwo[2]);
		
		vec4 nextPointOut = hermit(t + dt, outliner[1], vo[1], outliner[2], vo[2]);
		float height = length(nextPointOut - pointOut);
		float width = length(pointIn - pointOut);
		float aspect = width / height;
		
		gs_out.texcoord = vec2(aspect, i%2); 
		gs_out.world_pos = worldposOut.rgb;
		gs_out.normal = vec3(0, 1, 0);
		gl_Position =  pointOut;
		EmitVertex();
//		vec4 point = hermit(t, gl_in[1].gl_Position, v[0], gl_in[2].gl_Position, v[1]);
//		gl_Position =  point;
//		EmitVertex();
	}

	EndPrimitive();
} 

