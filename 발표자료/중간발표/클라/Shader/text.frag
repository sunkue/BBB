#version 450

uniform sampler2D u_texture; //text 
uniform vec3 u_color;

in vec2 texcoord; 

out vec4 o_flagcolor;

void main() 
{ 
    vec4 sampled = vec4(vec3(1),texture(u_texture, texcoord));
    o_flagcolor = vec4(u_color, 1.0) * sampled;
} 

