#version 330 core



layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
out vec2 texcoord;


void main()
{
    gl_Position = vec4(vertex.xy, 0.0, 1.0);
    texcoord = vertex.zw;
}  
