#pragma once


#include "Defines.h"
#include "enum.h"
#include "PacketZip.h"


//====================================

#pragma warning(push)
#pragma warning(disable: 26812)
#pragma warning(disable: 26495)

BETTER_ENUM
(
	PACKET_TYPE, uint8

	, NONE = 0
	/* Client 2 Server */

	, Cs = 10
	, Cs_connect
	, Cs_change_ready
	, Cs_ready2run
	, Cs_player_info
	, Cs_use_item


	/* Server 2 Client */

	, Sc = 100
	, Sc_servertime
	, Sc_accept
	, Sc_new_matcher
	, Sc_change_ready
	, Sc_matchcomplete
	, Sc_gamestart
	, Sc_remove_player
	, Sc_set_player_info
	, Sc_set_player_info_hard
	, Sc_outed
	, Sc_collide
	, Sc_regen
	, Sc_player_accquire_item
	, Sc_player_use_item
	, Sc_clearlab
	, Sc_gameend
);

// 가용길이 패킷 
// FIXED_PACKET // DYNAMIC_PACKET => chat, 맵 로딩 등,,,

#pragma pack (push, 1)
//================ BASE ================

template<class T>
struct packet_base
{
	packet_size_t size = sizeof(T);
	PACKET_TYPE packet_type = +PACKET_TYPE::_from_string_nocase(typeid(T).name() + 7);
};
#define PACKET(name) struct name : packet_base<name>											

PACKET(none)
{
};

//============== packet =================

PACKET(sc_servertime)
{
	clk::time_point server_time;
};

PACKET(cs_connect)
{
	size_t version;
};

PACKET(sc_accept)
{
	InGameID ur_ingameid;
	GameRoomID ur_gameroomid;
};

PACKET(sc_new_matcher)
{
	InGameID new_matcher_ingameid;
	bool ready;
};

PACKET(sc_change_ready)
{
	InGameID ingameid;
	bool ready;
};

PACKET(cs_change_ready)
{
	bool ready;
};

PACKET(sc_matchcomplete)
{

};

PACKET(cs_ready2run)
{
};

PACKET(sc_gamestart)
{
	clk::time_point starttime;
	random_device::result_type seed;
};

PACKET(sc_remove_player)
{
	InGameID removed_player_ingameid;
};

PACKET(sc_set_player_info) // copy of cs_player_info
{
	clk::time_point timestamp;
	Net::CompressedPos_t pos;
	Net::CompressedRot_t rotate;
	Net::CompressedLinearSpeed_t linear_speed;

	InGameID ingameid;
	int8 rank;
};

PACKET(cs_player_info)
{
	clk::time_point timestamp; // this is servertime.
	Net::CompressedPos_t pos;
	Net::CompressedRot_t rotate;
	Net::CompressedLinearSpeed_t linear_speed;
};

PACKET(sc_regen)
{
	InGameID ingameid;
	int8 includedtracknode;
};

PACKET(sc_outed)
{
	InGameID ingameid;
};

PACKET(sc_clearlab)
{
	InGameID ingameid;
	milliseconds clearAt;
};

PACKET(sc_gameend)
{
	clk::time_point endtime;
};

PACKET(sc_collide)
{
	Net::CompressedLinearSpeed_t force;
};

PACKET(sc_player_accquire_item)
{
	InGameID ingameid;
	uint8 itemtype;
	ObjID itemobjid;
};

PACKET(cs_use_item)
{
	// valid check
	uint8 itemtype; 
};

PACKET(sc_player_use_item)
{
	InGameID ingameid;
	// valid check
	uint8 itemtype;
};


#pragma pack(pop)
#pragma warning(pop)
