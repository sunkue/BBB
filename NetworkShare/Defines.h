#pragma once

//====================================

using BYTE = uint8_t;

using int8 = int8_t;
using int16 = int16_t;
using int32 = DWORD;
using int64 = int64_t;

using uint8 = uint8_t;
using uint16 = uint16_t;
using uint32 = uint32_t;
using uint64 = uint64_t;

//====================================

using _ID = int;
using InGameID = _ID; //MAX_PLAYER
using GameRoomID = _ID; //MAX_GAMEROOM
using ClientID = _ID; // MAX_CLIENT
using ObjID = _ID; // objID 주로 아이템임..

// 네트워크로 전송하기위해 압축된 타입
using NetID = _ID; // nt<type,1> :: ID
using packet_size_t = uint8; //  need to be unsigned

//====================================

const uint16_t SERVER_PORT = 8282;

//====================================
const int MIN_PLAYER = 1;
const int MAX_PLAYER = 4;
const int MAX_BOT = 20;
const int MAX_VEHICLE = MAX_PLAYER + MAX_BOT;
const int MAX_ROOM = 10;
const int MAX_LAB = 3;
//const int MAX_LAB = 1;
const int MAX_CLIENT = MAX_PLAYER * MAX_ROOM;
static_assert(MAX_PLAYER < std::numeric_limits<ClientID>::max(), "Player ID could overflow");
const int MAX_PACKET_SIZE = std::numeric_limits<packet_size_t>::max() + 1;
const int MAX_BUFFER_SIZE = MAX_PACKET_SIZE * 16;
static_assert(MAX_PACKET_SIZE <= MAX_BUFFER_SIZE, "Net Buffer could overflow");

//====================================

const glm::vec3 V_ZERO{ glm::vec3{0} };
const glm::vec3 X_DEFAULT{ 1, 0, 0 };
const glm::vec3 Y_DEFAULT{ 0, 1, 0 };
const glm::vec3 Z_DEFAULT{ 0, 0, 1 };
const glm::vec3 V3_DEFAULT{ glm::vec3(1) };
const glm::vec4 V4_DEFAULT{ glm::vec3(0), 1 };
const glm::mat4 M_DEFAULT{ 1 };
constexpr float ROOT3{ 1.732421875 };
constexpr float HALF_ROOT3 = { 0.8662109375 };
constexpr float ellipsis = { 0.00001f };
constexpr float ellipsis2 = { 0.1f };

const glm::vec3 HEADING_DEFAULT = X_DEFAULT;
const glm::vec3 UP_DEFAULT = Y_DEFAULT;
const glm::vec3 RIGHT_DEFAULT = Z_DEFAULT;