#pragma once
#include "Includes.h"
#include <array>
/////////////////////////////////////////////////////

#define SUNKUE_NET Net

/////////////////////////////////////////////////////

namespace SUNKUE_NET
{
	constexpr auto float_precision1 = 1;
	constexpr auto float_precision2 = 2;
	constexpr auto float_precision3 = 3;
	constexpr auto float_precision4 = 4;
	constexpr auto float_precision5 = 5;

	template<class compressed_T, size_t N>
	struct nt
	{
		using compressed_t = compressed_T;
		using this_type = nt<compressed_t, N>;

		std::array<compressed_t, N> data;

		template<class originT> inline static nt<compressed_t, N> encode(const originT& var);
		template<class originT> inline static originT decode(const nt<compressed_t, N>& var);
	};
};

namespace SUNKUE_NET
{
#define NT_ENCODE(type, n, origin_type)template<> template<> inline nt<type, n> nt<type, n>::encode(const origin_type& var)
#define NT_DECODE(type, n, origin_type)template<> template<> inline origin_type nt<type, n>::decode(const nt<type, n>& var)

	//////////////////////////////////////////////////////////////////////////////
	constexpr auto typetest = numeric_limits<int16>::max();
	constexpr auto typetest_ = numeric_limits<int8>::max();
	
	using CompressedPos_t = Net::nt<int16, 2>;

	NT_ENCODE(CompressedPos_t::compressed_t, 2, glm::vec3)
	{
		int mul = std::pow(10, float_precision1);
		this_type ret{};
		ret.data.at(0) = round(var.x * mul);
		ret.data.at(1) = round(var.z * mul);
		return ret;
	};

	NT_DECODE(CompressedPos_t::compressed_t, 2, glm::vec3)
	{
		int div = std::pow(10, float_precision1);
		glm::vec3 ret
		{
			(float)var.data.at(0),
			0.f,
			(float)var.data.at(1)
		};
		ret /= div;
		return ret;
	};

	//////////////////////////////////////////////////////////////////////////////

	using CompressedRot_t = Net::nt<int8, 2>;

	NT_ENCODE(CompressedRot_t::compressed_t, 2, glm::quat)
	{
		int mul = std::pow(10, float_precision2);
		this_type ret{};
		ret.data.at(0) = round(var.w * mul);
		ret.data.at(1) = round(var.y * mul);
		return ret;
	};

	NT_DECODE(CompressedRot_t::compressed_t, 2, glm::quat)
	{
		int div = std::pow(10, float_precision2);
		glm::quat ret
		{
			(float)var.data.at(0),
			0.f,
			(float)var.data.at(1),
			0.f
		};
		ret /= div;
		return ret;
	};

	//////////////////////////////////////////////////////////////////////////////
	
	using CompressedLinearSpeed_t = CompressedPos_t;



	
	//////////////////////////////////////////////////////////////////////////////
};



/////////////////////////////////////////////////////

	/*
int main()
{
	vec3 origin{ 1.18f, 2.21f, -3.36f };

	auto net_data = Net::nt<char, 3>::encode<vec3>(origin);
	auto rec_data = Net::nt<int8, 3>::decode<vec3>(net_data);

	std::cout << rec_data.x << std::endl;
	std::cout << rec_data.y << std::endl;
	std::cout << rec_data.z << std::endl;
}
	*/