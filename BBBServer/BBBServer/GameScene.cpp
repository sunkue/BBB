#include "stdafx.h"
#include "GameScene.h"
#include "Game.h"

void GameScene::update(milliseconds elapsed)
{
	for (auto& c : cars_)
		c->clear_frame_log();

	track_->update(elapsed);
	for (auto& c : cars_)
		c->update(elapsed);

	update_ranks();
}

void GameScene::update_ranks()
{
	array<InGameID, MAX_VEHICLE> carids;
	std::iota(ALLOF(carids), 0);

	std::sort(carids.begin(), carids.end(),
		[&](InGameID a, InGameID b)
		{
			return !cars_[a]->rank_worse_than(*cars_[b]);
		});

	for (int rank = 0; rank < carids.size(); rank++)
	{
		cars_[carids[rank]]->set_rank(rank + 1);
	}
}
///////////////////////////////////////////////////////////////////

void GameScene::init(GameRoomID roomid)
{
	track_ = make_unique<Track>(roomid);
	deco_ = make_unique<Deco>(roomid);

	for (InGameID id = 0; id < MAX_PLAYER; id++)
	{
		cars_[id] = make_shared<VehicleObj>(roomid, id);
	}

	for (InGameID id = MAX_PLAYER; id < MAX_VEHICLE; id++)
	{
		cars_[id] = make_shared<AutoVehicleObj>(roomid, id);
	}

	for (auto& car : cars_)
	{
		track_->init_include_obj(*car.get(), true);
		car->set_enable(false);
	}
}

void GameScene::start()
{
	for (auto& v : cars_)
	{
		if (auto av = dynamic_cast<AutoVehicleObj*>(v.get()))
		{
			av->set_enable(true);
		}
	}
}
