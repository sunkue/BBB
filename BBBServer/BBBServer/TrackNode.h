#pragma once

#include "Obj.h"
#include "Item.h"


using TrackNodePtr = shared_ptr<class TrackNode>;
class TrackNode : public Obj
{
	friend class Track;

public:
	TrackNode(GameRoomID roomid) :Obj{ roomid } {}
protected:
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file) final;

public:
	virtual void update(milliseconds time_elapsed) override;

	bool is_joined(Obj& obj)
	{
		auto res = std::find(ALLOF(joined_objs_), &obj);
		return (res != joined_objs_.end());
	}
private:
	void process_detach(Obj& obj)
	{
		for (auto& next : next_nodes_)
		{
			if (next->collision_detect(obj))
			{
				detach_behave(obj);
				next->join_behave(obj);
				return;
			}
		}

		for (auto& prev : prev_nodes_)
		{
			if (prev->collision_detect(obj))
			{
				detach_behave(obj);
				prev->join_behave(obj);
				return;
			}
		}

		return detach_behave(obj, true);
	}
	void join_behave(Obj& obj, bool from_no_where = false);
	void detach_behave(Obj& obj, bool to_no_where = false);

public:
	glm::vec3 get_next_center()
	{
		glm::vec3 ret{ 0 };
		for (auto& next : next_nodes_)
		{
			ret += next->get_position();
		}
		ret /= next_nodes_.size();
		return ret;
	}
	glm::vec3 get_prev_center()
	{
		glm::vec3 ret{ 0 };
		for (auto& prev : prev_nodes_)
		{
			ret += prev->get_position();
		}
		ret /= prev_nodes_.size();
		return ret;
	}

public:
	void process_collide(); // 연결된 노드들 포함.

private:
	void process_collide(TrackNode* node);

protected:
	void add_prev(TrackNode* prev, bool joint_them = true);
	void add_next(TrackNode* next, bool joint_them = true);

public:
	GET_REF(prev_nodes);
	GET_REF(next_nodes);
	GET(id);
	GET(from_start);
	GET(front);
private:
	int id_;
	int from_start_{}; //=> 작을 수록 스타트라인에 가까운...
	vector<TrackNode*> prev_nodes_;
	vector<TrackNode*> next_nodes_;

private:
	glm::vec3 front_;
	vector<Obj*> joined_objs_; // prev, next 와 함께 충돌검사. 

	// 겹칠경우 진행방향 쪽 노드에 포함(next node 인 노드에 포함).
	// 0 => 진행방향 => 1. 높을 수록 진행방향 쪽.
	// 
	// 이어지는 노드들 선형보간 텍스쳐 다닥다닥 곡선 트랙 맹글기.
	using obj_func = std::function<void(Obj&)>;

public:
	void set_editional_update_func(obj_func func = [](Obj&) {});
private:
	// 스타트, 엔드, 중간노드들.
	obj_func editional_update_func_{ [](Obj&) {} };
};
//////////////////////////////////////////////
class GameRoom;

class Track : public IDataOnFile
{
	using outlanded_obj = pair<milliseconds, Obj*>;
	using outlanded_update_func = std::function<void(outlanded_obj&)>;
public:
	Track(GameRoomID);
public:
	GET(gameroomid);
	GameRoom& getGameRoom();
private:
	GameRoomID gameroomid_;
protected:
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file) final;

public:
	outlanded_update_func outlanded_update_func_{};

	void update_outlanded_obj(outlanded_obj& outlanded_obj)
	{
		outlanded_update_func_(outlanded_obj);
	}

	void update(milliseconds time_elapsed);

public:
	// return -1 on fail
	int check_include(Obj& obj)
	{
		for (auto& track : tracks_)
		{
			if (track->collision_detect(obj))
			{
				return track->id_;
			}
		}

		return -1;
	}

	void init_include_obj(Obj& obj, bool from_no_where)
	{
		auto res = check_include(obj);

		if (-1 == res)
		{
			cerr << "[!!!!!init_include_obj fail!!!!!!!!!!!!!]" << endl;
			//return;
			res = find_closest_track(obj);
		}

		tracks_[res]->join_behave(obj, from_no_where);
		//cerr << "included track ==[ " << res << " ]" << endl;
	}

	void include_obj(Obj& obj, bool from_no_where, glm::vec3 diff = glm::vec3{ 0 })
	{
		auto res = check_include(obj);

		if (-1 == res)
		{
			res = find_closest_track(obj);
		}

		auto move = tracks_[res]->get_position() - obj.get_position() + diff; move.y = 0;
		obj.move(move);
		tracks_[res]->join_behave(obj, from_no_where);
	}

	//return -1 on fail.
	int find_closest_track(Obj& obj)
	{
		pair<int, float> ret = make_pair(-1, -1.f);

		auto pos = obj.get_position();

		for (auto& track : tracks_)
		{
			auto len = glm::length(track->get_position() - pos);

			if (ret.second < 0 || len < ret.second)
			{
				ret = make_pair(track->id_, len);
			}
		}

		return ret.first;
	}

public:
	GET_REF(tracks);
	GET_REF_UNSAFE(objs_in_outland);
	GET_REF(items);
private:
	vector<TrackNodePtr> tracks_;
	vector<outlanded_obj> objs_in_outland_;
	vector<ItemObjPtr> items_;

private:
	void set_start_node(TrackNodePtr& newNode);
	void set_end_node(TrackNodePtr& newNode);
	void set_mid1_node(TrackNodePtr& newNode);
	void set_mid2_node(TrackNodePtr& newNode);

private:
	TrackNodePtr start_point_;
	TrackNodePtr mid_point1_;
	TrackNodePtr mid_point2_;
	TrackNodePtr end_point_;

public:
	SET(draw);
	GET(draw);

private:
	bool draw_ = false;
	bool draw_all_edges_ = true;
	bool draw_nearby_edges_ = true;
	bool draw_select_edges_ = true;
	bool wiremode_ = false;
	bool auto_regen_ = true;

};

///////////////////////////////////////////////////////////////
