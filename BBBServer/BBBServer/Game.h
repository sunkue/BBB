#pragma once
#include "timer.h"
#include "DynamicObj.h"
#include "Event.h"
#include "FileHelper.h"
#include "GameScene.h"

class Game
{
	DISABLE_COPY_MOVE(Game) = delete;
public:
	Game(GameRoomID roomid) :id{ roomid }
	{
		scene = make_unique<GameScene>();
		timer_system = make_unique<TimerSystem>();
		eventmanager = make_unique<EventManager>(roomid);
	}

	void init()
	{
		scene->init(id);
	}

	void StartGame() 
	{
		timer_system->start();
		scene->start();
		for (auto& item : scene->get_track()->get_items())
		{
			item->initseed(seed);
		}


		cout << "START GAME :: " << id << endl;
	};

	void NewPlayer(InGameID ingameid)
	{
		scene->getVehicle(ingameid)->set_enable(true);
	}
	void RemovePlayer(InGameID ingameid)
	{
		scene->getVehicle(ingameid)->set_enable(false);
	}
public:
	void update()
	{
		auto milli_tick = timer_system->tick_time();
		auto f_milli_tick = timer_system->tick_timef();
		eventmanager->update(milli_tick);
		scene->update(milli_tick);
		timer_system->tick();
	}
public:
	auto GetSeed() const { return seed; }
private:
	random_device::result_type seed{ rd() };
public:
	unique_ptr<GameScene> scene;
	unique_ptr<TimerSystem> timer_system;
	unique_ptr<EventManager> eventmanager;
	GameRoomID id;
};

