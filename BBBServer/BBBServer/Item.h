#pragma once
#include "Obj.h"

struct TimerEvent
{
	milliseconds begin_time;
	void* context;
};

#pragma warning(push)
#pragma warning(disable: 26812)
#pragma warning(disable: 26495)
BETTER_ENUM
(
	ItemType, int
	, rocket = 0
	, rocket3
	, swap
	, steal

	, shield
	, repair
	, boost
	, duple
);
#pragma warning(pop)

using ItemInstancePtr = shared_ptr<class ItemInstance>;
using ItemObjPtr = shared_ptr<class ItemObj>;

// 맵에 떠 있는 아이템.
class ItemObj : public Obj, public RandomEngine
{
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file);
public:
	CREATE_SHARED(ItemObj);
	ItemObj(GameRoomID roomid) :Obj{ roomid }
	{
		const_cast<Boundings&>(get_boundings()).load("boxbounding");
		randomize_type();
	}
	virtual ~ItemObj() {}

public:
	ItemInstancePtr to_instance();
	virtual void update(milliseconds time_elapsed) override;
	virtual void on_collide(Obj& other, OUT void* context = nullptr);
	void init();
	void initseed(random_device::result_type seed) 
	{ 
		RandomEngine_initseed(seed);
		randomize_type();
	}
public:
	GET(type);
	GET(included_node);
	SET(included_node);

private:
	int included_node_{ -1 };

private:
	void normalize_probability_table();
	void randomize_type();

private:
	ItemType type_{ ItemType::rocket };
	bool enable_ = true;
	milliseconds disabled_time_{};
	static constexpr milliseconds regen_period_{ 5000ms };
	array<float, ItemType::_size()> probability_table_{ 1 };
};

// 획득된 아이템 인스턴스.
class ItemInstance
{
public:
	CREATE_SHARED(ItemInstance);
	ItemInstance(ItemType type) : type_{ type } { }
	ItemObjPtr to_obj(GameRoomID roomid, glm::vec3 pos);
	GET(type);
private:
	ItemType type_;
};

// 아이템리스트 
// 렌더러에서는 그냥 array + size
class ItemInstanceList
{
public:
	static constexpr size_t maxSize = 3;
public:
	int TryPush(const ItemInstancePtr& item);
	optional<ItemInstancePtr> Pop();
	size_t size() const { return itemQ.size(); }
	FixedQueue<ItemInstancePtr, maxSize> itemQ;
	void UseDuple() { doubled = true; }
protected:
	bool doubled = false;
};
