#include "stdafx.h"
#include "Deco.h"
#include "GameScene.h"
#include "Game.h"
#include "Collision.h"

void Deco::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);

	int subObjCount = 0;
	LOAD_FILE(file, subObjCount);

	for (int i = 0; i < subObjCount; i++)
	{
		auto filename = "decos/subObj/deco0subobj" + to_string(i);
		SubObjs.push_back(make_shared<Obj>(get_gameroomid()));
		SubObjs.back()->boundings_.load("boxbounding");
		SubObjs.back()->load(filename);
	}
}
