#include "stdafx.h"
#include "GameSystem.h"
#include "GameRoom.h"
#include "Server.h"

int main()
{
	GameSystem::get(); // singleton init.

	ThreadRAII server_thread{ thread{[&]()
		{ Server::get().ProcessQueuedCompleteOperationLoopEx(); }}, ThreadRAII::DtorAction::join };

	// 여기서 postQueue 로 게임도 굴릴까? update().. 쓰레드 버그는 안생길 테지만,, 좋지 않은 방법이려나?
	
	GameManager::get().init();

	while (true)
	{
		GameManager::get().update();
		SleepEx(1, true);
	}
}