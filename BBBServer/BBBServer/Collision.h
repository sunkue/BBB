#pragma once

#include "FileHelper.h"



struct BoundingSphere
{
	glm::vec3 center{};
	float radius{ 1 };

	bool intersects(const struct BoundingSphere& sh) const;
	//	bool intersects(const struct BoundingBox& box) const;
	bool intersects(const struct BoundingFrustum& fr) const;
	bool intersects(const struct Ray& ray)const;
};

struct BoundingBox
{
	glm::vec3 center{};
	glm::vec3 extents{ 1 };
	glm::quat orientation{};

	//	bool intersects(const struct BoundingSphere& sh) const;
	bool intersects(const struct BoundingBox& box) const;
	bool intersects(const struct BoundingFrustum& fr) const;
private:
	bool getSeparatingPlane(const glm::vec3& RPos, const glm::vec3& Plane, const BoundingBox& box) const;
	glm::vec3 AxisX()const { return glm::rotate(orientation, X_DEFAULT); }
	glm::vec3 AxisY()const { return glm::rotate(orientation, Y_DEFAULT); }
	glm::vec3 AxisZ()const { return glm::rotate(orientation, Z_DEFAULT); }
};


struct BoundingFrustum
{
	glm::vec3 origin{};
	glm::quat orientation{};

	float right_slope;
	float left_slope;
	float top_slope;
	float bottom_slope;
	float n, f;

	bool intersects(const struct BoundingSphere& sh) const;
	bool intersects(const struct BoundingBox& box) const;
	bool intersects(const struct BoundingFrustum& fr) const;

	static void CreateFromMatrix(glm::mat4 projection);
};

///////////////////////////////////

class Boundings : public IDataOnFile
{
protected:
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file) final;

	BoundingSphere L1_origin;
	BoundingBox L2_origin;
public:
	// L1 should contain L2.
	BoundingSphere L1;
	BoundingBox L2;
	
	void rotate(const glm::quat& q) { L2.orientation *= q; }
	void move(const glm::vec3& dif) { L1.center += dif; L2.center += dif; }
	void scaling(const glm::vec3& ratio) { L2.extents *= ratio; L1.radius *= (compMax(ratio) > 1.f) ? glm::compMax(ratio) : glm::compMin(ratio); }

	bool intersects(const Boundings& other) const
	{
		if (L1.intersects(other.L1))
		{
			return L2.intersects(other.L2);
		}

		return false;
	}
};
