#include "stdafx.h"
#include "DynamicObj.h"

void DynamicObj::update_movement(float time_elapsed)
{
	if (get_collided())
	{
		auto others_pos = get_force();
		auto diff = others_pos - get_position();
		diff.y = 0;
		int backed = 0;
		auto speed = glm::length(linear_speed_);

		constexpr float e = 0.5f;

		linear_speed_ -= ((1 + e) / 2) * (linear_speed_ - get_force2());
		move(linear_speed_ * time_elapsed);

		set_force(glm::vec3{});
		set_force2(glm::vec3{});
		set_collided(false);
	}
	else
		move(linear_speed_ * time_elapsed);
}
