#include "stdafx.h"
#include "ItemEvent.h"

///////////////////////////////////////////////////////



///////////////////////////////////////////////////////

EventPtr EventFactory::createEvent(const ItemInstancePtr& instance,GameRoomID roomId, InGameID user, InGameID target)
{
	//cout << instance->get_type()._to_string() << endl;
	switch (instance->get_type())
	{
	case ItemType::rocket:
	{
		return ::move(make_shared<ItemEvent>(roomId));
	}
	CASE ItemType::rocket3 :
	{
		return ::move(make_shared<ItemEvent>(roomId));
	}
	CASE ItemType::swap :
	{
		return ::move(make_shared<ItemEvent>(roomId));
	}
	CASE ItemType::steal :
	{
		return ::move(make_shared<ItemEvent>(roomId));
	}
	CASE ItemType::shield :
	{
		return ::move(make_shared<SheildItemEvent>(roomId, user));
	}
	CASE ItemType::repair :
	{
		return ::move(make_shared<ItemEvent>(roomId));
	}
	CASE ItemType::boost :
	{
		return ::move(make_shared<BoostItemEvent>(roomId, user));
	}
	CASE ItemType::duple :
	{
		return ::move(make_shared<DupleItemEvent>(roomId, user));
	}
	break; default: cerr << "err! ItemType" << endl; break;
	}
}
