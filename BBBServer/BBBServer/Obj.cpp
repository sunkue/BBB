#include "stdafx.h"
#include "Obj.h"
#include "timer.h"
#include "GameRoom.h"

void Obj::load_file_impl(ifstream& file)
{
	LOAD_FILE(file, translate_);
	LOAD_FILE(file, quaternion_);
	LOAD_FILE(file, scale_);
	LOAD_FILE(file, objid_);

	boundings_.move(translate_);
	boundings_.rotate(quaternion_);
	boundings_.scaling(scale_);
}

GameRoom& Obj::getGameRoom()
{
	return GameManager::get().getGameRoom(gameroomid_);
}



bool Obj::collision_detect(const Obj& other) const
{
	if (!enable_)return false;
	if (!other.get_enable())return false;
	if (&other == this)return false;
	bool isCollide = get_boundings().intersects(other.get_boundings());
	return isCollide;
}

