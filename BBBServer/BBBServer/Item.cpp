#include "stdafx.h"
#include "Item.h"
#include "TrackNode.h"
#include "Vehicle.h"
#include "GameRoom.h"

///////////////////////////////////////////////////

void ItemObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
	LOAD_FILE(file, included_node_);
	LOAD_FILE(file, probability_table_);
	randomize_type();
}

///////////////////////////////////////////////////

void ItemObj::init()
{
	auto& track = getGameRoom().game->scene->get_track();
	track->init_include_obj(*this, true);
	normalize_probability_table();
	randomize_type();
}

void ItemObj::normalize_probability_table()
{
	// normalize table.
	float length = 0;

	for (auto& probability : probability_table_)
	{
		length += probability;
	}

	for (auto& probability : probability_table_)
	{
		probability /= length;
	}
}

void ItemObj::randomize_type()
{
	auto randomValue = SUNKUE::GetRandomValueZeroToOne(random);
	for (int i = 0; i < probability_table_.size(); i++)
	{
		if (randomValue <= probability_table_[i])
		{
			type_ = ItemType::_from_integral(i);
			break;
		}
		randomValue -= probability_table_[i];
	}
}

ItemInstancePtr ItemObj::to_instance()
{
	return ItemInstance::create(type_);
}

void ItemObj::update(milliseconds time_elapsed)
{
	// re able.
	if (false == enable_)
	{
		disabled_time_ += time_elapsed;
		if (regen_period_ < disabled_time_)
		{
			enable_ = true;
			disabled_time_ = 0ms;
			randomize_type();
		}
	}
}

void ItemObj::on_collide(Obj& other, OUT void* context)
{
	if (!enable_) return;
	// vehicle
	if (auto vehicle = dynamic_cast<VehicleObj*>(&other))
	{
		bool acquired = vehicle->TryAcquireItem(*this);
		enable_ = !acquired;
	}
}

///////////////////////////////////////////////////

ItemObjPtr ItemInstance::to_obj(GameRoomID roomid, glm::vec3 pos)
{
	auto ret = ItemObj::create(roomid);
	ret->move(pos);
	return ret;
}

///////////////////////////////////////////////////

int ItemInstanceList::TryPush(const ItemInstancePtr& item)
{
	int ret = 0;
	if (doubled)
	{
		cerr << "doubled" << endl;
		doubled = false;
		cout << itemQ.size() << endl;
		ret += itemQ.try_push((ItemInstance::create(item->get_type())));
		cout << itemQ.size() << endl;
	}
	ret += itemQ.try_push(item);
	return ret;
}

optional<ItemInstancePtr> ItemInstanceList::Pop()
{
	if (itemQ.empty())
		return nullopt;

	auto ret = itemQ.front(); itemQ.pop();
	return ret;
}
