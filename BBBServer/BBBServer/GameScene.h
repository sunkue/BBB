#pragma once

#include "DynamicObj.h"
#include "TrackNode.h"
#include "Vehicle.h"
#include "Deco.h"

//////////////////////////////////////////////////////

class GameScene
{
public:
	GameScene() { };
public:
	virtual void update(milliseconds elapsed) final;
public:
	void init(GameRoomID roomid);
	void start();
public:
	GET_REF_UNSAFE(cars);
	GET_REF_UNSAFE(track);
	GET_REF_UNSAFE(deco);
private:
	void update_ranks();
public:
	auto& getVehicle(InGameID ingameid) { return cars_[ingameid]; }
private:
	array<VehiclePtr, MAX_VEHICLE> cars_;
	unique_ptr<Track> track_;
	unique_ptr<Deco> deco_;
};