#include "stdafx.h"
#include "TrackNode.h"
#include "Vehicle.h"
#include "timer.h"
#include "Obj.h"
#include "GameRoom.h"

//////////////////////

void TrackNode::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
	LOAD_FILE(file, id_);
	LOAD_FILE(file, from_start_);
	LOAD_FILE(file, front_);
}

void TrackNode::update(milliseconds time_elapsed)
{
	for (const auto obj : joined_objs_)
	{
		bool int_nextnode = false;
		for (auto& next : next_nodes_)
		{
			int_nextnode += next->collision_detect(*obj);
		}

		// detach
		if (!collision_detect(*obj) || int_nextnode)
		{
			process_detach(*obj);
		}

		editional_update_func_(*obj);

		// dir
		if (auto vehicle = dynamic_cast<VehicleObj* const>(obj))
		{
			auto head = vehicle->get_head_dir();
			auto drive_good_dir = glm::dot(head, front_) >= 0 ? true : false;
			if (drive_good_dir)
			{
				//cout << "good dir" << endl;
			}
			else
			{
				//cout << "bad dir" << endl;
				// 일정시간 지나면 자동복귀..? 1초? 3초?
			}
		}
	}
}

void TrackNode::join_behave(Obj& obj, bool from_no_where)
{
	if (from_no_where)
	{
		auto& track = getGameRoom().game->scene->get_track();
		auto& outed_objs = track->get_objs_in_outland();
		std::erase_if(outed_objs, [&](auto& x) { return x.second == &obj; });
	}
	if (VehicleObj* car = dynamic_cast<VehicleObj*>(&obj))
	{
		car->set_included_node(id_);
	}
	if (ItemObj* item = dynamic_cast<ItemObj*>(&obj))
	{
		item->set_included_node(id_);
	}

	joined_objs_.push_back(&obj);
}

void TrackNode::detach_behave(Obj& obj, bool to_no_where)
{
	if (to_no_where)
	{
		auto& track = getGameRoom().game->scene->get_track();
		auto& timer = getGameRoom().game->timer_system;
		auto& outed_objlist = track->get_objs_in_outland();
		if (outed_objlist.end() == std::find_if(ALLOF(outed_objlist), [&](auto& x) { return x.second == &obj; }))
		{
			track->get_objs_in_outland().emplace_back(timer->game_time(), &obj);
		}
	}
	else
	{
		// cerr << "to nearnode" << endl;
	}

	joined_objs_.erase(std::remove(joined_objs_.begin(), joined_objs_.end(), &obj), joined_objs_.end());
}

void TrackNode::process_collide()
{
	process_collide(this);

	for (auto& prev : prev_nodes_)
	{
		process_collide(prev);
	}

	for (auto& next : next_nodes_)
	{
		process_collide(next);
	}
}

void TrackNode::process_collide(TrackNode* node)
{
	for (const auto& obj : joined_objs_)
	{
		for (const auto& other : node->joined_objs_)
		{
			if (obj == other)
			{
				continue;
			}

			obj->collision_detect(*other); // instersect로 검사 후 액션.
		}
	}
}

void TrackNode::set_editional_update_func(obj_func func)
{
	editional_update_func_ = func; // throw error here;
};

void TrackNode::add_prev(TrackNode* prev, bool joint_them)
{
	if (nullptr == prev || this == prev) return;

	prev_nodes_.emplace_back(prev);
	if (joint_them)
	{
		prev->add_next(this, false);
	}
}

void TrackNode::add_next(TrackNode* next, bool joint_them)
{
	if (nullptr == next || this == next) return;

	next_nodes_.emplace_back(next);
	if (joint_them)
	{
		next->add_prev(this, false);
	}
}


////////////////////////////////////////////////

GameRoom& Track::getGameRoom()
{
	return GameManager::get().getGameRoom(gameroomid_);
}


Track::Track(GameRoomID id) :gameroomid_{ id }
{
	load("track");

	outlanded_update_func_ =
		[this](outlanded_obj& outlanded_obj)
	{
		auto& timer = getGameRoom().game->timer_system;

		const auto& game_tp = timer->game_time();
		auto& outed_tp = outlanded_obj.first;
		auto& obj = *outlanded_obj.second;
		auto outed_elapsed = game_tp - outed_tp;
		if (1000ms < outed_elapsed)
		{
			if (auto car = dynamic_cast<VehicleObj*>(&obj))
			{
				car->set_outed(true);
			}
		}
		if (3000ms < outed_elapsed)
		{
			if (auto car = dynamic_cast<VehicleObj*>(&obj))
			{
				car->regenerate();
			}
		}
	};


}

void Track::load_file_impl(ifstream& file)
{
	// node
	int track_nums; LOAD_FILE(file, track_nums);
	tracks_.clear();
	tracks_.reserve(track_nums);
	for (int id = 0; id < track_nums; id++)
	{
		const string FILE_HEADER = "tracknode";
		string file_name = FILE_HEADER;
		file_name.append(std::to_string(id));

		tracks_.emplace_back(make_shared<TrackNode>(gameroomid_));
		tracks_.back()->load(file_name);
	}

	// edge
	for (const auto& node : tracks_)
	{
		vector<int> prev_ids; LOAD_FILE(file, prev_ids);
		for (const auto ids : prev_ids)
		{
			node->add_prev(tracks_[ids].get(), false);
		}

		vector<int> next_ids; LOAD_FILE(file, next_ids);
		for (const auto ids : next_ids)
		{
			node->add_next(tracks_[ids].get(), false);
		}
	}

	int cp_s, cp_e, cp_m1, cp_m2;
	LOAD_FILE(file, cp_s);
	LOAD_FILE(file, cp_e);
	LOAD_FILE(file, cp_m1);
	LOAD_FILE(file, cp_m2);

	set_start_node(tracks_[cp_s]);
	set_end_node(tracks_[cp_e]);
	set_mid1_node(tracks_[cp_m1]);  // 변경
	set_mid2_node(tracks_[cp_m2]);  // 변경


	// items
	int item_nums; LOAD_FILE(file, item_nums);
	items_.clear();
	items_.reserve(item_nums);
	for (int id = 0; id < item_nums; id++)
	{
		const string FILE_HEADER = "item";
		string file_name = FILE_HEADER;
		file_name.append(std::to_string(id));

		items_.emplace_back(ItemObj::create(gameroomid_));
		items_.back()->load(file_name);
	}
}

void Track::update(milliseconds time_elapsed)
{
	for (auto& node : tracks_)
	{
		node->update(time_elapsed);
	}

	for (auto& item : items_)
	{
		item->update(time_elapsed);
	}

	for (auto& outlanded_obj : objs_in_outland_)
	{
		auto& outed_tp = outlanded_obj.first;
		auto& obj = *outlanded_obj.second;

		auto res = check_include(obj);

		//cerr << objs_in_outland_.size() << endl;

		if (-1 == res)
		{
			if (dynamic_cast<AutoVehicleObj*>(&obj))
			{
				//	continue;
			}

			// 아웃랜드 오브제 업데이트 함수 돌리기
			if (auto_regen_)
			{
				update_outlanded_obj(outlanded_obj);
			}
		}
		else
		{
			// 다른 노드에 삽입,
			tracks_[res]->join_behave(obj, true);
		}
	}

	for (auto& car : getGameRoom().game->scene->get_cars())
	{
		for (auto& item : items_)
		{
			if (item->collision_detect(*car))
			{
				item->on_collide(*car);
			}
		}
	}

	for (auto& car : getGameRoom().game->scene->get_cars())
	{
		auto& deco = getGameRoom().game->scene->get_deco();

		for (auto& o : deco->SubObjs)
		{
			if (car->collision_detect(*o))
			{
				car->on_collide(*o);
			}
		}
	}
}

void Track::set_start_node(TrackNodePtr& newNode)
{
	if (start_point_)
	{
		start_point_->set_editional_update_func();
	}
	newNode->set_editional_update_func(
		[](Obj& obj)
		{
		});
	start_point_ = newNode;
}

void Track::set_end_node(TrackNodePtr& newNode)
{
	if (end_point_)
	{
		end_point_->set_editional_update_func();
	}
	newNode->set_editional_update_func(
		[](Obj& obj)
		{
			if (auto car = dynamic_cast<VehicleObj*>(&obj))
			{
				auto prev_cp = car->get_check_point();
				auto pre_target = VehicleObj::CHECK_POINT::check2;

				if (pre_target == prev_cp)
				{
					car->clear_lab();
				}
				car->set_check_point(VehicleObj::CHECK_POINT::begin);
			}
		});
	end_point_ = newNode;
}

void Track::set_mid1_node(TrackNodePtr& newNode)
{
	if (mid_point1_)
	{
		mid_point1_->set_editional_update_func();
	}
	newNode->set_editional_update_func(
		[](Obj& obj)
		{
			if (auto car = dynamic_cast<VehicleObj*>(&obj))
			{
				car->set_check_point(VehicleObj::CHECK_POINT::check1);
			}
		});
	mid_point1_ = newNode;
}

void Track::set_mid2_node(TrackNodePtr& newNode)
{
	if (mid_point2_)
	{
		mid_point2_->set_editional_update_func();
	}
	newNode->set_editional_update_func(
		[](Obj& obj)
		{
			if (auto car = dynamic_cast<VehicleObj*>(&obj))
			{
				car->set_check_point(VehicleObj::CHECK_POINT::check2);
			}
		});
	mid_point2_ = newNode;
}

