#pragma once
#include "Event.h"
#include "Item.h"
#include "GameRoom.h"

///////////////////////////////////////////////////////

class ItemEvent : public Event
{
public:
	//ItemEvent(itType before_this_it, milliseconds action_time, Obj& target)
	//	:Event{ before_this_it }, action_time{ action_time }, target{ target } {};

	ItemEvent(GameRoomID roomid) : Event{ roomid }, action_time{ 2000ms } {};
	~ItemEvent() {};

private:
	virtual void Start() override
	{
	};
	virtual void Update(milliseconds time) override
	{
		auto remain_time = action_time - time;

		//TODO
	//	cout << remain_time << "  " << action_time << endl;

		action_time = remain_time;

		if (action_time <= 0ms)
			Event::done();
	};
	virtual void Detroy() override
	{
	};
	milliseconds action_time;
	//Obj& target;
};

///////////////////////////////////////////////////////

class DupleItemEvent : public ItemEvent
{
public:
	DupleItemEvent(GameRoomID roomid, InGameID userId) :ItemEvent{ roomid }, userId{ userId } {};
private:
	virtual void Update(milliseconds elapsed) override
	{
		auto& user = GameManager::get().getGameRoom(get_roomid()).game->scene->getVehicle(userId);
		user->use_duple();
		Event::done();
	};
	InGameID userId;
};

class SheildItemEvent : public ItemEvent
{
public:
	SheildItemEvent(GameRoomID roomid, InGameID userId) :ItemEvent{ roomid }, userId{ userId } {};
private:
	virtual void Update(milliseconds elapsed) override
	{
		auto& user = GameManager::get().getGameRoom(get_roomid()).game->scene->getVehicle(userId);
		action_time -= elapsed;
		user->set_shield(true);
		if (action_time <= 0ms)
			Event::done();
	};
	virtual void Detroy() override
	{
		auto& user = GameManager::get().getGameRoom(get_roomid()).game->scene->getVehicle(userId);
		user->set_shield(false);
	};
	milliseconds action_time{ 3000ms };
	InGameID userId;
};


class BoostItemEvent : public ItemEvent
{
public:
	BoostItemEvent(GameRoomID roomid, InGameID userId) :ItemEvent{ roomid }, userId{ userId } {};
private:
	virtual void Update(milliseconds elapsed) override
	{
		auto& user = GameManager::get().getGameRoom(get_roomid()).game->scene->getVehicle(userId);
		action_time -= elapsed;
		user->set_boost(true);
		if (action_time <= 0ms)
			Event::done();
	};
	virtual void Detroy() override
	{
		auto& user = GameManager::get().getGameRoom(get_roomid()).game->scene->getVehicle(userId);
		user->set_boost(false);
	};
	milliseconds action_time{ 3000ms };
	InGameID userId;
};

///////////////////////////////////////////////////////

class EventFactory
{
public:
	static EventPtr createEvent(const ItemInstancePtr& instance, GameRoomID roomId, InGameID user, InGameID target);
};