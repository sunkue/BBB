#pragma once

#include "FileHelper.h"

class GameSystem : IDataOnFile
{
	SINGLE_TON(GameSystem)
	{
		load("[]GAME");
	}
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file)
	{
		LOAD_FILE(file, version_);
		cerr << "[GameVersion]::" << version_ << endl;
	}
public:
	GET(version);
private:
	size_t version_;
};

