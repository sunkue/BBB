#pragma once
#include "ClientSession.h"
#include "ListenSocket.h"

class Server
{
	SINGLE_TON(Server);
	~Server();
public:
	void ProcessQueuedCompleteOperationLoop();
	void ProcessQueuedCompleteOperationLoopEx();
	void RepeatSendLoop(milliseconds repeat_time = 50ms);
private:
	void StartAccept();
private:
	void OnRecvComplete(ClientID id, DWORD transfered);
	void OnSendComplete(ClientID id, ExpOverlapped* exover);
	void OnAcceptComplete(ExpOverlapped* exover);
	void OnDisconnectComplete(ClientID id, ExpOverlapped* exover);
public:
	void process_packet(ClientID id, const void* const packet);
private:
	ClientID get_free_id();
public:
	auto get_iocp() { return iocp; };
	auto& get_clients() { return clients; }
private:
	HANDLE iocp;
	array<ClientSession, MAX_CLIENT> clients;
};



