#include "stdafx.h"
#include "GameRoom.h"
#include "Server.h"
#include "ClientSession.h"


void GameRoom::synchronize_clients()
{
	auto SND2ME = [this](Player& me, const void* const packet)
	{
		Server::get().get_clients()[me.netid].do_send(packet);
	};


	auto SND2EVERYOTHERPLAYERS = [this](Player& except, const void* const packet)
	{
		for (auto& p : players)
		{
			if (p.IsFree())
				continue;

			if (p.netid == except.netid)
				continue;

			Server::get().get_clients()[p.netid].do_send(packet);
		}
	};

	auto SND2EVERYPLAYERS = [this](const void* const packet)
	{
		int i = 0;
		for (auto& p : players)
		{
			if (p.IsFree())
				continue;

			Server::get().get_clients()[p.netid].do_send(packet);
		}
	};

	// 이전프레임과 비교해서 변경된 부분들 체크해서 전송.

	auto elapsed = game->timer_system->tick_time();

	for (auto& p : players)
	{
		if (!p.IsRunning()) // running?
			continue;

		auto ingameid = p.ingameid;
		auto& vehicle = game->scene->getVehicle(ingameid);

		//
		{
			sc_set_player_info info;
			info.timestamp = p.timestamp;
			info.ingameid = ingameid;
			info.rank = vehicle->get_rank();
			info.pos = decltype(info.pos)::encode(vehicle->get_position());
			info.rotate = decltype(info.rotate)::encode(vehicle->get_rotation());
			info.linear_speed = decltype(info.linear_speed)::encode(vehicle->get_linear_speed());
			SND2EVERYPLAYERS(&info);
		}


		// item.. accquire first. (track -> vehicle)
		{
			auto& accquired_list = vehicle->get_accquired_this_frame_item_log();

			for (auto accquired : accquired_list)
			{
				sc_player_accquire_item accquireitem;
				accquireitem.ingameid = ingameid;
				accquireitem.itemtype = static_cast<decltype(accquireitem.itemtype)>(accquired.first);
				accquireitem.itemobjid = accquired.second;
				SND2EVERYPLAYERS(&accquireitem);
			}
		}

		// item.. use after accquire. (track -> vehicle)
		{
			auto& used_list = vehicle->get_used_this_frame_item_log();

			for (auto used : used_list)
			{
				sc_player_use_item useitem;
				useitem.ingameid = ingameid;
				useitem.itemtype = static_cast<decltype(useitem.itemtype)>(used);
				SND2EVERYPLAYERS(&useitem);
			}

		}


		// outed & regen & collide & change lab
		{
			if (vehicle->get_outed())
			{
				sc_outed outed;
				outed.ingameid = ingameid;
				SND2EVERYPLAYERS(&outed);
				vehicle->set_outed(false);
			}

			if (vehicle->get_regened())
			{
				sc_regen regen;
				regen.ingameid = ingameid;
				regen.includedtracknode = vehicle->get_included_node();
				SND2EVERYPLAYERS(&regen);
				vehicle->set_regened(false);
			}

			if (vehicle->get_cleared())
			{
				sc_clearlab clearlab;
				clearlab.ingameid = ingameid;
				clearlab.clearAt = game->timer_system->game_time();
				SND2EVERYPLAYERS(&clearlab);
				vehicle->set_cleared(false);
				vehicle->cleartime = clearlab.clearAt;
				if (!ReadyToEnd && MAX_LAB <= vehicle->get_lab())
				{
					ReadyToEnd = true;
					sc_gameend gameend;
					gameend.endtime = clk::now() + 10s;
					SND2EVERYPLAYERS(&gameend);
					game->eventmanager->Add(make_shared<GameClearEvent>(game->id, 10s));
				}
			}
		}
	}

	for (int ingameid = MAX_PLAYER; ingameid < game->scene->get_cars().size(); ingameid++)
	{
		auto& vehicle = game->scene->getVehicle(ingameid);

		{
			sc_set_player_info info;
			info.timestamp = steady_clock::now();
			info.ingameid = ingameid;
			info.rank = vehicle->get_rank();
			info.pos = decltype(info.pos)::encode(vehicle->get_position());
			info.rotate = decltype(info.rotate)::encode(vehicle->get_rotation());
			info.linear_speed = decltype(info.linear_speed)::encode(vehicle->get_linear_speed());
			SND2EVERYPLAYERS(&info);
		}


		// item.. accquire first. (track -> vehicle)
		{
			auto& accquired_list = vehicle->get_accquired_this_frame_item_log();

			for (auto accquired : accquired_list)
			{
				sc_player_accquire_item accquireitem;
				accquireitem.ingameid = ingameid;
				accquireitem.itemtype = static_cast<decltype(accquireitem.itemtype)>(accquired.first);
				accquireitem.itemobjid = accquired.second;
				SND2EVERYPLAYERS(&accquireitem);
			}
		}

		// item.. use after accquire. (track -> vehicle)
		{
			auto& used_list = vehicle->get_used_this_frame_item_log();

			for (auto used : used_list)
			{
				sc_player_use_item useitem;
				useitem.ingameid = ingameid;
				useitem.itemtype = static_cast<decltype(useitem.itemtype)>(used);
				SND2EVERYPLAYERS(&useitem);
			}

		}


		// outed & regen & collide & change lab
		{
			if (vehicle->get_outed())
			{
				sc_outed outed;
				outed.ingameid = ingameid;
				SND2EVERYPLAYERS(&outed);
				vehicle->set_outed(false);
			}

			if (vehicle->get_regened())
			{
				sc_regen regen;
				regen.ingameid = ingameid;
				regen.includedtracknode = vehicle->get_included_node();
				SND2EVERYPLAYERS(&regen);
				vehicle->set_regened(false);
			}

			if (vehicle->get_cleared())
			{
				sc_clearlab clearlab;
				clearlab.ingameid = ingameid;
				clearlab.clearAt = game->timer_system->game_time();
				SND2EVERYPLAYERS(&clearlab);
				vehicle->set_cleared(false);
				vehicle->cleartime = clearlab.clearAt;
				if (!ReadyToEnd && MAX_LAB <= vehicle->get_lab())
				{
					ReadyToEnd = true;
					sc_gameend gameend;
					gameend.endtime = clk::now() + 10s;
					SND2EVERYPLAYERS(&gameend);
					game->eventmanager->Add(make_shared<GameClearEvent>(game->id, 10s));
				}
			}
		}
	}

	servertime_interval_ += elapsed;
	if (100ms < servertime_interval_)
	{
		servertime_interval_ = 0ms;
		sc_servertime servertime;
		servertime.server_time = clk::now();
		SND2EVERYPLAYERS(&servertime);
	}
}

void GameRoom::StartGame(clk::time_point starttime)
{
	while (clk::now() <= starttime)
		;;;

	state = State::RUNNING;

	auto& clients = Server::get().get_clients();

	for (auto& p : players)
	{
		if (p.IsFree())
			continue;

		clients[p.netid].SetState(SESSION_STATE::INGAME);
		p.state = Player::State::RUNNING;
	}

	ReadyToEnd = false;
	game->StartGame();
	cout << "GameStart" << endl;
	//		sp = Timer::now();
}

InGameID GameRoom::findIngameId(ClientID id) const
{
	for (InGameID i = 0; i < players.size(); i++)
	{
		if (players[i].netid == id)
			return i;
	}

	//optinal?
	assert();
}

InGameID GameRoom::AddNewPlayerAsIncharge(ClientID id)
{
	for (InGameID i = 0; i < players.size(); i++)
	{
		if (players[i].IsFree())
		{
			players[i].state = Player::State::INCHARGE;
			players[i].netid = id;
			players[i].ingameid = i;
			game->NewPlayer(players[i].ingameid);

			return i;
		}
	}

	cerr << "GameRoom::AddNewPlayerAsIncharge" << id << endl;
	exit(-1);
	return -1; // 여기 안 오도록 맹글자
}

void GameRoom::RemovePlayer(InGameID id)
{
	//if (&MatchingRoom::get().getReservedRoom() == this)
	if (state == State::RESERVED) { MatchingRoom::get().disCountOnce(); }
	game->RemovePlayer(id);
	players[id].state = Player::State::FREE;
}
