#include "stdafx.h"
#include "Event.h"
#include "GameRoom.h"

///////////////////////////////////////////////////////



void EventManager::update(milliseconds time)
{
	int i = 0;
	for (auto it = EventList.begin(), before_it = EventList.before_begin(); it != EventList.end();)
	{
		i++;
		if (!(*it)->enable)
		{
			(*it)->Detroy();
			it = EventList.erase_after(before_it);
			continue;
		}

		it->get()->Update(time);
		before_it = it; it++;
	}
}

void EventManager::Add(EventPtr e)
{
	EventList.emplace_front(::move(e))->Start();
}

///////////////////////////////////////////////////////

void GameClearEvent::Start()
{

}

void GameClearEvent::Update(milliseconds elapsed)
{
	clearAt -= elapsed;
	if (clearAt <= 0ms)
		Event::done();
}

void GameClearEvent::Detroy()
{
	GameManager::get().getGameRoom(get_roomid()).state = GameRoom::State::ENDING;
}
