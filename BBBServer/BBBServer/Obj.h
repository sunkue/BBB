#pragma once

#include "Collision.h"

/////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////
class Obj;
class Camera;

__interface IObj
{
	void update(milliseconds time_elapsed);
	void on_collide(Obj& other, OUT void* context = nullptr); // 자기 자신의 변화만 적용.
	void on_uncollide(Obj& other, OUT void* context = nullptr);
};

class GameRoom;

/* 그려지는 모든 물체 */
class Obj : public IObj, public IDataOnFile
{
	friend class Deco;
protected:
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file);
public:
	explicit Obj(GameRoomID roomid) :gameroomid_{ roomid } {}
	GET(gameroomid);
	GameRoom& getGameRoom();
	GET(objid);
private:
	GameRoomID gameroomid_;
	ObjID objid_{ -1 };
public:
	virtual ~Obj() {}

	glm::mat4 model_mat()const { return glm::translate(translate_) * glm::toMat4(quaternion_) * glm::scale(scale_); }

	// 바운딩은 변환하지 않으므로 신중히 사용 할 것.
	//void set_position_nobounding(glm::vec3 pos) { translate_ = pos; }
	//void set_rotation_nobounding(glm::quat q) { quaternion_ = q; }
	//void set_scale_nobounding(glm::vec3 sale) { scale_ = sale; }

	glm::vec3 get_position()const { return translate_; }
	glm::quat get_rotation()const { return quaternion_; }
	glm::vec3 get_scale()const { return scale_; }

	glm::vec3 get_head_dir()const { return get_rotation() * HEADING_DEFAULT; }
	glm::vec3 get_up_dir()const { return get_rotation() * UP_DEFAULT; }
	glm::vec3 get_right_dir()const { return get_rotation() * RIGHT_DEFAULT; }

	void rotate(const glm::quat& q) { quaternion_ *= q; boundings_.rotate(q); }
	void move(const glm::vec3& dif) { translate_ += dif; boundings_.move(dif); }
	void scaling(const glm::vec3& ratio) { scale_ *= ratio; boundings_.scaling(ratio); }
public:
	virtual void update(milliseconds time_elapsed) {}
	bool collision_detect(const Obj& other)const;
	virtual void on_collide(Obj& other, OUT void* context = nullptr) {};
	virtual void on_uncollide(Obj& other, OUT void* context = nullptr) {};

	GET(force);
	SET(force);
	GET(force2);
	SET(force2);
	GET(collided);
	SET(collided);
private:
	glm::vec3 translate_;
	glm::quat quaternion_{};
	glm::vec3 scale_{ V3_DEFAULT };
	glm::vec3 force_{};
	glm::vec3 force2_{};
	bool collided_{ false }; 
public:
	GET_REF(boundings);
private:
	Boundings boundings_;
public:
	GET(enable);
	SET(enable);
private:
	bool enable_ = true;
};
using ObjPtr = shared_ptr<Obj>;

///////////////////////////////////////////

