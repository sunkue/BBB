#pragma once

/*
* gameroom 클래스는 네트워킹을 고려한 클래스.
* game 클래스는 네크워킹을 고려하지 않는 클래스.
*/

class Player
{
	friend class GameRoom;
	DISABLE_COPY_MOVE(Player) = default;
public:
	enum class State
	{
		FREE
		, INCHARGE // nonready
		, MATCHREADY
		, READY2RUN
		, RUNNING
	} state{ State::FREE };
	bool IsFree()const { return  State::FREE == state; }
	bool IsRunning()const { return  State::RUNNING == state; }

public:
	clk::time_point timestamp{};
	ClientID netid{ -1 };
	InGameID ingameid{ -1 };
};


#include "Game.h"

class GameRoom
{
	friend class GameManager;
	friend class MatchingRoom;
	DISABLE_COPY_MOVE(GameRoom) = delete;
public:
	GameRoom(GameRoomID id)
	{
		game = make_unique<Game>(id);
	}
public:
	void init() { game->init(); }
public:
	enum class State
	{
		FREE
		, RESERVED
		, READY2RUN
		, RUNNING
		, ENDING
		, END
	};
	atomic<State> state{ State::FREE };
	bool IsFree() const { return  State::FREE == state; }
	bool IsRunning() const { return  State::RUNNING == state; }
	bool IsReady2Run() const { return  State::READY2RUN == state; }
	bool IsOnMatching()const { return !IsFree() && !IsRunning() && !IsReady2Run(); }
	void StartGame(clk::time_point starttime);
	void CheckEmptyRoom() 
	{
		for (auto& p : players)
		{
			if (!p.IsFree())
				return;
		}

		state = State::FREE;
	}
public:
	void update()
	{

		if (!IsRunning())
			return;

		lock_guard lock{ gamelock };
		game->update();
		synchronize_clients();
	};
	void synchronize_clients();
protected:
	InGameID findIngameId(ClientID id) const;
	InGameID AddNewPlayerAsIncharge(ClientID id);
	void RemovePlayer(InGameID id);
public:
	auto& GetPlayers() { return players; }
private:
	array<Player, MAX_PLAYER> players{};
	milliseconds servertime_interval_{};
	bool ReadyToEnd{};
public:
	unique_ptr<Game> game;
	mutex gamelock;
};

/// ////////////////////////////////////////////////////

class GameManager
{
	SINGLE_TON(GameManager)
	{
		for (GameRoomID id = 0; id < rooms.size(); id++)
		{
			rooms[id] = make_unique<GameRoom>(id);
		}
	}
public:
	void init()
	{
		for (auto& r : rooms)
		{
			r->init();
		}
	}
public:
	void update()
	{
		for (auto& r : rooms)
		{
			r->update();
		}
	}
	GameRoom& getGameRoom(GameRoomID idx)
	{
		return *rooms[idx].get();
	}

	GameRoomID getFreeGameRoomId() const
	{
		static GameRoomID roomid = 0;
		roomid = roomid == rooms.size() ? 0 : roomid + 1;
		return roomid;

		for (GameRoomID i = 0; i < rooms.size(); i++)
		{
			if (rooms[i]->IsFree())
				return i;
		}

		cout << "NoFREE ROOM !!!" << endl;
	}
	void RemovePlayer(ClientID id)
	{
		auto& room = rooms[clinetRoomTable[id]];
		room->RemovePlayer(room->findIngameId(id));
	}
	GameRoomID findGameRoomId(ClientID id) const { return clinetRoomTable.at(id); }
	InGameID findIngameId(const GameRoom& room, ClientID id) const { return room.findIngameId(id); }
	void addPlayerInGameRoom(ClientID id, GameRoomID roomid) { clinetRoomTable[id] = roomid; }
private:
	array<unique_ptr<GameRoom>, MAX_ROOM> rooms;
	map<ClientID, GameRoomID> clinetRoomTable{};
};

/// ////////////////////////////////////////////////////

class MatchingRoom
{
	SINGLE_TON(MatchingRoom) { ReserveFreeRoom(); };
public:
	void ReserveFreeRoom() //1
	{
		cerr << "[ReserveRoom]" << reserved_room_id << endl;
		reserved_room_id = GameManager::get().getFreeGameRoomId();
		getReservedRoom().state = GameRoom::State::RESERVED;
	}
	ClientID AddNewMatchingPlayer(ClientID id) //2
	{
		matchingPlayerCount++;
		cerr << "[addNewPLayer]" << matchingPlayerCount << endl;
		ClientID ret = getReservedRoom().AddNewPlayerAsIncharge(id);

		if (-1 == ret)
		{
			cerr << "Couldnt Be here" << endl;
		}

		cerr << "[addNewPLayer]" << matchingPlayerCount << endl;
		GameManager::get().addPlayerInGameRoom(id, reserved_room_id);
		cerr << "[addNewPLayer]" << matchingPlayerCount << endl;

		return ret;
	}
	bool IsFull() const { return MAX_PLAYER == matchingPlayerCount; }
	bool IsReady()
	{
		if (matchingPlayerCount < MIN_PLAYER)
			return false;

		for (auto& p : getReservedRoom().GetPlayers())
		{
			if (p.IsFree())
				continue;

			if (p.state != Player::State::MATCHREADY)
				return false;
		}

		return true;
	}
	void MatchingComplete() //3
	{
		cerr << "[MatchComplte]" << endl;
		getReservedRoom().state = GameRoom::State::READY2RUN;
		matchingPlayerCount = 0;
	}
	void disCountOnce() { matchingPlayerCount--; }
	GameRoomID getReservedRoomId() const { return reserved_room_id; }
public:
	GameRoom& getReservedRoom() { return GameManager::get().getGameRoom(reserved_room_id); }

private:
	GameRoomID reserved_room_id{ 0 };
	InGameID matchingPlayerCount{ 0 };
};