#pragma once

#include "Obj.h"
#include "Item.h"

class Camera;

/* moving obj */
class DynamicObj : public Obj
{
public:
	explicit DynamicObj(GameRoomID roomid) :Obj{ roomid } {}
public:
	virtual ~DynamicObj() {};

public:
	GET(linear_speed);
	SET(linear_speed);
	glm::vec3 get_moving_dir()const { return glm::normalize(linear_speed_); }


public:
	virtual void update(milliseconds time_elapsed) override
	{
		if (!get_enable())
			return;

		auto t = time_elapsed.count() / 1000.f;
		update_speed(t);
		update_rotate(t);
		update_movement(t);
	}

private:
	void update_rotate(float time_elapsed)
	{
		rotate(angular_speed_ * time_elapsed);
	}

	virtual void update_speed(float time_elapsed) {};

	void update_movement(float time_elapsed);

protected:
	glm::vec3 linear_speed_{ glm::epsilon<float>() / 1.25,0,0 };
	glm::vec3 angular_speed_{};
};


