#pragma once

class Event;
using EventPtr = shared_ptr<Event>;

class EventManager
{
	friend Event;
	using ListType = forward_list<EventPtr>;

public:
	EventManager(GameRoomID roomid) :roomid{ roomid } {}
public:
	void update(milliseconds time);
	void Add(EventPtr e);

private:
	GameRoomID roomid;
	ListType EventList; // 여기서 관리
};

///////////////////////////////////////////////////////

class Event
{
	friend EventManager;
public:
	Event(GameRoomID roomid) :roomid_{ roomid } {}
	virtual ~Event() = default;

protected:
	void done() { enable = false; }

private:
	bool enable{ true };
	virtual void Start() = 0;
	virtual void Update(milliseconds time) = 0;
	virtual void Detroy() = 0;

public:
	GET(roomid);
private:
	GameRoomID roomid_;
};

///////////////////////////////////////////////////////

class GameClearEvent : public Event
{
	friend EventManager;
public:
	GameClearEvent(GameRoomID roomid, milliseconds clearAt) :Event{ roomid }, clearAt{ clearAt } {};
	virtual ~GameClearEvent() = default;
protected:
	void done() { enable = false; }
	int id{};
private:
	bool enable{ true };
	milliseconds clearAt;
	virtual void Start() override;
	virtual void Update(milliseconds time) override;
	virtual void Detroy() override;
};
