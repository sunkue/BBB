#pragma once
#include "DynamicObj.h"

using VehiclePtr = shared_ptr<class VehicleObj>;
class VehicleObj : public DynamicObj
{
	friend class AutoVehicleObj;
private:
	virtual void save_file_impl(ofstream& file) {};
	virtual void load_file_impl(ifstream& file) override;

public:
	enum class CONTROLL { negative = -1, none = 0, positive = 1 };

public:
	explicit VehicleObj(GameRoomID roomid, InGameID id)
		: DynamicObj{ roomid }, id_{ id }
	{
		load("car" + to_string(id));
		const_cast<Boundings&>(get_boundings()).load("carbounding");
	
		accquired_this_frame_item_log_.reserve(item_list_.maxSize);
		used_this_frame_item_log_.reserve(item_list_.maxSize);
	}

public:
	virtual void update(milliseconds time_elapsed) override
	{
		if (!get_enable())
			return;

		if (item_use_on_)
		{
			item_use_on_ = false;
			OnUseItem();
		}
	}

	virtual void on_collide(Obj& other, OUT void* context = nullptr);
public:
	void regenerate();

public:
	//void processinput(InputKey key, bool press);
public:

public:
	GET(rank);
	SET(rank);
	SET(item_use_on);
	GET(id);

	GET(shield);
	SET(shield);
	GET(boost);
	SET(boost);
private:
	InGameID id_;
	bool item_use_on_ = false;
	bool shield_ = false;
	bool boost_ = false;
public:
	SET(included_node);
	GET(included_node);
	GET(lab);
	milliseconds cleartime;
private:
	int included_node_ = 0;
	int rank_ = 0;
	int lab_ = -1;
public:
	// this < other
	bool rank_worse_than(VehicleObj& other);

public:
	enum class CHECK_POINT
	{
		none = 0,
		begin = 1,
		check1,
		check2,
	};

	GET(check_point);
	SET(check_point);

	void clear_lab();
private:
	CHECK_POINT check_point_ = CHECK_POINT::check2;

public:
	bool TryAcquireItem(ItemObj& itemObj);
	void OnUseItem();

	void use_duple()
	{
		item_list_.UseDuple();
	}
private:
	ItemInstanceList item_list_;


public:
	void clear_frame_log() { clear_item_log(); }

	/*
		for GameRoom::synchronize_clients
	*/

	GET_REF(accquired_this_frame_item_log);
	GET_REF(used_this_frame_item_log);
	void clear_item_log()
	{
		accquired_this_frame_item_log_.clear();
		used_this_frame_item_log_.clear();
	}
private:
	vector<pair<ItemType, ObjID>> accquired_this_frame_item_log_;
	vector<ItemType> used_this_frame_item_log_;
public:
	GET(regened);
	SET(regened);
	SET(cleared);
	GET(cleared);
	GET(outed);
	SET(outed);
private:
	bool outed_{ false };
	bool regened_{ false };
	bool cleared_{ false };
};


/*=================================================
*
*				AutoVehicleObj
*
==================================================*/

using AutoVehiclePtr = shared_ptr<class AutoVehicleObj>;
class AutoVehicleObj : public VehicleObj
{
public:
	virtual void save_file_impl(ofstream& file) override {};
	virtual void load_file_impl(ifstream& file) override;
public:
	AutoVehicleObj(GameRoomID roomid, InGameID id) :
		VehicleObj{ roomid, id }
	{
		PredictForwardNodeCount_ += rand() % 3;
		StreightMoveBelowValue_ += 0 < rand() % 3 ? 0 : 0.05f;
		MoveHeadDirBlendAlpha_ += (rand() % 3) * 0.1f;
	};
public:
	void update_state();
	virtual void update_speed(float time_elapsed) override;
	virtual void on_collide(Obj& other, OUT void* context = nullptr);
	virtual void update(milliseconds time_elapsed) override;
	void DoAi(milliseconds time_elapsed);
private:
	bool AiOn_ = true;
	int PredictForwardNodeCount_ = 4;
	float JugmentValue = 0;
	float StreightMoveBelowValue_ = 0.1f;
	float MoveHeadDirBlendAlpha_ = 0.3f;
private:
	float angular_power_ = 1.5f;
	float acceleration_power_ = 16.f;
	float friction_power_ = 2.0f;
	float max_speed_ = 80.0f;

	float acceleration_ = 0.f;
	float friction_ = 0.f;

	CONTROLL accel_control_{ CONTROLL::none };
	CONTROLL angular_control_{ CONTROLL::none };

	bool front_on_ = false;
	bool back_on_ = false;
	bool right_on_ = false;
	bool left_on_ = false;

	bool brake_on_ = false;
	bool draft_on_ = false;
};

