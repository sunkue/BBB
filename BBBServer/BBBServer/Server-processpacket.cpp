#include "stdafx.h"
#include "Server.h"
#include "GameSystem.h"
#include "GameRoom.h"

/////////////////////////////////////////////////////////////////////////////////////////

void Server::process_packet(ClientID id, const void* const packet)
{
	// conditional funtion => 함수객체 전달해서 처리.
	auto SND2ME = [this, id](const void* const packet)
	{
		clients[id].do_send(packet);
	};

	auto SND2OTHERCLIENTS = [this, id](const void* const packet)
	{
		for (int i = 0; i < clients.size(); i++)
		{
			if (i != id) { clients[i].do_send(packet); }
		}
	};

	auto SND2EVERYCLIENTS = [this](const void* const packet)
	{
		for (auto& c : clients)
		{
			c.do_send(packet);
		}
	};

	auto SND2EVERYOTHERPLAYERS = [this, id](GameRoom& room, const void* const packet)
	{
		for (auto& p : room.GetPlayers())
		{
			if (p.IsFree())
				continue;

			if (p.netid == id)
				continue;

			clients[p.netid].do_send(packet);
		}
	};

	auto SND2EVERYPLAYERS = [this](GameRoom& room, const void* const packet)
	{
		for (auto& p : room.GetPlayers())
		{
			if (p.IsFree())
				continue;

			clients[p.netid].do_send(packet);
		}
	};

	auto packet_type = reinterpret_cast<const packet_base<void>*>(packet)->packet_type;
	//	cerr << "[RECV::" << +packet_type._to_string() << "]" << endl;

	auto& matchingRoom = MatchingRoom::get();

	if (+PACKET_TYPE::Cs_connect == packet_type)
	{
		auto pck = reinterpret_cast<const cs_connect*>(packet);
		bool sameversion = GameSystem::get().get_version() == pck->version;
		if (sameversion)
		{
			sc_accept accept;
			accept.ur_ingameid = matchingRoom.AddNewMatchingPlayer(id);
			accept.ur_gameroomid = matchingRoom.getReservedRoomId();
			if (accept.ur_ingameid < 0)
			{
				//  match fail
				cerr << "MATCHFAIL,INGAMEID::" << accept.ur_ingameid << endl;
				clients[id].do_disconnect();
			}
			else
			{
				// accepted. this is your id.
				SND2ME(&accept);

				// synchronize every matchers info.

				// board cast new matcher info.
				sc_new_matcher new_matcher;
				new_matcher.new_matcher_ingameid = accept.ur_ingameid;
;
				new_matcher.ready = false;
				SND2EVERYOTHERPLAYERS(matchingRoom.getReservedRoom(), &new_matcher);

				// get exist matchers info.
				for (auto& p : matchingRoom.getReservedRoom().GetPlayers())
				{
					if (p.IsFree())
						continue;

					if (p.netid == id)
						continue;

					clients[p.netid].do_send(&new_matcher);
					new_matcher.new_matcher_ingameid = p.ingameid;
					new_matcher.ready = (Player::State::MATCHREADY == p.state);
					SND2ME(&new_matcher);
				}

				// check match compelte
				if (matchingRoom.IsFull())
				{
					matchingRoom.MatchingComplete();

					sc_matchcomplete match_complete;

					for (auto& p : matchingRoom.getReservedRoom().GetPlayers())
					{
						if (p.IsFree())
							continue;

						clients[p.netid].do_send(&match_complete);
					}

					matchingRoom.ReserveFreeRoom();
				}
			}
		}
		else
		{
			//  match fail
			cerr << "DifferrntVersion::[c,s]::" << pck->version << ", " << GameSystem::get().get_version() << endl;
			clients[id].do_disconnect();
		}

		return;
	}

	auto roomid = GameManager::get().findGameRoomId(id);
	auto& gameRoom = GameManager::get().getGameRoom(roomid);
	auto ingameid = GameManager::get().findIngameId(gameRoom, id);
	auto& players = gameRoom.GetPlayers();
	auto& player = players[ingameid];

	switch (packet_type)
	{
	case PACKET_TYPE::Cs_connect:
	{
		/*위에서 처리하고있음*/
		cerr << "SOMETHING WRONG WITH CS CONNECT" << endl;
	}
	CASE PACKET_TYPE::Cs_change_ready :
	{
		auto pck = reinterpret_cast<const cs_change_ready*>(packet);

		if (roomid != matchingRoom.getReservedRoomId())
			break;

		if (pck->ready)
			gameRoom.GetPlayers()[ingameid].state = Player::State::MATCHREADY;
		else
			gameRoom.GetPlayers()[ingameid].state = Player::State::INCHARGE;

		sc_change_ready ready;
		ready.ingameid = ingameid;
		ready.ready = pck->ready;
		SND2EVERYOTHERPLAYERS(gameRoom, &ready);

		if (matchingRoom.IsReady())
		{
			matchingRoom.MatchingComplete();

			sc_matchcomplete match_complete;

			for (auto& p : matchingRoom.getReservedRoom().GetPlayers())
			{
				if (p.IsFree())
					continue;

				clients[p.netid].do_send(&match_complete);
			}

			matchingRoom.ReserveFreeRoom();
		}
	}
	// match & resource load compelete.
	CASE PACKET_TYPE::Cs_ready2run:
	{
		auto pck = reinterpret_cast<const cs_ready2run*>(packet);

		player.state = Player::State::READY2RUN;

		bool allready = true;
		for (auto& p : players)
		{
			if (p.state == Player::State::FREE)
				continue;

			if (p.state != Player::State::READY2RUN)
			{
				allready = false;
				break;
			}
		}

		if (allready)
		{
			sc_gamestart gamestart;
			gamestart.starttime = clk::now() + 5s;
			gamestart.seed = gameRoom.game->GetSeed();
			SND2EVERYPLAYERS(gameRoom, &gamestart);
			gameRoom.StartGame(gamestart.starttime);
		}
	}
	CASE PACKET_TYPE::Cs_player_info:
	{
		auto pck = reinterpret_cast<const cs_player_info*>(packet);
		auto& vehicle = gameRoom.game->scene->getVehicle(ingameid);
		auto pos = decltype(pck->pos)::decode<glm::vec3>(pck->pos);
		auto rotate = decltype(pck->rotate)::decode<glm::quat>(pck->rotate);
		auto linear_speed = decltype(pck->linear_speed)::decode<glm::vec3>(pck->linear_speed);
		auto constY = vehicle->get_position().y;
		pos.y = constY;
		//validcheck
		{
			lock_guard lock{ gameRoom.gamelock };
			vehicle->move(pos - vehicle->get_position());
			vehicle->rotate(glm::inverse(vehicle->get_rotation()) * rotate);
			vehicle->set_linear_speed(linear_speed);
		}

		player.timestamp = pck->timestamp;
	}
	CASE PACKET_TYPE::Cs_use_item:
	{
		auto pck = reinterpret_cast<const cs_use_item*>(packet);
		lock_guard lock{ gameRoom.gamelock };
		gameRoom.game->scene->getVehicle(ingameid)
			->set_item_use_on(true);
	}
	break; default: cerr << "[[[!!]]]" << endl; break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
