#include "stdafx.h"
#include "Vehicle.h"
#include "TrackNode.h"
#include "Item.h"
#include "Event.h"
#include "ItemEvent.h"
#include "GameRoom.h"

void VehicleObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
}



#define CHECK_NAN(v)								\
{													\
	auto res = glm::isnan(v);						\
	if (res.x || res.y || res.z)					\
	{												\
		cout << #v << __LINE__ << __func__ << endl;		\
	}												\
}


void VehicleObj::on_collide(Obj& other, OUT void* context)
{
	if (&other == this)
		return;
}

void VehicleObj::regenerate()
{
	// 리젠시 체크포인트 업데이트 필요함,,
	auto& track = getGameRoom().game->scene->get_track();
	track->include_obj(*this, true);
	regened_ = true;
}

bool VehicleObj::rank_worse_than(VehicleObj& other)
{
	if (!get_enable())
		return true;

	//lab
	if (lab_ < other.lab_)
	{
		return true;
	}
	else if (lab_ > other.lab_)
	{
		return false;
	}


	// same lab
	// cleared
	if (lab_ == MAX_LAB && other.lab_ == MAX_LAB)
	{
		return other.cleartime < cleartime;
	}

	// same lab
	// node
	auto& track = getGameRoom().game->scene->get_track();
	auto& nodes = track->get_tracks();
	auto& this_node = nodes.at(included_node_);
	auto& other_node = nodes.at(other.included_node_);
	if (this_node->get_from_start() < other_node->get_from_start())
	{
		return true;
	}
	else if (this_node->get_from_start() > other_node->get_from_start())
	{
		return false;
	}

	// same lab
	// same node
	// pos
	auto next_pos = this_node->get_next_center();
	for (auto next : this_node->get_next_nodes())
	{
		next_pos += next->get_next_center();
	}
	next_pos /= this_node->get_next_nodes().size() + 1;

	auto this_diff = next_pos - get_position();
	auto other_diff = next_pos - other.get_position();
	return glm::length(other_diff) < glm::length(this_diff);
}

void VehicleObj::clear_lab()
{
	lab_ += 1;
	cleared_ = true;
}

bool VehicleObj::TryAcquireItem(ItemObj& itemObj)
{
	auto item = itemObj.to_instance();
	int success = item_list_.TryPush(item);

	bool ret = success;
	while (success--)
		accquired_this_frame_item_log_.emplace_back(item->get_type(), itemObj.get_objid());

	return ret;
}

void VehicleObj::OnUseItem()
{
	optional<ItemInstancePtr> item = item_list_.Pop();
	if (!item.has_value())
	{
		//		cout << "!!!!" << endl;
		return;
	}

	auto usingItem = item.value();

	used_this_frame_item_log_.push_back(usingItem->get_type());

	// usingItem
	auto& eventmanager = getGameRoom().game->eventmanager;
	// target id..
	eventmanager->Add(EventFactory::createEvent(usingItem, get_gameroomid(), id_, get_gameroomid()));
}




/*=================================================
*
*				AutoVehicleObj
*
==================================================*/



void AutoVehicleObj::load_file_impl(ifstream& file)
{
	Obj::load_file_impl(file);
}

void AutoVehicleObj::DoAi(milliseconds time_elapsed)
{
	if (!AiOn_)return;
	// 상태 변경
	auto& track = getGameRoom().game->scene->get_track();;
	included_node_ = track->find_closest_track(*this);
	auto& node = track->get_tracks()[included_node_];
	auto next = node->get_next_nodes()[0];
	for (int i = 1; i < PredictForwardNodeCount_; i++)
		next = next->get_next_nodes()[0];

	auto target = next->get_next_center();

	auto targetDir = glm::normalize(target - get_position());
	auto movingDir = get_moving_dir();
	auto headDir = get_head_dir();
	//auto movingDir = get_head_dir();
	auto predictDir = glm::lerp(headDir, movingDir, MoveHeadDirBlendAlpha_);
	/* 좌우 판별 */
	JugmentValue = glm::dot(Y_DEFAULT, glm::cross(targetDir, predictDir));

	if (0 < JugmentValue)
	{
		//cout << "RR" << endl;
		right_on_ = true;
		left_on_ = false;
		back_on_ = true;
		front_on_ = false;
	}
	else
	{
		//	cout << "LL" << endl;
		right_on_ = false;
		left_on_ = true;
		back_on_ = true;
		front_on_ = false;
	}

	if (abs(JugmentValue) < StreightMoveBelowValue_)
	{
		//	cout << ".." << endl;
		front_on_ = true;
		back_on_ = false;
		right_on_ = false;
		left_on_ = false;
		item_use_on_ = true;
	}
	else
	{
		item_use_on_ = false;
	}
}

void AutoVehicleObj::update_state()
{
	const glm::vec3 _angular_power = Y_DEFAULT * angular_power_;
	const float _acceleration_power = acceleration_power_;
	const float _friction_power = friction_power_;

	angular_speed_ = _angular_power * static_cast<int>(angular_control_);
	acceleration_ = _acceleration_power * static_cast<int>(accel_control_);

	// [B] if(_brake_on)_acceleration = 0;
	//_acceleration -= _acceleration * _brake_on;

	friction_ = _friction_power;
	// [B] if(_brake_on)_friction += _friction_power;
	friction_ += _friction_power * draft_on_ * 7;
	//glm::length(get_linear_speed());

	friction_ += _friction_power * brake_on_ * 2;

	int accel_control{ static_cast<int>(CONTROLL::none) };
	int angular_control{ static_cast<int>(CONTROLL::none) };
	accel_control += front_on_;
	accel_control -= back_on_;
	angular_control -= right_on_;
	angular_control += left_on_;
	accel_control_ = static_cast<CONTROLL>(accel_control);
	angular_control_ = static_cast<CONTROLL>(angular_control);
}



void AutoVehicleObj::update_speed(float time_elapsed)
{
	auto head_dir = get_head_dir();
	CHECK_NAN(get_rotation());
	CHECK_NAN(head_dir);
	CHECK_NAN(linear_speed_);

	/* fric */
	auto prev_dir = glm::normalize(linear_speed_);
	auto prev_speed = glm::length(linear_speed_);
	if (prev_speed < glm::epsilon<float>())
	{
		prev_dir = V_ZERO;
	}
	auto fric = -1 * prev_dir * friction_ * time_elapsed;
	CHECK_NAN(fric);
	/* accel */

	if (boost_)
	{
		acceleration_ = 1;
	}
	auto accel = head_dir * acceleration_ * time_elapsed;
	CHECK_NAN(accel);

	///* accumulate */
	auto new_linear_speed = linear_speed_ + accel + fric;
	CHECK_NAN(new_linear_speed);

	/* no backward movement */
	auto new_dir = glm::normalize(new_linear_speed);
	auto new_speed = glm::length(new_linear_speed);
	if (new_speed < ellipsis)
	{
		new_dir = V_ZERO;
	}
	CHECK_NAN(new_dir);

	auto maxspeed = max_speed_;
	if (boost_)
	{
		maxspeed *= 1.2;
		new_linear_speed += accel * 20;
	}

	/* overspeed */
	bool over_speed = maxspeed < new_speed;

	// [B] if (over_speed) { new_linear_speed = new_dir * _max_speed; }
	new_linear_speed -= new_linear_speed * over_speed;
	new_linear_speed += new_dir * maxspeed * over_speed;
	CHECK_NAN(new_linear_speed);

	linear_speed_ = new_linear_speed;
	linear_speed_.y = 0;
	CHECK_NAN(linear_speed_);
}

void AutoVehicleObj::on_collide(Obj& other, OUT void* context)
{
	if (&other == this)
		return;

	if (shield_) return;
RE:
	if (collision_detect(other))
	{
		auto d = glm::normalize(other.get_position() - get_position());
		d.y = 0;
		move(-d * 0.001);
		if (glm::epsilon<float>() < glm::length(d))
			goto RE;
	}

	if (auto car = dynamic_cast<VehicleObj*>(&other))
	{
		if (shield_) return;
		set_force(get_force() + car->get_position());
		set_force2(get_force2() + car->get_linear_speed());
		set_collided(true);
		auto v = glm::length(get_linear_speed()) / max_speed_;
		return;
	}

	if (auto deco = dynamic_cast<ItemObj*>(&other))
	{
		return;
	}

	{
		if (shield_) return;
		auto force = other.get_position();
		force.y = get_position().y;
		set_force(get_force() + force);

		//	if (dynamic_cast<AutoVehicleObj*>(this))
		//	{
		auto& track = getGameRoom().game->scene->get_track();;
		auto& node = track->get_tracks()[included_node_];
		auto next = node->get_next_nodes()[0];
		for (int i = 1; i < 4; i++)
			next = next->get_next_nodes()[0];
		auto t = 10 * glm::normalize(next->get_position() - get_position());
		set_force2(get_force2() + t);
		//	}
		//	else
		//		set_force2(get_force2() - get_linear_speed() * 0.5);

		set_collided(true);
		auto v = glm::length(get_linear_speed()) / max_speed_;
		return;
	}
}

void AutoVehicleObj::update(milliseconds time_elapsed)
{
	if (!get_enable()) return;
	DoAi(time_elapsed);

	update_state();

	DynamicObj::update(time_elapsed);

	if (item_use_on_)
	{
		item_use_on_ = false;
		OnUseItem();
	}
}