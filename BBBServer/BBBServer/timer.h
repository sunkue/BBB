#pragma once


/////////////////////////////////////////////////////////////

/* logic machine */
class TimerSystem
{
public:
	void start() {
		elapsed_time_ = 0ms;
		game_time_ = 0ms;
		prev_time_point_ = clk::now();
	}

	void tick() 
	{
		auto now = clk::now();
		elapsed_time_ = duration_cast<milliseconds>(now - prev_time_point_);
		game_time_ += elapsed_time_; // should be overflow
		prev_time_point_ = now;
	};

	milliseconds tick_time()
	{
		return elapsed_time_;
	}

	float tick_timef()
	{
		return elapsed_time_.count()/1000.f;
	}

	milliseconds game_time()
	{
		return game_time_;
	}

private:
	milliseconds elapsed_time_{};
	milliseconds game_time_{};
	clk::time_point prev_time_point_{};
};

/// //////////////////////////////////////////////////




///////////////////////////////////////


